package com.inno.embroidery.itemPackage.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.packages.model.EmbroideryPackage;

@Entity
@Table(name="item_package")
public class ItemPackage {
	@Id @GeneratedValue
	@Column(name="id")
	private int id;
	
	@JoinColumn(name="packageid", referencedColumnName="packageid")
	@ManyToOne(optional=false)
	private EmbroideryPackage packageId;
	
	@JoinColumn(name="itemid" ,referencedColumnName="id")
	@ManyToOne(optional=false)
	private EmbroideryItem itemId;
	
	@Column(name="userid")
	private int userId;
	
	@Column(name="createddate")
	private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public EmbroideryPackage getPackageId() {
		return packageId;
	}

	public void setPackageId(EmbroideryPackage packageId) {
		this.packageId = packageId;
	}

	public EmbroideryItem getItemId() {
		return itemId;
	}

	public void setItemId(EmbroideryItem itemId) {
		this.itemId = itemId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
}
