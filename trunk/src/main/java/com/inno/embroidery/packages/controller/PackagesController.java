package com.inno.embroidery.packages.controller;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.packages.service.EmbroideryPackageService;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.categoryItem.model.*;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.itemPackage.model.ItemPackage;

@Controller
public class PackagesController {
	@Autowired
	protected EmbroideryPackageService service;	
	
	@RequestMapping(value= {"/embroideryPackages"})
	public String getPackages(Model model){
		List<EmbroideryPackage> embroideryPackages=service.getEmbroideryPackages();
		model.addAttribute("embroideryPackages", embroideryPackages );	
		model.addAttribute(new EmbroideryPackage());
		return "embroideryPackages";
	}
	
	@RequestMapping(value="create-NewPackage")
	public String createEmbroideryUserGet(Model model){
		System.out.println("*** Redirected to Create page");
		List<Category> categories= service.getCategories();
		List<CategoryItem> itemCategories=service.getItemCategories();
		model.addAttribute("embroideryPackage", new EmbroideryPackage());
		model.addAttribute("categories", categories);
		model.addAttribute("itemCategories", itemCategories );
		System.out.println("************ KA BOOOM ********");
		return "create-NewPackage";
	}
	
	// -------------------------------------------------------
//	@RequestMapping(value="embroideryPackages")
//	public String manageEmbroideryPackageGet(Model model){
//		model.addAttribute("manageInfo", new EmbroideryPackage());
//		return "embroideryPackages";
//	}
	//---------------------------------------------------------
	
	@SuppressWarnings("null")
	@RequestMapping(value="create-NewPackage",method = RequestMethod.POST )
	public String createPackagePost(
			@ModelAttribute("embroideryPackage") EmbroideryPackage embroideryPackage){
		
		// ---------- Insert Situation -------------
		if(embroideryPackage.getPackageId()==0 && embroideryPackage.getPackageActive()==0 && embroideryPackage.getMode()==null){
			embroideryPackage.setPackageActive(1);			
			 			
			
			System.out.println("right before 3----> ");
			List<ItemPackage> itemList=new ArrayList<ItemPackage>() ;			
			int i=0;
				System.out.println("Non Split :"+embroideryPackage.getAs());
			String[] splitArray=embroideryPackage.getAs().split(",");
			
			for (String retval: splitArray){				
//					System.out.println("*****"+retval);
				EmbroideryItem soloItem=service.getItemOb(retval); 
//					System.out.println("Itm is==>: "+soloItem.getFileName());
				ItemPackage itemPackageBean=new ItemPackage();
				itemPackageBean.setItemId(soloItem);
				itemPackageBean.setPackageId(embroideryPackage);
				itemList.add(itemPackageBean);
//					System.out.println("Check point ----> ");														
		      }			
//			System.out.println("*** I is :"+i);
//			if(i>0)	
			embroideryPackage.setItemPackageList(itemList);
			for(Iterator<ItemPackage> x= itemList.iterator(); x.hasNext();){
				ItemPackage checkList=x.next();
				System.out.println("ListItems: "+checkList.getItemId().getId());
				System.out.println("ListItems: "+checkList.getItemId().getFileName());
			}
			service.createEmbroideryPackages(embroideryPackage);
			System.out.println("Hello Done ******");			
		}
		// ----------- Update Situation -------------
		else if(embroideryPackage.getPackageId()!=0 && embroideryPackage.getPackageActive()==1 && embroideryPackage.getMode()==null)
		{			
			service.updateEmbroideryPackages(embroideryPackage);			
		}
		// ------------ Delete Situation ------------
		else if(embroideryPackage.getPackageId()!=0 && embroideryPackage.getPackageActive()==0 && embroideryPackage.getMode()==null){
			
			embroideryPackage.setPackageActive(0);
			service.updateEmbroideryPackages(embroideryPackage);
		}
//		else if(embroideryPackage.getMode()!=null){
//			String ret= embroideryPackage.getMode(); 
//			int conId=Integer.parseInt(ret);
////			EmbroideryItem
//			
//		}
		return "redirect:create-NewPackage";
	}
	
//	@RequestMapping(value="embroideryPackages",method=RequestMethod.POST )
//	public String updatePackagePost(
//			@ModelAttribute("embroideryPackage") EmbroideryPackage embroideryPackage){
//		service.updateEmbroideryPackages(embroideryPackage);
//		return "redirect:embroideryPackages";
//	} 
	@RequestMapping(value="embroideryPackages",method=RequestMethod.POST )
	public String searchPackagePost(Model model,
			@ModelAttribute("embroideryPackage") EmbroideryPackage embroideryPackage){
		
		System.out.println("The Mode is: "+embroideryPackage.getMode());
		if(embroideryPackage.getMode().equals("SEARCH")){
			List<EmbroideryPackage> embroideryPackages = service.getSearchEmbroideryPackages(embroideryPackage);
			model.addAttribute("embroideryPackages", embroideryPackages);
			
		}
		else{ }		
		return "embroideryPackages";
	} 
	
}
