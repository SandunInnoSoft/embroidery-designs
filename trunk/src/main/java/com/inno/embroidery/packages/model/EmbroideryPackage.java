package com.inno.embroidery.packages.model;



import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.inno.embroidery.itemPackage.model.ItemPackage;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.users.model.*;

/** 
 *  @author: VikumSam
 *  @date: 07/07/15
 *   
 * This is the model class for Packages
 * * This connects with the 'packages' table in 'embroider' database
 */
@Entity
@Table(name="packages")
public class EmbroideryPackage {

	@Id	@GeneratedValue
	@Column(name="packageid")
	private int packageId;
	
	@Column(name="packagename")
	private String packageName;
	
	@Column(name="packagedescrip")
	private String packageDescription;
	
	@Column(name="active")
	private int packageActive;
	
		
	@OneToMany(cascade=CascadeType.ALL, mappedBy="promoPackage")
	private List<EmbroideryPromotion> packagePromotionList; 
	
	@Transient
	private String mode;
	
	@Transient
	private String as;
	
	@Column(name="createddate")
	private Date createdDate;
	
	@OneToMany(cascade=CascadeType.ALL,mappedBy="packageId")
	private List<ItemPackage> itemPackageList;
	
	
	
	
//	@Column
//	private EmbroiderUser embroiderUser;
	
//	@Column
//	private Date date;
	
	public int getPackageId() {
		return packageId;
	}
	public List<EmbroideryPromotion> getPackagePromotionList() {
		return packagePromotionList;
	}
	public void setPackagePromotionList(
			List<EmbroideryPromotion> packagePromotionList) {
		this.packagePromotionList = packagePromotionList;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageDescription() {
		return packageDescription;
	}
	public void setPackageDescription(String packageDescription) {
		this.packageDescription = packageDescription;
	}
	public int getPackageActive() {
		return packageActive;
	}
	public void setPackageActive(String active) {
		
		this.packageActive = Integer.parseInt(active);
	}
	
	public void setPackageActive(int active) {
		this.packageActive=active;
	}
//	public EmbroiderUser getEmbroiderUser() {
//		return embroiderUser;
//	}
//	public void setEmbroiderUser(EmbroiderUser embroiderUser) {
//		this.embroiderUser = embroiderUser;
//	}
	/*
	 * Created Dates, Auto generated
	 * */
//	public Date getDate() {
//		return date;
//	}
//	public void setDate(Date date) {
//		this.date = date;
//	}
//	
//	public List<EmbroideryPromotion> getEmbroideryPromotions() {
//		return embroideryPromotions;
//	}
//	public void setEmbroideryPromotions(
//			List<EmbroideryPromotion> embroideryPromotions) {
//		this.embroideryPromotions = embroideryPromotions;
//	}
//	public String getMode() {
//		return mode;
//	}
	
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getMode() {
		return mode;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getAs() {
		return as;
	}
	public void setAs(String as) {
		this.as = as;
	}
	public List<ItemPackage> getItemPackageList() {
		return itemPackageList;
	}
	public void setItemPackageList(List<ItemPackage> itemPackageList) {
		this.itemPackageList = itemPackageList;
	}
	
	
	
}
