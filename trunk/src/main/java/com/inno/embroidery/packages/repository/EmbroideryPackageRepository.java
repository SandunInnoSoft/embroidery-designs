package com.inno.embroidery.packages.repository;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;




import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.packagehistory.model.PackageHistory;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.userhistory.model.UserHistory;
import com.inno.embroidery.users.model.EmbroiderUser;
import com.inno.embroidery.categoryItem.model.*;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.itemPackage.model.ItemPackage;

@Repository
@Transactional
public class EmbroideryPackageRepository {
	
	protected SessionFactory factory;
	
	public List<EmbroideryPackage> getPackages(){ 
		List<EmbroideryPackage> resultPackages=null;
		factory=HibernateUtil.getSessionFactory();
		
		Session session =factory.openSession();
		Transaction tx=null;
		
		try {
			
			tx=session.beginTransaction();
			resultPackages=session.createQuery("FROM EmbroideryPackage p where active='1' ").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{
			
			session.close();
		}
		
		return resultPackages;
		
	}
	public EmbroideryItem getItemUrl(EmbroideryItem item){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		EmbroideryItem items = new EmbroideryItem();
		try {
			tx = session.beginTransaction();
			int iD=item.getId();		
			List<EmbroideryItem> iList = session.createQuery(" FROM EmbroideryItem e where id='"+iD+"'").list();
				for(EmbroideryItem i:iList){
					items.setImagePath(i.getImagePath());
				}
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return items;
	}
	
	public EmbroideryItem getItemObj(String id){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		EmbroideryItem items = null;
		try {
			tx = session.beginTransaction();
			int iD=Integer.parseInt(id);			
			List<EmbroideryItem> iList = session.createQuery("FROM EmbroideryItem e where id='"+iD+"'").list();
				for(EmbroideryItem i:iList){
					items=i;
				}
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return items;
	}
	
	public List<Category> getCategories(){ 
		List<Category> resultCategories=null;
		factory=HibernateUtil.getSessionFactory();
		
		Session session =factory.openSession();
		Transaction tx=null;
		
		try {
			
			tx=session.beginTransaction();
			resultCategories=session.createQuery("FROM Category c ").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{
			
			session.close();
		}
		
		return resultCategories ;
		
	}
	
	public List<CategoryItem> getItemCategories(){ 
		List<CategoryItem> resultCategories=null;
		factory=HibernateUtil.getSessionFactory();
		
		Session session =factory.openSession();
		Transaction tx=null;
		
		try {
			
			tx=session.beginTransaction();
			resultCategories=session.createQuery("FROM CategoryItem c  ").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{
			
			session.close();
		}
		
		return resultCategories ;
		
	}
	
	
	public void createPackage(EmbroideryPackage package1){
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx=null;		
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			package1.setCreatedDate(date);
			tx=session.beginTransaction();
			session.save(package1);
			
			List<ItemPackage> savedItemSet=package1.getItemPackageList();
			if(savedItemSet!=null){
				for(ItemPackage i : savedItemSet){
					session.save(i);
				}
			}
			tx.commit();
		}
		catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }
		
	} 
	
	public void updateEmbroideryPackage(EmbroideryPackage embroideryPackage ){
		factory=new AnnotationConfiguration().configure().addAnnotatedClass(com.inno.embroidery.packages.model.EmbroideryPackage.class).buildSessionFactory();
		Session session = factory.openSession();
	      Transaction tx = null;
	      addHistory(embroideryPackage);
	      try{
	    	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				embroideryPackage.setCreatedDate(date);
	         tx = session.beginTransaction();
//	         embroideryPackage = 
//	                    (EmbroideryPackage)session.get(EmbroideryPackage.class, embroideryPackage.getPackageId() ); 
	         System.out.println(embroideryPackage.getPackageId());
	         session.update(embroideryPackage); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }		
	}
	
	public List<EmbroideryPackage> getSearchPackages(EmbroideryPackage e_embroideryPackages){
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
	      List < EmbroideryPackage> searchPackages = null;
	      try{
	         tx = session.beginTransaction();
	         searchPackages = session.createQuery("FROM EmbroideryPackage p WHERE packageName LIKE '%"+e_embroideryPackages.getPackageName()+"%'").list();
	         	         
	          tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return searchPackages;
	}
	
	public List <EmbroideryPackage> selectLastEntry(EmbroideryPackage pack){
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		List <EmbroideryPackage> getRecord=null;
		Transaction tx=null;
		
		try {
			tx=session.beginTransaction();
			getRecord=  session.createQuery("FROM EmbroideryPackage p where packageId='"+pack.getPackageId()+"'").list();					
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
	return getRecord;
		
	}
	
	public void addHistory(EmbroideryPackage embroideryPackage){
		factory= HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		PackageHistory packageHistory=new PackageHistory();
		List <EmbroideryPackage> userInformation =selectLastEntry(embroideryPackage);		
		try {	
			for(Iterator<EmbroideryPackage> i= userInformation.iterator();i.hasNext();){
				
				EmbroideryPackage packageInfo = i.next();					
				packageHistory.setPackageId(packageInfo.getPackageId());
				packageHistory.setPackageName(packageInfo.getPackageName());
				packageHistory.setPackageDescrip(packageInfo.getPackageDescription());				
				packageHistory.setActive(packageInfo.getPackageActive());
				packageHistory.setCreatedDate(packageInfo.getCreatedDate());
				
			tx=session.beginTransaction();
			session.save(packageHistory);
			tx.commit();
			
			}
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }
		
	}
	public void addItemPackage(ItemPackage item){
		factory= HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			item.setCreatedDate(date);
			tx=session.beginTransaction();
			session.save(item);
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
		
		
	}
	
}
