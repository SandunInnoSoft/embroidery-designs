package com.inno.embroidery.packages.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.packages.repository.EmbroideryPackageRepository;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.categoryItem.model.*;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;

/**
 * This class Holds communicate with Controller class and repository layer class for Package Functionals.  
 * <p>
 * All the date packets from repository and to repository are transfer through the Service layer 
 *
 * @author 		VikumSam
 * @since       20/07/15
 
 *  */

@Service
public class EmbroideryPackageService {

	@Autowired
	protected EmbroideryPackageRepository repository;
	
	public List<EmbroideryPackage> getEmbroideryPackages(){
		return repository.getPackages();
	}	
	public void createEmbroideryPackages(EmbroideryPackage package1){
		repository.createPackage(package1);		
	}
	public void updateEmbroideryPackages(EmbroideryPackage package1){
		repository.updateEmbroideryPackage(package1);
	}
	public List<EmbroideryPackage> getSearchEmbroideryPackages(EmbroideryPackage e_mbroiderPackage){
		return repository.getSearchPackages(e_mbroiderPackage);
	}
	public List <Category> getCategories(){
		return repository.getCategories();	
		}
	public EmbroideryItem getItemOb(String id){
		return repository.getItemObj(id);
	}
	public EmbroideryItem getImgPath(EmbroideryItem item){
		return repository.getItemUrl(item);
	}
	public List <CategoryItem> getItemCategories(){
		return repository.getItemCategories();	
	}
	
}

