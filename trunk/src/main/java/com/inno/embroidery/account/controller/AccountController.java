package com.inno.embroidery.account.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccountController {
	
	@RequestMapping(value = { "/account" })
	public String getAccountPage(Model model) {
		return "account";
	}	
}
