package com.inno.embroidery.error.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController {
	
	@RequestMapping(value = { "/error404" })
	public String getErrorPage(Model model) {
		return "error404";
	}
	
}
