package com.inno.embroidery.embroideryItem.repository;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.service.EmbroideryItemRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmbroideryItemService {

	@Autowired
	protected EmbroideryItemRepository repository;

	public List<EmbroideryItem> getEmbroideryItems() {
		return repository.getEmbroideryItems();
	}

	public void createEmbroideryItem(EmbroideryItem embroideryItem) {
		repository.createEmbroideryItem(embroideryItem);
	}

	public EmbroideryItem getEmbroideryItemforId(Integer id) {
		return repository.getEmbroideryItemforId(id);
	}
	
	public List<EmbroideryItem> getEmbroideryItemsBySearch(String keyword) {
		return repository.getEmbroideryItemsBySearch(keyword);
	}
	
}