package com.inno.embroidery.embroideryItem.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.persistence.*;

import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.itemPackage.model.ItemPackage;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;

@Entity
@Table(name = "embroidery_items")
public class EmbroideryItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1436433547746886116L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;

	@Column(name = "name", length = 255)
	private String fileName;

	@Column(name = "description", length = 255)
	private String description;

	@Column(name = "filepath")
	private String filePath;

	@Column(name = "imagepath")
	private String imagePath;

	@Column(name = "userid")
	private Integer userId;

	@Column(name = "designerid")
	private Integer designerId;

	@Column(name = "price")
	private double price;

	@Column(name = "date")
	private Date date;

	private transient Integer hasPromo;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "itemId")
	private List<CategoryItem> categoryItemList;

	@OneToMany(cascade=CascadeType.ALL,mappedBy="itemId")
	private List<ItemPackage> itemPackageList;
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="promoItem")
	private List<EmbroideryPromotion> itemPromotionList; 
	
	
	public List<EmbroideryPromotion> getItemPromotionList() {
		return itemPromotionList;
	}

	public void setItemPromotionList(List<EmbroideryPromotion> itemPromotionList) {
		this.itemPromotionList = itemPromotionList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public Integer getDesignerId() {
		return designerId;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getImagePath() {
		return imagePath;
	}

	public double getPrice() {
		return price;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setDesignerId(Integer designerId) {
		this.designerId = designerId;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<CategoryItem> getCategoryItemList() {
		return categoryItemList;
	}

	public void setCategoryItemList(List<CategoryItem> categoryItemList) {
		this.categoryItemList = categoryItemList;
	}

	public List<ItemPackage> getItemPackageList() {
		return itemPackageList;
	}

	public void setItemPackageList(List<ItemPackage> itemPackageList) {
		this.itemPackageList = itemPackageList;
	}


	public Integer getHasPromo() {
		return hasPromo;
	}
	
	public void setHasPromo(Integer hasPromo) {
		this.hasPromo = hasPromo;
	}

}