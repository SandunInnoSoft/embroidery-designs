package com.inno.embroidery.embroideryItem.controller;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.category.service.CategoryService;
import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.repository.EmbroideryItemService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;

@Controller
public class EmbroideryItemController implements ServletContextAware {

	
	private ServletContext servletContext;

	@Autowired
	protected EmbroideryItemService emService;
	
	@Autowired
	protected CategoryService catService;

	
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@RequestMapping(value = { "/embroideryItems" })
	public String getEmbroideryItems(Model model) {
		List<EmbroideryItem> embroideryItems = emService.getEmbroideryItems();
//		List<Category> categories = catService.getCategories();
//		model.addAttribute("categories",categories);		
		model.addAttribute("embroideryItems", embroideryItems);
		return "embroideryItems";
	}

	@RequestMapping(value = "create-embroideryItem")
	public String createEmbroideryItemGet(Model model){
		model.addAttribute("embroideryItem", new EmbroideryItem());
		List<Category> categories = catService.getCategories();
		model.addAttribute("categories",categories);
		return "create-EmbroideryItem";
	}

	@RequestMapping(value = "create-embroideryItem", method = RequestMethod.POST)
	public String createEmbroideryItemPost(
			@ModelAttribute("embroideryItem") EmbroideryItem embroideryItem,
			@RequestParam CommonsMultipartFile fileUpload,@RequestParam CommonsMultipartFile jpegUpload,@RequestParam String[] rightValues) {

		try {
			
			String scfp = servletContext.getRealPath(File.pathSeparator.replaceAll(";", ""));
			scfp = scfp+"\\";
			String fl = "resources\\embfiles\\";
			String im = "resources\\imgfiles\\";
			String filePath = scfp+fl;
			String imgPath = scfp+im;
			
			File imgFile = new File(imgPath.replaceAll(";", "") + jpegUpload.getOriginalFilename());
			File embfile = new File(filePath.replaceAll(";", "") + fileUpload.getOriginalFilename());
			
			if (!jpegUpload.getOriginalFilename().equals("") & !fileUpload.getOriginalFilename().equals("") & rightValues != null) {
				jpegUpload.transferTo(imgFile);
				fileUpload.transferTo(embfile);

				List<CategoryItem> categoryItems = new ArrayList<CategoryItem>();
				
				for (String catName : rightValues) {
					Category category =catService.getCategoryForName(catName);
					CategoryItem item = new CategoryItem();
					item.setCatId(category);
					item.setItemId(embroideryItem);
					categoryItems.add(item);
				}
				
				embroideryItem.setCategoryItemList(categoryItems);
				embroideryItem.setFilePath(fl+fileUpload.getOriginalFilename());
				embroideryItem.setImagePath(im+jpegUpload.getOriginalFilename());
				
				emService.createEmbroideryItem(embroideryItem);
			}
			
				
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:create-embroideryItem";
	}
}