package com.inno.embroidery.embroideryItem.service;

import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class EmbroideryItemRepository {

	@SuppressWarnings("unchecked")
	public List<EmbroideryItem> getEmbroideryItems() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<EmbroideryItem> items = null;
		try {
			tx = session.beginTransaction();
			items = session.createQuery("FROM EmbroideryItem e").list();

			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return items;

	}

	
	
	public void createEmbroideryItem(EmbroideryItem embroideryItem) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();

			Date date = new Date();
			@SuppressWarnings("deprecation")
			java.sql.Date date2 = new java.sql.Date(date.getYear(),date.getMonth(),date.getDay());
			embroideryItem.setDate(date2);
			embroideryItem.setUserId(1);

			session.save(embroideryItem);

			List<CategoryItem> catList = embroideryItem.getCategoryItemList();
			if (catList != null) {
				for (CategoryItem catItem : catList) {
					session.save(catItem);
				}				
			}
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
	}

	public EmbroideryItem getEmbroideryItemforId(Integer id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		
		List<EmbroideryItem> items = null;
		EmbroideryItem item = null;
		try {
			tx = session.beginTransaction();

			Query query = session.createQuery("SELECT e FROM EmbroideryItem e WHERE e.id = ?");
			query.setParameter(0,id);
			items=query.list();
			
			for (EmbroideryItem embroideryItem : items) {
				item = embroideryItem;
			}
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		
		return item;		
	}

	public List<EmbroideryItem> getEmbroideryItemsBySearch(String keyword) {
		System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE EmbRepo method invoked..");
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<EmbroideryItem> items = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("SELECT e FROM EmbroideryItem e WHERE e.fileName like ?");
			query.setParameter(0,"%"+keyword+"%");
			items=query.list();
			
//			for (EmbroideryItem embroideryItem : items) {
//				System.out.println("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE EmbRepo item file names.."+embroideryItem.getFileName());				
//			}
//			
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}		
		
		return items;
	}
}