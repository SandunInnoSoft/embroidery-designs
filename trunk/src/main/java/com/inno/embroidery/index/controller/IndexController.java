package com.inno.embroidery.index.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.category.service.CategoryService;
import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.categoryItem.service.CategoryItemService;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.repository.EmbroideryItemService;

@Controller
public class IndexController {

	@Autowired
	protected EmbroideryItemService emService;
	
	@Autowired
	protected CategoryService catService;

	@Autowired
	protected CategoryItemService catItemService;
	
	@RequestMapping(value = { "/index" }, method=RequestMethod.GET)
	public String getEmbroideryItems(Model model,HttpServletRequest request) {
//		List<EmbroideryItem> embroideryItems = emService.getEmbroideryItems();
		
		List<EmbroideryItem> embItems = emService.getEmbroideryItems();
		List<EmbroideryItem> embroideryItems = new ArrayList<EmbroideryItem>();
		
		List<Category> categories = catService.getCategories();
		model.addAttribute("categories",categories);
		model.addAttribute("embroideryItems", embroideryItems);
				
		//check the item that it have a promotion
		for (EmbroideryItem embroideryItem : embItems) {

			if (true) { // has a promotion = true
				embroideryItem.setHasPromo(1);
			}else{
				embroideryItem.setHasPromo(0);
			}

			embroideryItems.add(embroideryItem);
		}

		model.addAttribute("embroideryItems", embroideryItems);
	
		setViewCartOpt(request,model);
		return "index";		
	}


	
	@RequestMapping(value = { "/itm&itmId={hid}" },method=RequestMethod.GET)
	public String getEmbroideryItem(Model model,HttpServletRequest request,@PathVariable String hid){
		EmbroideryItem embroideryItem = emService.getEmbroideryItemforId(Integer.parseInt(hid));
		String imgPath = embroideryItem.getImagePath();
		String newImgPath = imgPath.replace("\\", "/");
		embroideryItem.setImagePath("/"+newImgPath);
		model.addAttribute("embroideryItem", embroideryItem);

		setViewCartOpt(request,model);		
		return "single";
	}

	
	@RequestMapping(value = "index", method=RequestMethod.POST)
	public String getEmbroideryItemsForCategory(Model model,HttpServletRequest request,@RequestParam String name){
			
			model.addAttribute("embroideryItem", new EmbroideryItem());
			List<EmbroideryItem> embroideryItems = new ArrayList<EmbroideryItem>();
			
			Category category=catService.getCategoryForName(name);
			
			//get embroideryitem Ids for the catId from CategoryItem
			@SuppressWarnings("unchecked")
			List<CategoryItem> catItems = catItemService.getEmbroideryItemIdsForCategoryId(category);
			
			for (CategoryItem categoryItem : catItems) {
				EmbroideryItem item = emService.getEmbroideryItemforId(categoryItem.getItemId().getId());
				
				embroideryItems.add(item);
				
			}
			//get embroideryitems for itemIds
			model.addAttribute("embroideryItems", embroideryItems);

			setViewCartOpt(request,model);			
			return "index";					
	}
	
	
//	Not implemented yet(Search item option)................................................................................................................
	@RequestMapping(value = "search&id={kw}", method=RequestMethod.GET)
	public String getEmbroideryItemsBySearch(Model model,HttpServletRequest request,@PathVariable String kw,@RequestParam String keyword){

		List<EmbroideryItem> embroideryItems = emService.getEmbroideryItemsBySearch(keyword);
		
		List<Category> categories = catService.getCategories();
		model.addAttribute("categories",categories);
		model.addAttribute("embroideryItems", embroideryItems);
		if (keyword == "" | keyword == null | keyword.equals("enter your search terms")) {
			model.addAttribute("myKeyword",null);			
		} else {
			model.addAttribute("myKeyword",keyword);
		}
		setViewCartOpt(request,model);
		return "index";
	}
	
	
	public void setViewCartOpt(HttpServletRequest request,Model model){
			
			double totPrice = 0;
			int itemCount = 0;
			
			Enumeration<String> names = request.getSession().getAttributeNames();
			List<String> attrNames = new ArrayList<String>();
			
			for (;names.hasMoreElements();) {
				attrNames.add(names.nextElement());
			}
			
			for (String attrName : attrNames) {
				EmbroideryItem ssItm = null;
				Object obj = request.getSession().getAttribute(attrName);
	
				if (!obj.equals(null) & !(obj instanceof Double) & !(obj instanceof Integer)) {
					if (obj instanceof EmbroideryItem) {
						itemCount++;
					}
					ssItm = (EmbroideryItem) obj;
					totPrice = totPrice + ssItm.getPrice();
				}
			}
			
			request.getSession().setAttribute("totalPrice", totPrice);
			model.addAttribute("totalPrice",totPrice);		
	
			request.getSession().setAttribute("itemCount", itemCount);		
			model.addAttribute(itemCount);
	}
	
}