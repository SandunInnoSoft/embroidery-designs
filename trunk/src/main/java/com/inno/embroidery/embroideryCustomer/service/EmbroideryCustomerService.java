package com.inno.embroidery.embroideryCustomer.service;

import com.inno.embroidery.embroideryCustomer.model.EmbroideryCustomer;
import com.inno.embroidery.embroideryCustomer.repository.EmbroideryCustomerRepository;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmbroideryCustomerService {

	@Autowired
	protected EmbroideryCustomerRepository repository;
	
	public List<EmbroideryCustomer> getEmbroideryCustomers(){
		return repository.getEmbroideryCustomers();
	}
	
	public void createEmbroideryCustomer(EmbroideryCustomer e_mbroideryCustomer){
		repository.createEmbroideryCustomer(e_mbroideryCustomer);
	}
	
	public List<EmbroideryCustomer> getEmbroideryCustomersByUsername(EmbroideryCustomer e_mbroideryCustomer){
		return repository.getEmbroideryCustomersByUsername(e_mbroideryCustomer);
	}// End of getEmbroideryCustomersByUsername method
}
