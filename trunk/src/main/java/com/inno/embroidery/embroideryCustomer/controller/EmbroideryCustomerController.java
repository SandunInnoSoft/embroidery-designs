package com.inno.embroidery.embroideryCustomer.controller;

import com.inno.embroidery.embroideryCustomer.model.EmbroideryCustomer;
import com.inno.embroidery.embroideryCustomer.service.EmbroideryCustomerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class EmbroideryCustomerController {
	
	@Autowired
	protected EmbroideryCustomerService service;
	
	
	
	@RequestMapping(value = {"/embroideryCustomer"})
	public String getEmbroideryCustomers(Model m_odel){
		//List<EmbroideryCustomer> embroideryCustomers=service.getEmbroideryCustomers();
		
		m_odel.addAttribute("embroideryCustomer", new EmbroideryCustomer());
		System.out.println("getEmbroideryCustomers called+++++++++++++++++");
		return "embroideryCustomer";
	}
	
	@RequestMapping(value="create-EmbroideryCustomer")
	public String createEmbroideryCustomerGet(Model m_odel){
		m_odel.addAttribute("embroideryCustomer", new EmbroideryCustomer());
		return "create-EmbroideryCustomer";
	}
	
	@RequestMapping(value = "create-EmbroideryCustomer", method = RequestMethod.POST)
    public String createEmbroideryCustomerPost(@ModelAttribute("embroideryCustomer") EmbroideryCustomer e_mbroideryCustomer) {
        service.createEmbroideryCustomer(e_mbroideryCustomer);
        return "redirect:embroideryCustomers";
    }

}
