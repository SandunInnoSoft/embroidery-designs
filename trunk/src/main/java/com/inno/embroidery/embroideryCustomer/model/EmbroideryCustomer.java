package com.inno.embroidery.embroideryCustomer.model;
 
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name="customer", catalog="embroider", uniqueConstraints={@UniqueConstraint(columnNames="customerid")})
public class EmbroideryCustomer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="customerid")
	private int customerID;
	
	@Column(name="name")
	private String customerName;
	
	@Column(name="emailaddress")
	private String emailAddress;
	
	@Column(name="phonenumber")
	private String phoneNumber;
	
	@Column(name="password")
	private String password;
	
	@Column(name="date")
	private Date date;

	
	
	
	/**
	 * @return the customerID
	 */
	public int getCustomerID() {
		return customerID;
	}

	/**
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	/**
	 * @return the customerName
	 */
	public String getCustomerName() {
		return customerName;
	}

	/**
	 * @param customerName the customerName to set
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return the phoneNumber
	 */
	public String getPhoneNumber() {
		return phoneNumber;
	}

	/**
	 * @param phoneNumber the phoneNumber to set
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	/**
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate() {
		this.date = new Date();
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
	
	
	
	
	

	
}
