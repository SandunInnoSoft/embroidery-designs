package com.inno.embroidery.embroideryCustomer.repository;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryCustomer.model.EmbroideryCustomer;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

@Repository
@Transactional
public class EmbroideryCustomerRepository {

	
	@SuppressWarnings("unchecked")
	public List<EmbroideryCustomer> getEmbroideryCustomers(){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List customers = null;
	      try{
	         tx = session.beginTransaction();
	          customers = session.createQuery("FROM EmbroideryCustomer").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return customers;	
	}
	
	
	public void createEmbroideryCustomer(EmbroideryCustomer e_mbroideryCustomer){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	          session.save(e_mbroideryCustomer); 
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
		
	}// end of createEmbroideryCustomer method
	
	public List<EmbroideryCustomer> getEmbroideryCustomersByUsername(EmbroideryCustomer e_mbroideryCustomer){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List designers = null;
	      try{
	         tx = session.beginTransaction();
	          designers = session.createQuery("FROM EmbroideryCustomer WHERE emailAddress = '"+e_mbroideryCustomer.getEmailAddress()+"'").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return designers;
	} // end of getEmbroideryCustomersByUsername method
}
