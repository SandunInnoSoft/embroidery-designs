package com.inno.embroidery.embroideryAdmin.controller;



import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.inno.embroidery.users.model.EmbroiderUser;


@Controller
public class EmbroideryAdminController {

	@RequestMapping(value = {"/AdminPage"})
	public String getEmbroideryAdmins(){
		
		return "AdminPage";
	}
	
//	@RequestMapping(value = "/AdminPage")
//	public ModelAndView adminPage() {
//
//		ModelAndView model = new ModelAndView();
////		model.addObject("title", "Spring Security Hello World");
////		model.addObject("message", "This is protected page!");
//		model.setViewName("AdminPage");
//
//		return model;
//
//	}
	
//	@RequestMapping(value = { "/", "/welcome**" })
//	public ModelAndView welcomePage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Hello World");
//		model.addObject("message", "This is welcome page!");
//		model.setViewName("hello");
//		return model;
//	
//	}
	
//-------------- Added for the customized login window---------------
	
//	@RequestMapping(value = "/AdminPage**", method = RequestMethod.GET)
//	public ModelAndView adminPage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security + Hibernate Example");
//		model.addObject("message", "This page is for ROLE_ADMIN only!");
//		model.setViewName("AdminPage");
//
//		return model;
//
//	}
//	
//	@RequestMapping(value = "/login", method = RequestMethod.GET)
//	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
//			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {
//
//		ModelAndView model = new ModelAndView();
//		if (error != null) {
//			model.addObject("error", getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION"));
//		}
//
//		if (logout != null) {
//			model.addObject("msg", "You've been logged out successfully.");
//		}
//		model.setViewName("login");
//
//		return model;
//
//	}
//	
//	
//	@RequestMapping(value = "/login", method = RequestMethod.POST)
//	public ModelAndView loginSuccess(@RequestParam(value = "error", required = false) String error,
//			@RequestParam(value = "logout", required = false) String logout, HttpServletRequest request) {
//
//			
//		ModelAndView model = new ModelAndView();
//		System.out.println("error=================== " +error);
//		error = getErrorMessage(request, "SPRING_SECURITY_LAST_EXCEPTION");
//		System.out.println("error222=================== " +error);
//		if (error == null || error.trim().equals("")) {			
//			model.setViewName("AdminPage");			
//		} else {
//			model.setViewName("login");			
//		}
//		return model;
//	}
//	
//	// customize the error message
//	private String getErrorMessage(HttpServletRequest request, String key) {
//
//		Exception exception = (Exception) request.getSession().getAttribute(key);
//
//		String error = "";
//		if (exception instanceof BadCredentialsException) {
//			error = "Invalid username and password!";
//		} else if (exception instanceof LockedException) {
//			error = exception.getMessage();
//		} else {
//			error = "Invalid username and password!";
//		}
//
//		return error;
//	}
//
//	// for 403 access denied page
//	@RequestMapping(value = "/403", method = RequestMethod.GET)
//	public ModelAndView accesssDenied() {
//
//		ModelAndView model = new ModelAndView();
//
//		// check if user is login
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		if (!(auth instanceof AnonymousAuthenticationToken)) {
//			UserDetails userDetail = (UserDetails) auth.getPrincipal();
//			System.out.println(userDetail);
//
//			model.addObject("username", userDetail.getUsername());
//
//		}
//
//		model.setViewName("403");
//		return model;
//
//	}
//	
//---------------- for cutomized login window with hibernate----------


//	@RequestMapping(value = "/", method = RequestMethod.GET) 
//	public String displayLogin(Model model) { 
//	    model.addAttribute("login", new Login()); 
//	    return "login"; 
//	}
	
//	@RequestMapping("/login")
//	public String initForm(ModelMap model){
//		//return form view
//		return "login";
//	}
	
//	@RequestMapping("/login")
//	public ModelAndView getLoginForm(@ModelAttribute EmbroiderUser users,
//			@RequestParam(value = "error", required = false) String error,
//			@RequestParam(value = "logout", required = false) String logout) {
//		System.out.println("1111111111111111");
//		String message = "";
//		if (error != null) {
//			System.out.println("22222222222222222");
//			message = "Incorrect username or password !";
//		} else if (logout != null) {
//			System.out.println("333333333333333333333");
//			message = "Logout successful !";
//		}
//		return new ModelAndView("login", "message", message);
//	}
//
//	@RequestMapping("/AdminPage**")
//	public String getAdminProfile() {
//		System.out.println("44444444444444444444444");
//		return "AdminPage";
//	}
//
//	@RequestMapping("/hello**")
//	public String getUserProfile() {
//		System.out.println("5555555555555555555555");
//		return "hello";
//	}
//
//	@RequestMapping("/403")
//	public ModelAndView getAccessDenied() {
//		Authentication auth = SecurityContextHolder.getContext()
//				.getAuthentication();
//		String username = "";
//		if (!(auth instanceof AnonymousAuthenticationToken)) {
//			UserDetails userDetail = (UserDetails) auth.getPrincipal();
//			username = userDetail.getUsername();
//			System.out.println("666666666666666666666666");
//		}
//
//		return new ModelAndView("403", "username", username);
//	}
//	
}
