package com.inno.embroidery.login.service;

import com.inno.embroidery.users.model.*;

public interface LoginService {

	public boolean checkLogin(String userName, String userPassword);
}
