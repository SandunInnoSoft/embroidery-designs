package com.inno.embroidery.login.controller;

//import org.springframework.security.authentication.AnonymousAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.servlet.ModelAndView;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.beans.factory.annotation.Autowired;



import java.util.Map;

import javax.validation.Valid;

import com.inno.embroidery.login.model.LoginForm;
import com.inno.embroidery.login.service.*;
import com.inno.embroidery.users.model.EmbroiderUser;

//@Controller
@Controller
@RequestMapping("loginform.html")
public class LoginController {
	
	
//	@RequestMapping(value = { "/", "/welcome**" })
//	public ModelAndView welcomePage() {
//
//		ModelAndView model = new ModelAndView();
//		model.addObject("title", "Spring Security Hello World");
//		model.addObject("message", "This is welcome page!");
//		model.setViewName("hello");
//		return model;
//	
//	}
//
//	@RequestMapping("/login")
//	public ModelAndView getLoginForm(@ModelAttribute EmbroiderUser users,
//			@RequestParam(value = "error", required = false) String error,
//			@RequestParam(value = "logout", required = false) String logout) {
//		System.out.println("1111111111111111");
//		String message = "";
//		if (error != null) {
//			System.out.println("22222222222222222");
//			message = "Incorrect username or password !";
//		} else if (logout != null) {
//			System.out.println("333333333333333333333");
//			message = "Logout successful !";
//		}
//		return new ModelAndView("login", "message", message);
//	}
//
//	@RequestMapping("/AdminPage**")
//	public String getAdminProfile() {
//		System.out.println("44444444444444444444444");
//		return "AdminPage";
//	}
//
//	@RequestMapping("/hello**")
//	public String getUserProfile() {
//		System.out.println("5555555555555555555555");
//		return "hello";
//	}
//
//	@RequestMapping("/403")
//	public ModelAndView getAccessDenied() {
//		Authentication auth = SecurityContextHolder.getContext()
//				.getAuthentication();
//		String username = "";
//		if (!(auth instanceof AnonymousAuthenticationToken)) {
//			UserDetails userDetail = (UserDetails) auth.getPrincipal();
//			username = userDetail.getUsername();
//			System.out.println("666666666666666666666666");
//		}
//
//		return new ModelAndView("403", "username", username);
//	}
//	
	
	
	//----------------- for the new login ----------------------
	
	@Autowired
	public LoginService loginService;

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Map model) {
		System.out.println("12121212121222121212");
		LoginForm loginForm = new LoginForm();
		model.put("loginForm", loginForm);
		return "loginform";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String processForm(@Valid LoginForm loginForm, BindingResult result,
			Map model) {

		
		if (result.hasErrors()) {
			return "loginform";
		}
		
		/*
		String userName = "UserName";
		String password = "password";
		loginForm = (LoginForm) model.get("loginForm");
		if (!loginForm.getUserName().equals(userName)
				|| !loginForm.getPassword().equals(password)) {
			return "loginform";
		}
		*/
		boolean userExists = loginService.checkLogin(loginForm.getUserName(),loginForm.getPassword());
		if(userExists){
			model.put("loginForm", loginForm);
			return "loginsuccess";
		}else{
			result.rejectValue("userName","invaliduser");
			return "loginform";
		}

	}
	
	
	
	
}
