package com.inno.embroidery.login.dao;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;

import java.util.List;

import com.inno.embroidery.users.model.*;
import com.inno.embroidery.common.HibernateUtil;

@Repository("loginDAO")
public class LoginDAOImpl implements LoginDAO{

	
	   public boolean checkLogin(String userName, String userPassword){
				System.out.println("In Check login");
				Session session = HibernateUtil.getSessionFactory().openSession();
				boolean userFound = false;
				//Query using Hibernate Query Language
				String SQL_QUERY =" from EmbroiderUser as o where o.userName=? and o.userPassword=?";
				Query query = session.createQuery(SQL_QUERY);
				query.setParameter(0,userName);
				query.setParameter(1,userPassword);
				List list = query.list();

				if ((list != null) && (list.size() > 0)) {
					userFound= true;
				}

				session.close();
				return userFound;              
	       }
	
}
