package com.inno.embroidery.login.dao;

import com.inno.embroidery.users.model.*;

public interface LoginDAO {
	
	public boolean checkLogin(String userName, String userPassword);

}
