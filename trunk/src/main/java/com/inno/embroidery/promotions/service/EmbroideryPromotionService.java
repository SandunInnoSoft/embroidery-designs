package com.inno.embroidery.promotions.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.promotions.repository.EmbroideryPromotionRepository;
import com.inno.embroidery.users.model.EmbroiderUser;

/**
 * This class Holds communicate with Controller class and repository layer class For Promotion functionals.  
 * <p>
 * All the date packets from repository and to repository are transfer through the Service layer 
 *
 * @author 		VikumSam
 * @since       20/07/15
 
 *  */

@Service
public class EmbroideryPromotionService {
	
	@Autowired
	protected EmbroideryPromotionRepository repository;
	
	public List<EmbroideryPromotion> getEmbroideryPromotions(){
		
		return repository.getPromotions();
	}
	
	public void createEmbroideryPromotions(EmbroideryPromotion promotion){
		
		repository.createPromotion(promotion);		
		
	}
	public void updateEmbroideryPromotions(EmbroideryPromotion embroideryPromotion){
		
		repository.updateEmbroideryPromotion(embroideryPromotion);
	}
	public List<EmbroideryPromotion> getSearchEmbroideryPromotions(EmbroideryPromotion e_mbroiderPromotion){
		return repository.getSearchPromotions(e_mbroiderPromotion);
	}
	public List<EmbroideryPackage> getPackageList(){
		return repository.getPackages();
	}
	public List<EmbroideryPackage> getSelectedPack(String idValue){
		int id=Integer.parseInt(idValue);
		return repository.getSelected(id);
	}
	public List<EmbroideryItem> getReqItem(String id){
		int itemId=Integer.parseInt(id);
		return repository.getSelectedItems(itemId);
	}
}
