package com.inno.embroidery.promotions.controller;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;







import org.springframework.web.bind.annotation.RequestParam;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.promotions.model.*; 
import com.inno.embroidery.promotions.service.EmbroideryPromotionService;
//import com.mchange.v2.c3p0.impl.NewProxyStatement;


/**
 * Handles request and responses from Embroidery Promotion jsp pages and Service,Repository and Model Classes. 
 * <p>
 * From requesting JSP pages, this controls all the requests all request response functionals 
 *
 * @author 		VikumSam
 * @since       20/07/15
 
 *  */

@Controller
public class EmbroideryPromotionController {
	
	@Autowired
	protected EmbroideryPromotionService service;
	
	@RequestMapping(value={"/embroideryPromotions"})
	public String getPromotions(Model model){
		List<EmbroideryPromotion> embroideryPromotions=service.getEmbroideryPromotions();
		model.addAttribute("embroideryPromotions", embroideryPromotions);
		model.addAttribute(new EmbroideryPromotion());
		return "embroideryPromotions";
	}
	
	@RequestMapping(value="create-NewPromotion")
	public String createEmbroideryPromotionGet(Model model){
		model.addAttribute("embroideryPromotion", new EmbroideryPromotion());
		List<EmbroideryPackage> packages=service.getPackageList();
		model.addAttribute("packages", packages);
		return "create-NewPromotion";
	}
	
	@RequestMapping(value="create-NewPromotion",method= RequestMethod.POST)
	public String createEmbroideryPromotionPost(
			@ModelAttribute("embroideryPromotion") EmbroideryPromotion embroideryPromotion,@RequestParam String idStartDateSel,@RequestParam String idEndDateSel){
		if(embroideryPromotion.getPromoId()==0 && embroideryPromotion.getPromoActive()==0){			
			embroideryPromotion.setPromoStartDate(idStartDateSel);
			embroideryPromotion.setPromoEndDate(idEndDateSel);			
			embroideryPromotion.setPromoActive(1);
			String id=embroideryPromotion.getMode();
			String type=embroideryPromotion.getType();
			if(type.equals("package")){
				List<EmbroideryPackage> selPackage=service.getSelectedPack(id);
				EmbroideryPackage selectPackage=new EmbroideryPackage();
				for(Iterator<EmbroideryPackage> x=selPackage.iterator();x.hasNext();){
					selectPackage=x.next();
				}
				embroideryPromotion.setPromoPackage(selectPackage);
			}else if(type.equals("item")){
				List<EmbroideryItem> selItem=service.getReqItem(id);
				EmbroideryItem selectItem= new EmbroideryItem();
				for(Iterator< EmbroideryItem> i=selItem.iterator();i.hasNext();){
					selectItem = i.next();
				}
				embroideryPromotion.setPromoItem(selectItem);
			}
			
			service.createEmbroideryPromotions(embroideryPromotion);
			
		}else if(embroideryPromotion.getPromoId()!=0 && embroideryPromotion.getPromoActive()==1){
			
			embroideryPromotion.setPromoStartDate(idStartDateSel);
			embroideryPromotion.setPromoEndDate(idEndDateSel);	
			service.updateEmbroideryPromotions(embroideryPromotion);
		}
		else if(embroideryPromotion.getPromoId()!=0 && embroideryPromotion.getPromoActive()==0){
			
			embroideryPromotion.setPromoStartDate(idStartDateSel);
			embroideryPromotion.setPromoEndDate(idEndDateSel);	
			embroideryPromotion.setPromoActive(0);
			service.updateEmbroideryPromotions(embroideryPromotion);
		}
		return "redirect:create-NewPromotion";
	}
	
	@RequestMapping(value="embroideryPromotions",method=RequestMethod.POST )
	public String searchPackagePost(Model model,
			@ModelAttribute("embroideryPromotion") EmbroideryPromotion embroideryPromotion){		
		
		if(embroideryPromotion.getMode().equals("SEARCH")){
			List<EmbroideryPromotion> embroideryPromotions = service.getSearchEmbroideryPromotions(embroideryPromotion);
			model.addAttribute("embroideryPromotions", embroideryPromotions);
			
		}
		else{ }		
		return "embroideryPromotions";
	} 
}
