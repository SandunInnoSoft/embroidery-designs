package com.inno.embroidery.promotions.repository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.promotionhistory.model.PromotionHistory;
import com.inno.embroidery.promotions.model.EmbroideryPromotion;
import com.inno.embroidery.users.model.EmbroiderUser;

/**
 * This class Holds Database transaction layer 
 * <p>
 * This acts as the data access layer. All the database functions are written inside repository class. This class directly communicate with service layer class 
 *
 * @author 		VikumSam
 * @since       20/07/15
 
 *  */
@Repository
@Transactional
public class EmbroideryPromotionRepository {
	
	protected SessionFactory factory;
	
	@SuppressWarnings("deprecation")
	public List<EmbroideryPromotion> getPromotions(){
		
		List<EmbroideryPromotion> resPromotions=null;
		
		factory=new AnnotationConfiguration().
				configure().addAnnotatedClass(com.inno.embroidery.promotions.model.EmbroideryPromotion.class).buildSessionFactory();
		Session session = factory.openSession();		
	     Transaction tx = null;
	     try {
				tx=session.beginTransaction();
				resPromotions= session.createQuery("FROM EmbroideryPromotion ep Where active='1' ").list();
				tx.commit();
				
			} catch (HibernateException e) {
				if (tx!=null) tx.rollback();
		         e.printStackTrace();
				
			}finally{
				
				session.close();
			}
		
		return resPromotions;
	}
	
	public List<EmbroideryPackage> getSelected(int packageId){ 
		List<EmbroideryPackage> resultPackages=null;
		factory=HibernateUtil.getSessionFactory();
		
		Session session =factory.openSession();
		Transaction tx=null;
		
		try {
			
			tx=session.beginTransaction();
			resultPackages=session.createQuery("FROM EmbroideryPackage p where packageId='"+packageId+"' ").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{
			
			session.close();
		}
		
		return resultPackages;
		
	}
	
	public List<EmbroideryItem> getSelectedItems(int itmId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<EmbroideryItem> items = null;
		try {
			tx = session.beginTransaction();
			items = session.createQuery("FROM EmbroideryItem e where id='"+itmId+"'").list();

			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return items;

	}
	
	public List<EmbroideryPackage> getPackages(){ 
		List<EmbroideryPackage> resultPackages=null;
		factory=HibernateUtil.getSessionFactory();
		
		Session session =factory.openSession();
		Transaction tx=null;
		
		try {
			
			tx=session.beginTransaction();
			resultPackages=session.createQuery("FROM EmbroideryPackage p where active='1' ").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{
			
			session.close();
		}
		
		return resultPackages;
		
	}
	
	@SuppressWarnings("deprecation")
	public void createPromotion(EmbroideryPromotion promotion) {
		//factory.getCurrentSession().save(promotion);
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			promotion.setCreatedDate(date);
			tx=session.beginTransaction();
			session.save(promotion);
			tx.commit();
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }				
		
	}
	
	public void updateEmbroideryPromotion(EmbroideryPromotion embroideryPromotion ){
		factory=new AnnotationConfiguration().configure().addAnnotatedClass(com.inno.embroidery.promotions.model.EmbroideryPromotion.class).buildSessionFactory();
		Session session = factory.openSession();
	      Transaction tx = null;
	      addHistory(embroideryPromotion);
	      try{
	    	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				embroideryPromotion.setCreatedDate(date);
	         tx = session.beginTransaction();
	         System.out.println(embroideryPromotion.getPromoId());
	         session.update(embroideryPromotion); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }		
	}
	public List<EmbroideryPromotion> getSearchPromotions(EmbroideryPromotion e_embroideryPromotions){
		Session session=HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
	      List < EmbroideryPromotion> searchPromotions = null;
	      try{
	         tx = session.beginTransaction();
	      	 searchPromotions = session.createQuery("FROM EmbroideryPromotion p WHERE promoName LIKE '%"+e_embroideryPromotions.getPromoName()+"%'").list();
	         	         
	          tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return searchPromotions;
	}
	
	public List <EmbroideryPromotion> selectLastEntry(EmbroideryPromotion promo){
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		List <EmbroideryPromotion> getRecord=null;
		Transaction tx=null;
		try {
			tx=session.beginTransaction();
			getRecord=  session.createQuery("FROM EmbroideryPromotion p where promoId='"+promo.getPromoId()+"'").list();					
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
	return getRecord;
		
	}
	
	public void addHistory(EmbroideryPromotion embroideryPromotion ){
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		PromotionHistory promoHistory=new PromotionHistory();
		List <EmbroideryPromotion> promoInformation=selectLastEntry(embroideryPromotion);
		try{
			for(Iterator<EmbroideryPromotion> i= promoInformation.iterator();i.hasNext();){
				
				EmbroideryPromotion promoInfo = i.next();	
				promoHistory.setPromotionId(promoInfo.getPromoId());
				promoHistory.setPromoName(promoInfo.getPromoName());
				promoHistory.setPromoDescrip(promoInfo.getPromoDescrip());
				promoHistory.setPromoDiscount(promoInfo.getPromoDiscount());
				promoHistory.setStartDate(promoInfo.getPromoStartDate());
				promoHistory.setEndDate(promoInfo.getPromoEndDate());
				promoHistory.setCreatedDate(promoInfo.getCreatedDate());
				promoHistory.setActive(promoInfo.getPromoActive());				
			}
			
			tx=session.beginTransaction();
			session.save(promoHistory);
			tx.commit();
			System.out.println("History added!!!!!!!!!");
			}catch (HibernateException e) {
				 e.printStackTrace();
			}finally {
		         session.close(); 
		      }
			
		}
	
}
