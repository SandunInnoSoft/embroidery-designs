package com.inno.embroidery.promotions.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.packages.model.EmbroideryPackage;

/**
 * This class maps the fields with Database table. So it make easy to handle data with Model class 
 * <p>
 * Above each field the related table column has mapped. Since the annotation based hibernate mapping is used, the mapping is only handled by model class and Hibernate config 
 *
 * @author 		VikumSam
 * @since       20/07/15
 
 *  */

@Entity
@Table(name="promotions")
public class EmbroideryPromotion {
	
	@Id	@GeneratedValue
	@Column(name="id")
	private int promoId;
	
	@OneToOne(optional = false)
	@JoinColumn(name="packageid", referencedColumnName="packageid")
	private EmbroideryPackage promoPackage;
	
	
	@Column(name="startdate")
	private java.sql.Date promoStartDate;
	
	@Column(name="enddate")
	private java.sql.Date promoEndDate;
	
	@Column(name="discount")
	private double promoDiscount;
	
	@Column(name="active")
	private int promoActive;
	
	@Column(name="promoname")
	private String promoName;
	
	@JoinColumn(name = "item", referencedColumnName = "id")
	@OneToOne(optional = false)
	private EmbroideryItem promoItem;		
	
	@Column(name="promodescrip")
	private String promoDescrip;
	
	@Column(name="createddate")
	private Date createdDate;
	
	@Transient
	private String mode;
	
	@Transient
	private String type;
	
	
	
	
	

	

	public EmbroideryItem getPromoItem() {
		return promoItem;
	}

	public void setPromoItem(EmbroideryItem promoItem) {
		this.promoItem = promoItem;
	}

	public EmbroideryPromotion(){}

	public int getPromoId() {
		return promoId;
	}

	public void setPromoId(int promoId) {
		this.promoId = promoId;
	}

	public EmbroideryPackage getPromoPackage() {
		return promoPackage;
	}

	public void setPromoPackage(EmbroideryPackage promoPackage) {
		this.promoPackage = promoPackage;
	}

	public java.sql.Date getPromoStartDate() {
		return promoStartDate;
	}

//	public void setPromoStartDate(Date promoStartDate) {
//		this.promoStartDate=promoStartDate;
//	}
	
	public void setPromoStartDate(String promoStartDate) {	
		 
		Date date1 =null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		 
		try {
			date1 = formatter.parse(promoStartDate);
		} catch (ParseException e) {
			// 
			e.printStackTrace();
		}
		
		java.sql.Date date=new java.sql.Date(date1.getTime());
		
		this.promoStartDate = date;
	}

	public java.sql.Date getPromoEndDate() {
		return promoEndDate;
	}

	public void setPromoEndDate(String promoEndDate) {
		
		Date date1 =null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		 
		try {
			date1 = formatter.parse(promoEndDate);
		} catch (ParseException e) {
			// 
			e.printStackTrace();
		}
		
		java.sql.Date date=new java.sql.Date(date1.getTime());			
		
		this.promoEndDate = date;		
	}
	
//	public void setPromoEndDate(Date promoEndDate) {
//		this.promoEndDate=promoEndDate;
//		
//	}

	public double getPromoDiscount() {
		return promoDiscount;
	}

	public void setPromoDiscount(String promoDiscount) {
		this.promoDiscount = Double.parseDouble(promoDiscount);
	}
	public void setPromoDiscount(double promoDiscount) {
		this.promoDiscount = promoDiscount;
	}

	public int getPromoActive() {
		return promoActive;
	}
	
	public void setPromoActive(String promoActive) {
		this.promoActive =Integer.parseInt(promoActive);
	}

	public void setPromoActive(int promoActive) {
		this.promoActive = promoActive;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public String getPromoDescrip() {
		return promoDescrip;
	}

	public void setPromoDescrip(String promoDescrip) {
		this.promoDescrip = promoDescrip;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
