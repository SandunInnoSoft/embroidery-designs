package com.inno.embroidery.categoryHistory.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="categoryhistory")
public class CategoryHistory {
	
	@Id@GeneratedValue
	@Column(name="id")
	private int id;
	
	@Column(name="categoryid")
	private int categoryId;
	
	@Column(name="categoryname")
	private String categoryName;
	
	@Column(name="description")
	private String description;
	
	@Column(name="active")
	private int active;
	
	@Column(name="date")
	private Date date ;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
