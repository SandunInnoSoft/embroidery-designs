
package com.inno.embroidery.categoryItem.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.embroideryItem.model.EmbroideryItem;

/**
 *
 * @author Prabath
 */
@Entity
@Table(name = "category_item", catalog = "embroider", schema = "")
//@NamedQueries({
//    @NamedQuery(name = "CategoryItem.findById", query = "SELECT c FROM CategoryItem c WHERE c.id = :id")})
public class CategoryItem implements Serializable {
	
	private static final long serialVersionUID = 6741944073129741109L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @JoinColumn(name = "itemId", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private EmbroideryItem itemId;
    
    @JoinColumn(name = "catId", referencedColumnName = "categoryid")
    @ManyToOne(optional = false)
    private Category catId;

    public CategoryItem() {
    }

    public CategoryItem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EmbroideryItem getItemId() {
        return itemId;
    }

    public void setItemId(EmbroideryItem itemId) {
        this.itemId = itemId;
    }

    public Category getCatId() {
        return catId;
    }

    public void setCatId(Category catId) {
        this.catId = catId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CategoryItem)) {
            return false;
        }
        CategoryItem other = (CategoryItem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.inno.embroidery.categoryItem.model.CategoryItem[ id=" + id + " ]";
    }
    
}
