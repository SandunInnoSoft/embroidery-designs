package com.inno.embroidery.categoryItem.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.categoryItem.repository.CategoryItemRepository;

@Service
public class CategoryItemService {

	@Autowired
	protected CategoryItemRepository repository;

	public List<CategoryItem> getEmbroideryItemIdsForCategoryId(Category catId) {
		return repository.getEmbroideryItemIdsForCategoryId(catId);
	}
}
