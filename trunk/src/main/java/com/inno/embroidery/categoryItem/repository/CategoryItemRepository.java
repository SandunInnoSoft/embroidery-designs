package com.inno.embroidery.categoryItem.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.categoryItem.model.CategoryItem;
import com.inno.embroidery.common.HibernateUtil;

@Repository
@Transactional
public class CategoryItemRepository {

	public List<CategoryItem> getEmbroideryItemIdsForCategoryId(Category catId) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		
		List<CategoryItem> catItems = null;

		try {
			tx = session.beginTransaction();

			Query query = session.createQuery("SELECT c FROM CategoryItem c WHERE c.catId = ?");
			query.setParameter(0,catId);
			catItems=query.list();
			
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		
		return catItems;		
	}
	
}
