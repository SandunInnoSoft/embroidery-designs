package com.inno.embroidery.embroideryRequest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.inno.embroidery.embroideryRequest.model.EmbroideryRequest;
import com.inno.embroidery.embroideryRequest.repository.EmbroideryRequestRepository;

/**<h1>Service class for Embroidery Request</h1>
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */

@Service
public class EmbroideryRequestService {

	@Autowired
	protected EmbroideryRequestRepository repository;
	
	/**<b>Get Embroidery Requests</b>
	 * 
	 * @return
	 */
	public List<EmbroideryRequest> getEmbroideryRequests(){
		return repository.getEmbroideryRequests();
	}// end of getEmbroideryRequests method
	
	/**<b>Create an Embroidery Request</b>
	 * 
	 * @param e_mbroideryRequest
	 */
	public void createEmbroideryRequests(EmbroideryRequest e_mbroideryRequest){
		repository.createEmbroideryRequest(e_mbroideryRequest);
	}//  end of createEmbroideryRequests method
}// End of EmbroideryRequestService class
