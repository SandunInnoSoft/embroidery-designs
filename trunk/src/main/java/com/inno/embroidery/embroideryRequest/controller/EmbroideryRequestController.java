package com.inno.embroidery.embroideryRequest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;

import com.inno.embroidery.embroideryRequest.model.EmbroideryRequest;
import com.inno.embroidery.embroideryRequest.service.EmbroideryRequestService;

/**<h1>Controller class for EmbroideryRequest</h1>
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */

@Controller
public class EmbroideryRequestController implements ServletContextAware{

	private ServletContext servletContext;
	
	@Autowired
	protected EmbroideryRequestService service;
	
	/**
	 * 
	 * @param servletContext
	 */
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}
	
	/**<b>Get Embroidery Requests</b>
	 * 
	 * @param m_odel
	 * @return
	 */
	@RequestMapping(value = {"/embroideryRequests"})
	public String getEmbroideryRequests(Model m_odel){
		List<EmbroideryRequest> embroideryRequests=service.getEmbroideryRequests();
		m_odel.addAttribute("embroideryRequests", embroideryRequests);
		System.out.println("getEmbroideryRequests called +++++++");
		return "embroideryRequests";
	}// End of getEmbroideryRequests method
	
	/**<b>Create Embroidery Request using GET</b>
	 * 
	 * @param m_odel
	 * @return
	 */
	@RequestMapping(value = "create-EmbroideryRequest")
	public String createEmbroideryRequestGet(Model m_odel){
		m_odel.addAttribute("embroideryRequest", new EmbroideryRequest());
		return "create-EmbroideryRequest";
	}// End of createEmbroideryRequestGet method
	
	/**<b>Create Embroidery Request using POST</b>
	 * 
	 * @param e_mbroideryRequest
	 * @return
	 */
	@RequestMapping(value = "create-EmbroideryRequest", method = RequestMethod.POST)
    public String createEmbroideryRequestPost(@ModelAttribute("embroideryRequest") EmbroideryRequest e_mbroideryRequest,
    	@RequestParam CommonsMultipartFile jpegUpload) {
		
		
		try {
			
			
			String scFilePath = servletContext.getRealPath(File.pathSeparator);
//			String filePath = scFilePath+"uploads\\jpegs\\";
			
			String imgPath = scFilePath;
			
			File imgFile = new File(imgPath + jpegUpload.getOriginalFilename());
			
			
			if (!jpegUpload.getOriginalFilename().equals("")) {
				jpegUpload.transferTo(imgFile);
				
				System.out.println("Saved files:"
						+ imgFile.getAbsolutePath());
				String path=imgFile.getAbsolutePath();
				e_mbroideryRequest.setImageFilePath(path);
			}
			
				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
        service.createEmbroideryRequests(e_mbroideryRequest);
     return "redirect:embroideryRequests";
    }// End of createEmbroideryRequestPost method
	
}// End of EmbroideryRequestController class
