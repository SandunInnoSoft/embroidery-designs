package com.inno.embroidery.embroideryRequest.repository;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.embroideryRequest.model.EmbroideryRequest;


/**<h1>Repository class for Embroidery Request</h1>
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */
@Repository
@Transactional
public class EmbroideryRequestRepository {

	@SuppressWarnings("unchecked")
	
	/**<b>Get Embroidery Requests</b>
	 * This method returns a list of EmbroideryRequest objects fetched from the database
	 * @return
	 */
	public List<EmbroideryRequest> getEmbroideryRequests(){
		//return sessionFactory.getCurrentSession().createQuery("FROM EmbroideryDesigner e").list();
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List requests = null;
	      try{
	         tx = session.beginTransaction();
	          requests = session.createQuery("FROM EmbroideryRequest").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return requests;
	} // end of getEmbroideryRequests method
	
	/**<b>Create an Embroidery Request</b>
	 * This method will create a new entry of an embroidery request in the database
	 * 
	 * @param e_mbroideryRequest
	 */
	public void createEmbroideryRequest(EmbroideryRequest e_mbroideryRequest){
		//HibernateUtil.getSessionFactory().getCurrentSession().save(e_mbroideryDesigner);
		//sessionFactory.getCurrentSession().save(e_mbroideryDesigner);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	          session.save(e_mbroideryRequest); 
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
		
	}// End of createEmbroideryRequest method
	
}// End of EmbroideryRequestRepository class
