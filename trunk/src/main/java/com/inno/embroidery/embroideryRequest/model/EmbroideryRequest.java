package com.inno.embroidery.embroideryRequest.model;

import java.util.Date;

import javax.persistence.*;

import org.springframework.web.multipart.MultipartFile;

/**<h1>Model class of EmbroideryRequest</h1>
 * <p>This class contains annotations, methods and attributes to store and retrieve embroidery request data in the database
 * 	using hibernate.
 *  </p>
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */

@Entity
@Table(name="embroideryrequest", catalog="embroider", uniqueConstraints={@UniqueConstraint(columnNames="requestid")})
public class EmbroideryRequest {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="requestid")
	private int requestID;

	@Column(name="imagefilepath")
	private String imageFilePath;
	
	@Column(name="width")
	private float width;
	
	@Column(name="height")
	private float height;
	
	@Column(name="description")
	private String description;
	
	@Column(name="customerid")
	private int customerID; // this is test only, later this should change into a customer class type
	
	@Column(name="date")
	private Date date;

	/**<b>Request ID Getter Method</b>
	 * This method returns the Embroider Request ID
	 * @return the requestID
	 */
	public int getRequestID() {
		return requestID;
	}

	/**<b>Request ID Setter Method</b>
	 * This method sets the Embroider Request ID as the value passed as parameter
	 * @param requestID the requestID to set
	 */
	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}

	/**<b>Uploaded image file path of the Request Getter Method</b>
	 * This method returns the uploaded image file path of the embroidery request
	 * @return the imageFilePath
	 */
	public String getImageFilePath() {
		return imageFilePath;
	}

	/**<b>Uploaded image file path of the request Setter Method</b>
	 * This method sets the file path of Embroider Request upload as the value passed as parameter
	 * @param imageFilePath the imageFilePath to set
	 */
	public void setImageFilePath(String imageFilePath) {
		this.imageFilePath = imageFilePath;
	}

	/**<b>Width of the Request Getter Method</b>
	 * This method returns the width required by the customer through a particular request
	 * @return the width
	 */
	public float getWidth() {
		return width;
	}

	/**<b>Width of the Request Setter Method</b>
	 * This method sets the width required by the customer in this Embroider Request as the value passed as parameter
	 * @param width the width to set
	 */
	public void setWidth(float width) {
		this.width = width;
	}

	/**<b>Height of the Request Getter Method</b>
	 * This method returns the height required by the customer through a particular request
	 * @return the height
	 */
	public float getHeight() {
		return height;
	}

	/**<b>Height of the Request Setter Method</b>
	 * This method sets the height required by the customer in this Embroider Request as the value passed as parameter
	 * @param height the height to set
	 */
	public void setHeight(float height) {
		this.height = height;
	}

	/**<b>Request Description Getter Method</b>
	 * This method returns the Embroider Request Description
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**<b>Request description Setter Method</b>
	 * This method sets the Embroider Request description as the value passed as parameter
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**<b>Customer ID Getter Method</b>
	 * This method returns the customer ID of the customer who placed the request
	 * @return the customerID
	 */
	public int getCustomerID() {
		return customerID;
	}

	/**<b>Customer ID Setter Method</b>
	 * This method sets the Id of the customer who placed the Embroider Request as the value passed as parameter
	 * @param customerID the customerID to set
	 */
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

	/**<b>Request ID date placed Getter Method</b>
	 * This method returns the date of the request placed in the database
	 * @return the date
	 */
	public Date getDate() {
		return date;
	}

	/**<b>Request placed date Setter Method</b>
	 * This method sets the date created of particular Embroider Request entry as the value passed as parameter
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
	
}
