package com.inno.embroidery.single.controller;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.repository.EmbroideryItemService;

@Controller
public class SingleController {

	@Autowired
	EmbroideryItemService emService;
	
	@RequestMapping(value = "multTtms&itid={embItemId}", method=RequestMethod.GET)
	public String getMultipleCheckout(Model model,HttpServletRequest request,@PathVariable String embItemId){
		model.addAttribute("embroideryItem", new EmbroideryItem());
		List<EmbroideryItem> embroideryItems = new ArrayList<EmbroideryItem>();
		
		double totPrice = 0;
		int itemCount = 0;
		
		EmbroideryItem item = emService.getEmbroideryItemforId(Integer.parseInt(embItemId));
		
		request.getSession().setAttribute(embItemId, item);		
		Enumeration<String> names = request.getSession().getAttributeNames();
		List<String> attrNames = new ArrayList<String>();
		
		for (;names.hasMoreElements();) {
			attrNames.add(names.nextElement());
		}
		
		for (String attrName : attrNames) {
			EmbroideryItem ssItm = null;
			Object obj = request.getSession().getAttribute(attrName);

			if (!obj.equals(null) & !(obj instanceof Double) & !(obj instanceof Integer)) {
				if (obj instanceof EmbroideryItem) {
					itemCount++;
				}
				ssItm = (EmbroideryItem) obj;
				totPrice = totPrice + ssItm.getPrice();
				
				String imgPath = ssItm.getImagePath();
				String newImgPath = imgPath.replace("\\", "/");
				ssItm.setImagePath("/"+newImgPath);				
				embroideryItems.add(ssItm);
			}
		}
		
		request.getSession().setAttribute("totalPrice", totPrice);
		model.addAttribute("totalPrice",totPrice);		

		request.getSession().setAttribute("itemCount", itemCount);		
		model.addAttribute(itemCount);
		
		model.addAttribute("embroideryItems", embroideryItems);
		
		return "checkout";		
	}

	
	
}
