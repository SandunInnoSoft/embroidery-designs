package com.inno.embroidery.promotionhistory.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="promotionhistory")
public class PromotionHistory {
	
	@Id @GeneratedValue
	@Column(name="idpromotionhistory")
	private int id;
	
	@Column(name="idpromotion")
	private int promotionId;
	
	@Column(name="promoname")
	private String promoName;
	
	@Column(name="promodescrip")
	private String promoDescrip;
	
	@Column(name="stardate")
	private Date startDate;
	
	@Column(name="enddate")
	private Date  endDate;
	
	@Column(name="discount")
	private double promoDiscount;
	
	@Column(name="active")
	private int active;
	
	@Column(name="createddate")
	private Date createdDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public String getPromoDescrip() {
		return promoDescrip;
	}

	public void setPromoDescrip(String promoDescrip) {
		this.promoDescrip = promoDescrip;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public double getPromoDiscount() {
		return promoDiscount;
	}

	public void setPromoDiscount(double promoDiscount) {
		this.promoDiscount = promoDiscount;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	} 
	
}
