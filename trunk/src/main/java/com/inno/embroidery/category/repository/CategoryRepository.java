package com.inno.embroidery.category.repository;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hsqldb.rights.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.categoryHistory.model.CategoryHistory;
import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.packagehistory.model.PackageHistory;
import com.inno.embroidery.packages.model.EmbroideryPackage;

@Repository
@Transactional
public class CategoryRepository {
	
	public CategoryRepository(){
		
	}

	@SuppressWarnings("unchecked")
	public List<Category> getCategories() {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<Category> categories = null;
		try {
			tx = session.beginTransaction();
			categories = session.createQuery("FROM Category c").list();

			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return categories;
		// return factory.getCurrentSession()
		// .createQuery("FROM Category c")
		// .list();
	}

	public void createCategory(Category category) {
		// factory.getCurrentSession().save(category);

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();			
			dateFormat.format(date);
			category.setDate(date);
			tx = session.beginTransaction();
			session.save(category);
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public Category getCategoryForName(String catName) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<Category> categories = null;
		Category category = null;
		try {
			tx = session.beginTransaction();
//			categories = session.createQuery("SELECT c FROM Category c WHERE c.catName = :"+catName).list();
			Query query = session.createQuery("SELECT c FROM Category c WHERE c.catName = ?");
			query.setParameter(0,catName);
			categories=	query.list();
			for (Category c : categories) {
				category = c;
			}
			tx.commit();
		} catch (HibernateException s) {
			if (tx != null)
				tx.rollback();
			s.printStackTrace();
			System.exit(0);
		} finally {
			session.close();
		}
		return category;
	}
	
	public List <Category> selectLastEntry(Category cat){		
		Session session = HibernateUtil.getSessionFactory().openSession();
		List <Category> getRecord=null;
		Transaction tx=null;
		
		try {
			tx=session.beginTransaction();
			getRecord=  session.createQuery("FROM Category c where id='"+cat.getId()+"'").list();					
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
	return getRecord;
		
	}
	
	public void addHistory(Category category){
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx=null;
		CategoryHistory categoryHistory=new CategoryHistory();
		List <Category> userInformation =selectLastEntry(category);		
		try {	
			for(Iterator<Category> i= userInformation.iterator();i.hasNext();){
				
				Category categoryInfo = i.next();				
				categoryHistory.setCategoryId(categoryInfo.getId());
				categoryHistory.setCategoryName(categoryInfo.getCatName());
				categoryHistory.setDescription(categoryInfo.getDescription());
				categoryHistory.setActive(categoryInfo.getActive());
				categoryHistory.setDate(categoryInfo.getDate());
				
				
			tx=session.beginTransaction();
			session.save(categoryHistory);
			tx.commit();
			
			}
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }
		
	}
	
	public void updateCategories(Category category){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		addHistory(category);
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			dateFormat.format(date);
			category.setDate(date);
			tx=session.beginTransaction();
			
			session.update(category);
			tx.commit();
			
		}catch (HibernateException e) {			
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }	
	}
}
