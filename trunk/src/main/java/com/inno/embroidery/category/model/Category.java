package com.inno.embroidery.category.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.inno.embroidery.categoryItem.model.CategoryItem;

@Entity
@Table
public class Category implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -6945415933353252338L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="categoryid")
    private int id;
 
    @Column(name="categoryname", nullable = false, length = 255)
    private String catName;
 
    @Column(name="description", nullable = false, length = 255)
    private String description;
    
    @Column(name="active")
    private int active;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catId")
    private List<CategoryItem> categoryItemList;
    
    @Column(name="date")
    private Date date;
    
    @Transient
    private String mode;
    
    public int getId() {
		return id;
	}
    
    public String getCatName() {
		return catName;
	}
    
    public String getDescription() {
		return description;
	}
	
    
    public void setId(int id) {
		this.id = id;
	}
    
    public void setCatName(String catName) {
		this.catName = catName;
	}
    
    public void setDescription(String description) {
		this.description = description;
	}
	
    public void setCategoryItemList(List<CategoryItem> categoryItemList) {
		this.categoryItemList = categoryItemList;
	}

    public List<CategoryItem> getCategoryItemList() {
		return categoryItemList;
	}

	public int getActive() {
		return active;
	}

	public void setActive(String active){
		this.active=Integer.parseInt(active);
	}

	public void setActive(int active) {
		this.active = active;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
    
}