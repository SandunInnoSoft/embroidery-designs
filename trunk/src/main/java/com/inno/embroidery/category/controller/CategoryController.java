package com.inno.embroidery.category.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.category.service.CategoryService;

@Controller
public class CategoryController {

	@Autowired
	protected CategoryService service;

	@RequestMapping(value = { "/categories" })
	public String getCategories(Model model ) {
		List<Category> categories = service.getCategories();
		model.addAttribute("categories", categories);
		model.addAttribute(new Category());
		return "categories";
	}

	@RequestMapping(value = "createCategory")
	public String createCategoryGet(Model model) {
		model.addAttribute("category", new Category());
		return "createCategory";
	}

	@RequestMapping(value = "createCategory", method = RequestMethod.POST)
	public String createCategoryPost(
			@ModelAttribute("category") Category category) {
		if(category.getId()==0 && category.getActive()==0){
				category.setActive(1);
				service.createCategory(category);
		}
		else if(category.getId()!=0 && category.getActive()==1){
			service.updateCat(category);
		}
		else if(category.getId()!=0 && category.getActive()==0){
			category.setActive(0);
			service.updateCat(category);
		}
		
		return "redirect:createCategory";
	}
}
