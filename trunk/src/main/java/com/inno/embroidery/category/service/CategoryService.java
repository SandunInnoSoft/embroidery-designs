package com.inno.embroidery.category.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inno.embroidery.category.model.Category;
import com.inno.embroidery.category.repository.CategoryRepository;

@Service
public class CategoryService {


	protected CategoryRepository categoryRepository = new CategoryRepository();
	
    public List<Category> getCategories() {
        return categoryRepository.getCategories();
//    	return new ArrayList();
    }
 
    public void createCategory(Category category) {
    	categoryRepository.createCategory(category);
    }

	public Category getCategoryForName(String catName) {
       return categoryRepository.getCategoryForName(catName);
		
	}
	
	public void updateCat(Category category){
		categoryRepository.updateCategories(category);
	}
}
