package com.inno.embroidery.userhistory.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="userhistory")
public class UserHistory {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="userhistoryid")
	private int id;
	
	@Column(name="userid")
	private int userId;
	
	@Column(name="fullname")
	private String fullName;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="useremail")
	private String userEmail;
	
	@Column(name="address")
	private String userAddress;
	
	@Column(name="date")
	private Date date;
	
	@Column(name="passwordx")
	private String userPassword;
	
	@Column(name="active")
	private int active;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

//	public int getCreatedUserId() {
//		return createdUserId;
//	}
//
//	public void setCreatedUserId(int createdUserId) {
//		this.createdUserId = createdUserId;
//	}

	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public int getActive() {
		return active;
	}

	public void setActive(int active) {
		this.active = active;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
