package com.inno.embroidery.embroideryDesigner.controller;

import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.embroideryDesigner.service.EmbroideryDesignerService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**<h1>Controller class of Embroider Designer</h1>
 * This class contains annotations, attributes and methods to map the requests 
 * 
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-7
 */
@Controller
public class EmbroideryDesignerController {

	@Autowired
	protected EmbroideryDesignerService service;
	
	/**
	 * <b>Output a list of embroidery designers fetched from the database</b>
	 * This method will be called to display the list on the embroideryDesigners.jsp page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/embroideryDesigners"})
	public String getEmbroideryDesigners(Model model){
		List<EmbroideryDesigner> embroideryDesigners=service.getEmbroideryDesigners();
		model.addAttribute("embroideryDesigners", embroideryDesigners);
		model.addAttribute("embroideryDesigner", new EmbroideryDesigner());
		System.out.println("getEmbroideryDesigners called +++++++");
		return "embroideryDesigners";
	}// End of getEmbroideryDesigners method
	
//	@RequestMapping(value = {"/embroideryDesigners"})
//	public String getEmbroideryDesigners(Model model,EmbroideryDesigner e_mbroideryDesigner){
//		List<EmbroideryDesigner> embroideryDesigners=service.getEmbroideryDesigners(e_mbroideryDesigner);
//		model.addAttribute("embroideryDesigners", embroideryDesigners);
//		System.out.println("filtered getEmbroideryDesigners called +++++++");
//		return "embroideryDesigners";
//	}// End of getEmbroideryDesigners method
	
	
	/**
	 * <b>Create a new designer record in the database</b>
	 * This method will be invoked to add a new designer record in the database if the submit form in the create-EmbroideryDesigner.jsp page used GET method
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "create-EmbroideryDesigner")
	public String createEmbroideryItemGet(Model model){
		model.addAttribute("embroideryDesigner", new EmbroideryDesigner());
		return "create-EmbroideryDesigner";
	}// End of createEmbroideryItemGet method
	
//	@RequestMapping(value = "create-EmbroideryDesigner", method = RequestMethod.POST)
//    public String createEmbroideryDesignerPost(@ModelAttribute("embroideryDesigner") EmbroideryDesigner e_mbroideryDesigner) {
//        service.createEmbroideryDesigner(e_mbroideryDesigner);
//        return "redirect:create-EmbroideryDesigner";
//    }// End of createEmbroideryDesignerPost method
	
	
	/**
	 * <b>Create, update and delete a designer record in the database</b>
	 * This method will create a new embroidery designer record, update or delete an existing embroidery designer record in the database
	 * id the submit from in the create-Embroidery designer.jsp page used POST method. <br>
	 * This will use the value of the "active" attribute in the Embroidery Designer object to select which database operation to be executed
	 * on the data of the received embroidery designer object 
	 * @param e_mbroiderDesigner
	 * @return
	 */
	@RequestMapping(value="create-EmbroideryDesigner",method=RequestMethod.POST )
	public String manageDesignersPost(Model model,
			@ModelAttribute("embroideryDesigner") EmbroideryDesigner e_mbroiderDesigner){
		System.out.println("*********************************************");
		System.out.println("+++++++++++++ designer ID="+e_mbroiderDesigner.getId()+" active state = "+e_mbroiderDesigner.getActive());
		
		String mode=e_mbroiderDesigner.getMode();
		String returnText=null;
		System.out.println("++++++++++++++++MODE = "+mode);
		if(mode.equals("CREATE")){
			List<EmbroideryDesigner> existingDesigners = service.getEmbroideryDesignersByNameAndPhoneNumber(e_mbroiderDesigner);
			//Precheck if the a designer with the same name exists in the database 
			if(existingDesigners.size()==0){
				if (e_mbroiderDesigner.getId()==0 && e_mbroiderDesigner.getActive()==0)//&& e_mbroiderDesigner.getSearch()==0 
				{
					
				 System.out.println("*****Create Embroidery designer called");
				 e_mbroiderDesigner.setActive(1);	
				 service.createEmbroideryDesigner(e_mbroiderDesigner);
				 returnText="redirect:create-EmbroideryDesigner";
				}
			}else{
				System.out.println("*********Designer exists in the database");
			}
			
		}else if(mode.equals("UPDATE")){
			//Precheck if such designer exists in the database
			if(e_mbroiderDesigner.getId()!=0 && e_mbroiderDesigner.getActive()==1){
				List<EmbroideryDesigner> existingDesigners = service.getEmbroideryDesignersByNameAndPhoneNumber(e_mbroiderDesigner);
				if(existingDesigners.size()==0){
					System.out.println("No Such record exists in the database!");
				}else{
				
				System.out.println("******Update Embroidery designer called");
				 service.updateEmbroideryDesigner(e_mbroiderDesigner);
				 
				 returnText="redirect:create-EmbroideryDesigner";
				}
			}
			
		}else if(mode.equals("DELETE")){
			System.out.println("***************Delete designer called");
			 service.deleteEmbroideryDesigner(e_mbroiderDesigner);
			 returnText="redirect:create-EmbroideryDesigner";
			
		}else{
			System.out.println("******************************Invalid designer mode******");
		}
		
		 
		return returnText;	
	}// End of manageDesignersPost method
	
	
	/**
	 * 
	 * @param model
	 * @param e_mbroiderDesigner
	 * @return
	 */
	@RequestMapping(value="embroideryDesigners",method=RequestMethod.POST )
	public String manageDesignersFromListPost(Model model,
			@ModelAttribute("embroideryDesigner") EmbroideryDesigner e_mbroiderDesigner){
		String mode=e_mbroiderDesigner.getMode();
		System.out.println("++++++++++++++++MODE = "+mode);
		if(mode.equals("SEARCH")){
			System.out.println("**********************Searching called");
			 
			List<EmbroideryDesigner> embroideryDesigners=service.getEmbroideryDesigners(e_mbroiderDesigner);
			model.addAttribute("embroideryDesigners", embroideryDesigners);
    		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++Came here");
    		System.out.println("+++++++++++++from embroiderydesigners++++ Size = "+embroideryDesigners.size());
			
		}else{
			System.out.println("******************************Invalid designer mode******");
		}
		
		return "embroideryDesigners";	
	}// End of manageDesignersPost method
	
	
	
}// End of  EmbroideryDesignerController class
