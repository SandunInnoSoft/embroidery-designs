package com.inno.embroidery.embroideryDesigner.model;

import java.util.Date;

import javax.persistence.*;


/**
 * <h1>Model class of Embroidery Designer</h1>
 * <p>This class contains the methods and attributes of the Embroidery designer
 *    bean class and the annonations to link the database table and columns of
 *    the mysql database through hibernate.
 * </p>
 * @author Sandun Munasinghe
 * @since  2015-7-7
 * 
 *
 */
@Entity
@Table(name="designer", catalog="embroider", uniqueConstraints={@UniqueConstraint(columnNames="designerid")})
public class EmbroideryDesigner {
	
	private int id; // stores designer ID
	
	
	private String designerName; // stores designer name
	
	
	private String emailAddress1; // stores email address 1

	
	private String emailAddress2; // stores email address 2
	
	
	private String phoneNumber1; // stores phone number 1
	
	
	private String phoneNumber2; // stores phone number 2
	
	
	private String description; // stores the description about the designer
	
	
	private String address; // stores the address of the designer
	
	
	private int userid; // stores the reference of the admin user who created this particular designer 
	
	
	private Date date; // date of the designer record created
	
	private int active; // To represent the state of the designer record
	
	private String mode; 

	
	


	/**
	 * <b>Designer ID getter method</b>
	 * This method return the designer ID
	 * 
	 * @return the id
	 */
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="designerid")
	public int getId() {
		return id;
	}

	/**<b>Designer ID setter method</b>
	 * This method sets the Designer ID to the value passsed as param
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**<b>Designer Name getter method</b>
	 * This method return the designer name
	 * @return the designerName
	 */
	
	@Column(name="designername")
	public String getDesignerName() {
		return designerName;
	}

	/**<b>Designer Name setter method</b>
	 * This method sets the Designer name to the value passsed as param
	 * @param designerName the designerName to set
	 */
	public void setDesignerName(String designerName) {
		this.designerName = designerName;
	}

	/**<b>Designer email address 1 getter method</b>
	 * This method return the designer email address 1
	 * @return the emailAddress1
	 */
	
	@Column(name="emailaddress1")
	public String getEmailAddress1() {
		return emailAddress1;
	}

	/**<b>Designer Email Address 1 setter method</b>
	 * This method sets the Designer email address 1 to the value passsed as param
	 * @param emailAddress1 the emailAddress1 to set
	 */
	public void setEmailAddress1(String emailAddress1) {
		this.emailAddress1 = emailAddress1;
	}

	/**<b>Designer email address 2 getter method</b>
	 * This method return the designer email address 2
	 * @return the emailAddress2
	 */
	
	@Column(name="emailaddress2")
	public String getEmailAddress2() {
		return emailAddress2;
	}

	/**<b>Designer Email Address 2 setter method</b>
	 * This method sets the Designer email address 2 to the value passsed as param
	 * @param emailAddress2 the emailAddress2 to set
	 */
	public void setEmailAddress2(String emailAddress2) {
		this.emailAddress2 = emailAddress2;
	}

	/**<b>Designer phone number getter method</b>
	 * This method return the designer phone number
	 * @return the phoneNumber1
	 */
	
	@Column(name="phonenumber1")
	public String getPhoneNumber1() {
		return phoneNumber1;
	}

	/**<b>Designer Phone Number 1 setter method</b>
	 * This method sets the Designer phone number 1 to the value passsed as param
	 * @param phoneNumber1 the phoneNumber1 to set
	 */
	public void setPhoneNumber1(String phoneNumber1) {
		this.phoneNumber1 = phoneNumber1;
	}

	/**<b>Designer Phone Number 2 getter method</b>
	 * This method return the designer phone number 2
	 * @return the phoneNumber2
	 */
	
	@Column(name="phonenumber2")
	public String getPhoneNumber2() {
		return phoneNumber2;
	}

	/**<b>Designer Phone Number 2 setter method</b>
	 * This method sets the Designer phone number 2 to the value passsed as param
	 * @param phoneNumber2 the phoneNumber2 to set
	 */
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}

	/**<b>Designer Description getter method</b>
	 * This method return the designer description
	 * @return the description
	 */
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}

	/**<b>Designer Description setter method</b>
	 * This method sets the Designer description to the value passsed as param
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**<b>Designer Address getter method</b>
	 * This method return the designer address
	 * @return the address
	 */
	
	@Column(name="address")
	public String getAddress() {
		return address;
	}

	/**<b>Designer Address setter method</b>
	 * This method sets the Designer address to the value passsed as param
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**<b>Designer Admin User ID getter method</b>
	 * This method return the designer admin user ID
	 * @return the userid
	 */
	
	@Column(name="userid")
	public int getUserid() {
		return userid;
	}

	/**<b>Designer Admin User ID setter method</b>
	 * This method sets the Designer admin user ID to the value passsed as param
	 * @param userid the userid to set
	 */
	public void setUserid(int userid) {
		this.userid = userid;
	}

	/**<b>Designer Date of creation getter method</b>
	 * This method return the designer date of its creation
	 * @return the date
	 */
	
	@Column(name="date")
	public Date getDate() {
		return date;
	}

	/**<b>Designer Date of Creation setter method</b>
	 * This method sets the Designer date of creation to the value passsed as param
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**<b>State of the designer record getter method</b>
	 * This method returns a value which will be represented as the current state of the a particular record.
	 * @return the active
	 */
	@Column(name="active")
	public int getActive() {
		return active;
	}

	/**<b>State of the designer record setter method</b>
	 * This method sets a value passed as the parameter which will be represented as the current state of the a particular record.
	 * @param active the active to set
	 */
	public void setActive(int active) {
		this.active = active;
	}

	/**
	 * @return the mode
	 */
	@Transient
	public String getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	
	

}// End of EmbroideryDesigner class
