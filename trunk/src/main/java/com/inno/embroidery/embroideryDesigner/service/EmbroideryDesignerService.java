package com.inno.embroidery.embroideryDesigner.service;

import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.embroideryDesigner.repository.EmbroideryDesignerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**<h1>Service class of Embroider Designer</h1>
 * This class contains attributes and methods to provide service.
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-7
 *
 */

@Service
public class EmbroideryDesignerService {

	@Autowired
	protected EmbroideryDesignerRepository repository;
	
	/**
	 * <b>Retrieve embroidery designers</b>
	 * This method will retrieve embroidery designers from the database using a method from the repository class
	 * 
	 * @return
	 */
	public List<EmbroideryDesigner> getEmbroideryDesigners(){
		return repository.getEmbroideryDesigners();
	}// End of getEmbroideryDesigners method
	
	/**
	 * <b>Retrieve embroidery designers searched by the designer name</b>
	 * This method will perform a search using the designer name on the designer table and return a list of the records that 
	 * matches with the search criteria
	 * 
	 * @param e_mbroideryDesigner
	 * @return
	 */
	public List<EmbroideryDesigner> getEmbroideryDesigners(EmbroideryDesigner e_mbroideryDesigner){
		return repository.getEmbroideryDesigners(e_mbroideryDesigner);
	}// End of getEmbroideryDesigners method
	
	public List<EmbroideryDesigner> getEmbroideryDesignersByNameAndPhoneNumber(EmbroideryDesigner e_mbroideryDesigner){
		return repository.getEmbroideryDesignersByNameAndPhoneNumber(e_mbroideryDesigner);
	}// End of getEmbroideryDesigners method
	
	
	/**
	 * <b>Create a new embroidery designer</b>
	 * This method will create a new designer record in the database using the method specified in the repository class
	 * @param e_mbroideryDesigner
	 */
	public void createEmbroideryDesigner(EmbroideryDesigner e_mbroideryDesigner){
		repository.createEmbroideryDesigner(e_mbroideryDesigner);
	}//  End of createEmbroideryDesigner method
	
	/**
	 * <b>Update an existing embroidery designer</b>
	 * This method will update an existing designer record in the database using the method specified in the repository class
	 * @param e_mbroideryDesigner
	 */
	public void updateEmbroideryDesigner(EmbroideryDesigner e_mbroideryDesigner){
		repository.updateEmbroideryDesigner(e_mbroideryDesigner);
	}// End of updateEmbroideryDesigner method
	
	/**
	 * <b>Delete an existing designer</b>
	 * This method will delete an existing designer record in the database using the method specified in the repository class
	 * @param e_mbroideryDesigner
	 */
	public void deleteEmbroideryDesigner(EmbroideryDesigner e_mbroideryDesigner){
		repository.deleteEmbroideryDesigner(e_mbroideryDesigner);
	}// End of deleteEmbroideryDesigner method
	
	
}// end of EmbroideryDesignerService class
