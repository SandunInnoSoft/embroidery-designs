package com.inno.embroidery.embroideryDesigner.repository;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.designerHistory.model.DesignerHistory;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.packagehistory.model.PackageHistory;
import com.inno.embroidery.packages.model.EmbroideryPackage;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

/**<h1>Repository class for Embroidery Designer</h1>
 * <p>This class contains methods to get the embroidery designers form the
 * database and to create a new entry of an embroidery designer in the database.
 * </p>
 * 
 * @author Sandun Munasinghe
 * @since  2017-7-7
 */

@Repository
@Transactional
public class EmbroideryDesignerRepository {
	
	protected SessionFactory factory;

	//@Autowired
	//protected SessionFactory sessionFactory;
	
	@SuppressWarnings("unchecked")
	/**
	 * <b>Gets all the embroidery designers from the database</b>
	 * This method gets all the availdable embroidery designers in the database and return the list.
	 * @return
	 */
	public List<EmbroideryDesigner> getEmbroideryDesigners(){
		factory=HibernateUtil.getSessionFactory();
		Session session = factory.openSession();
	      Transaction tx = null;
	      List designers = null;
	      try{
	         tx = session.beginTransaction();
	         designers = session.createQuery("FROM EmbroideryDesigner").list(); 	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return designers;
	} // end of getEmbroideryDesigners method
	
	
	/**
	 * 
	 * @param e_mbroideryDesigner
	 * @return
	 */
	public List<EmbroideryDesigner> getEmbroideryDesigners(EmbroideryDesigner e_mbroideryDesigner){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List designers = null;
	      try{
	         tx = session.beginTransaction();
	          designers = session.createQuery("FROM EmbroideryDesigner WHERE designerName LIKE '%"+e_mbroideryDesigner.getDesignerName()+"%'").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return designers;
	} // end of getEmbroideryDesigners method
	
	
	public List<EmbroideryDesigner> getEmbroideryDesignersByNameAndPhoneNumber(EmbroideryDesigner e_mbroideryDesigner){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List designers = null;
	      try{
	         tx = session.beginTransaction();
	          designers = session.createQuery("FROM EmbroideryDesigner WHERE designerName = '"+e_mbroideryDesigner.getDesignerName()+"' AND phoneNumber1 = '"+e_mbroideryDesigner.getPhoneNumber1()+"'").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return designers;
	} // end of getEmbroideryDesigners method
	
	/**
	 * <b>Creates a new designer</b>
	 * This method creates a new designer record in the database.
	 * @param e_mbroideryDesigner
	 */
	public void createEmbroideryDesigner(EmbroideryDesigner e_mbroideryDesigner){	
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	          session.save(e_mbroideryDesigner); 
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
		
	}// End of createEmbroideryDesigner method
	
	/* This method grabs last record from data table to update 'designerhistory' table*/
	
	public List <EmbroideryDesigner> selectLastEntry(EmbroideryDesigner designer){
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		List <EmbroideryDesigner> getRecord=null;
		Transaction tx=null;
		
		try {
			tx=session.beginTransaction();
			getRecord=  session.createQuery("FROM EmbroideryDesigner d where id='"+designer.getId()+"'").list();					
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
	return getRecord;
		
	}
	public void addHistory(EmbroideryDesigner embroideryDesigner){
		factory= HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		DesignerHistory designerHistory=new DesignerHistory();
		List <EmbroideryDesigner> userInformation =selectLastEntry(embroideryDesigner);		
		try {	
			for(Iterator<EmbroideryDesigner> i= userInformation.iterator();i.hasNext();){
				
				EmbroideryDesigner designerInfo = i.next();					
				designerHistory.setDesignerId(designerInfo.getId());
				designerHistory.setDesignerName(designerInfo.getDesignerName());
				designerHistory.setEmailAddress1(designerInfo.getEmailAddress1());
				designerHistory.setEmailAddress2(designerInfo.getEmailAddress2());
				designerHistory.setPhoneNumber1(designerInfo.getPhoneNumber1());
				designerHistory.setPhoneNumber2(designerInfo.getPhoneNumber2());
				designerHistory.setAddress(designerInfo.getAddress());
				designerHistory.setDescription(designerInfo.getDescription());
				designerHistory.setActive(designerHistory.getActive());
				
			tx=session.beginTransaction();
			session.save(designerHistory);
			tx.commit();
			
			}
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }
		
	}
	
	/**
	 * <b>Update existing embroidery designer</b>
	 * This method will update an existing embroidery designer record in the database with the values passed as an embroidery designer
	 * object into this method.
	 * 
	 * @param e_mbroiderDesigner
	 */
	public void updateEmbroideryDesigner(EmbroideryDesigner e_mbroiderDesigner ){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      addHistory(e_mbroiderDesigner);
	      try{
	         tx = session.beginTransaction();
	         System.out.println(e_mbroiderDesigner.getId());
	         session.update(e_mbroiderDesigner); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }		
	}// End of updateEmbroideryDesigner method
	
	/**
	 * <b>Delete an existing embroidery designer record in the database</b>
	 * This method will delete an existing embroidery designer record in the database. <br>
	 * The record that need to be deleted will be tracked using the ID passed as an embroidery designer object into this method
	 * @param e_mbroiderDesigner
	 */
	public void deleteEmbroideryDesigner(EmbroideryDesigner e_mbroiderDesigner ){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      int id=e_mbroiderDesigner.getId();
	      try{
	         tx = session.beginTransaction();
	         System.out.println(e_mbroiderDesigner.getId());
	         session.delete(e_mbroiderDesigner); 
	         tx.commit();
	         System.out.println("************ Designer ID "+id+" was deleted successfully!");
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }		
	}// End of deleteEmbroideryDesigner method
	
	
}// end of EmbroideryDesignerRepository class
