package com.inno.embroidery.image.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.repository.EmbroideryItemService;

@Controller
public class ImageController {

	@Autowired
	protected EmbroideryItemService emService;
	
	@RequestMapping(value = { "/image" })
	public String getEmbroideryItemForId(Model model,@RequestParam String id){
		List<EmbroideryItem> embroideryItems = new ArrayList<EmbroideryItem>();
		EmbroideryItem item = emService.getEmbroideryItemforId(Integer.parseInt(id));
		embroideryItems.add(item);
		model.addAttribute("embroideryItems", embroideryItems);
		return "image";
	}
}
