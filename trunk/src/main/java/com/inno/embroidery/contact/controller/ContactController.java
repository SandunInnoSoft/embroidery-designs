package com.inno.embroidery.contact.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.contact.model.Contact;

@Controller
public class ContactController {

	@RequestMapping(value = { "/contact" })
	public String getContactPage(Model model) {
		model.addAttribute("contact", new Contact());
		return "contact";
	}
	
	@RequestMapping(value = { "/contact" }, method=RequestMethod.POST)
	public String postMessage(@ModelAttribute("contact")Contact contact) {
		System.out.println("HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE Message : "+contact.getMessage());
		//if (success){
		//return success message sent page
		//}else{
		//return contact page with errors. 
		return "contact";
		
		//}
	}
	
}
