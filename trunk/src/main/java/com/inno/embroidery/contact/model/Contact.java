package com.inno.embroidery.contact.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Contact implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = -3839649781218314156L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="contactId")
    private int id;
 
    @Column(name="name", nullable = false, length = 255)
    private String name;
 
    @Column(name="email", nullable = false, length = 255)
    private String email;
	
    @Column(name="subject", nullable = false, length = 255)
    private String subject;
 
    @Column(name="message", nullable = false, length = 255)
    private String message;
    
    public int getId() {
		return id;
	}
    
    public String getName() {
		return name;
	}
    
    public String getMessage() {
		return message;
	}
    
    public String getSubject() {
		return subject;
	}
    
    public String getEmail() {
		return email;
	}
    
    public void setId(int id) {
		this.id = id;
	}
    
    public void setName(String name) {
		this.name = name;
	}
    
    public void setEmail(String email) {
		this.email = email;
	}
    
    public void setMessage(String message) {
		this.message = message;
	}
    
    public void setSubject(String subject) {
		this.subject = subject;
	}
    
}
