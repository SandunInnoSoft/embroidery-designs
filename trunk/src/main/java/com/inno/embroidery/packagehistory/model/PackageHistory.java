package com.inno.embroidery.packagehistory.model;



import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="packagehistory")
public class PackageHistory {
	
	@Id @GeneratedValue
	@Column(name="packagehistoryid")
	private int id;
	
	@Column(name="packageid")
	private int packageId;
	
	@Column(name="packagename")
	private String packageName;
	
	@Column(name="packagedescrip")
	private String packageDescrip;
	
	@Column(name="createddate")
	private Date createdDate;
	
	@Column(name="active")
	private int active;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPackageId() {
		return packageId;
	}
	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	public String getPackageName() {
		return packageName;
	}
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}
	public String getPackageDescrip() {
		return packageDescrip;
	}
	public void setPackageDescrip(String packageDescrip) {
		this.packageDescrip = packageDescrip;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
			
}
