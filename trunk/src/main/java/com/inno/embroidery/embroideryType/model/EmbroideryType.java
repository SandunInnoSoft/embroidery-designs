package com.inno.embroidery.embroideryType.model;
import java.util.Date;

import javax.persistence.*;

/**
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */
@Entity
@Table(name="type", catalog="embroider", uniqueConstraints={@UniqueConstraint(columnNames="id")})
public class EmbroideryType {
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int typeID;
	
	@Column(name="name")
	private String name;
	
	@Column(name="description")
	private String description;

	/**
	 * @return the typeID
	 */
	public int getTypeID() {
		return typeID;
	}

	/**
	 * @param typeID the typeID to set
	 */
	public void setTypeID(int typeID) {
		this.typeID = typeID;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	

}
