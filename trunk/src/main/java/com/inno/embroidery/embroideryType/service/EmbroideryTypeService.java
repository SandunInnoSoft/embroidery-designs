package com.inno.embroidery.embroideryType.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import com.inno.embroidery.embroideryType.model.EmbroideryType;
import com.inno.embroidery.embroideryType.repository.EmbroideryTypeRepository;

/**
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 */

@Service
public class EmbroideryTypeService {

	@Autowired
	protected EmbroideryTypeRepository repository;
	
	/**
	 * 
	 * @return
	 */
	public List<EmbroideryType> getEmbroideryTypes(){
		return repository.getEmbroideryTypes();
	}// end of getEmbroideryTypes method
	
	/**
	 * 
	 * @param e_mbroideryType
	 */
	public void createEmbroideryType(EmbroideryType e_mbroideryType){
		repository.createEmbroideryType(e_mbroideryType);
	}//  end of createEmbroideryDesigner method
	
}// End of EmbroideryTypeService class
