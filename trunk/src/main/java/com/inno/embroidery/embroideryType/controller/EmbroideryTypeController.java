package com.inno.embroidery.embroideryType.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.embroideryType.model.EmbroideryType;
import com.inno.embroidery.embroideryType.service.EmbroideryTypeService;

/**
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 *
 */

@Controller
public class EmbroideryTypeController {

	@Autowired
	protected EmbroideryTypeService service;
	
	/**
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = {"/embroideryTypes"})
	public String getEmbroideryTypes(Model m_odel){
		List<EmbroideryType> embroideryTypes=service.getEmbroideryTypes();
		m_odel.addAttribute("embroideryTypes", embroideryTypes);
		System.out.println("getEmbroideryTypes called +++++++");
		return "embroideryTypes";
	}// End of getEmbroideryTypes method
	
	@RequestMapping(value = "create-EmbroideryType")
	public String createEmbroideryTypeGet(Model m_odel){
		m_odel.addAttribute("embroideryType", new EmbroideryType());
		return "create-EmbroideryType";
	}// End of createEmbroideryTypeGet method
	
	@RequestMapping(value = "create-EmbroideryType", method = RequestMethod.POST)
    public String createEmbroideryTypePost(@ModelAttribute("embroideryType") EmbroideryType e_mbroideryType) {
        service.createEmbroideryType(e_mbroideryType);
        return "redirect:embroideryTypes";
    }// End of createEmbroideryTypePost method
	
}// End of EmbroideryTypeController class
