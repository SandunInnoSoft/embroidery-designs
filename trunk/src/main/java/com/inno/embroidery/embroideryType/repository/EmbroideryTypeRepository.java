package com.inno.embroidery.embroideryType.repository;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryType.model.EmbroideryType;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author Sandun Munasinghe
 * @since  2015-7-8
 *
 */

@Repository
@Transactional
public class EmbroideryTypeRepository {

	@SuppressWarnings("unchecked")
	
	public List<EmbroideryType> getEmbroideryTypes(){
		//return sessionFactory.getCurrentSession().createQuery("FROM EmbroideryDesigner e").list();
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List types = null;
	      try{
	         tx = session.beginTransaction();
	          types = session.createQuery("FROM EmbroideryType").list(); 
	         
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return types;
	} // end of getEmbroideryTypes method
	
	public void createEmbroideryType(EmbroideryType e_mbroideryType){
		//HibernateUtil.getSessionFactory().getCurrentSession().save(e_mbroideryDesigner);
		//sessionFactory.getCurrentSession().save(e_mbroideryDesigner);
		
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      try{
	         tx = session.beginTransaction();
	          session.save(e_mbroideryType); 
	         tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
		
	}// End of createEmbroideryType method
	
}// End of EmbroideryTypeRepository class
