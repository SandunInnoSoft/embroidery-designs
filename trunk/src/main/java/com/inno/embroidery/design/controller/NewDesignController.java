package com.inno.embroidery.design.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.inno.embroidery.embroideryItem.model.EmbroideryItem;
import com.inno.embroidery.embroideryItem.repository.EmbroideryItemService;

@Controller
public class NewDesignController {

	@Autowired
	EmbroideryItemService emService;
	
	@RequestMapping(value = { "/design" })
	public String getEmbroideryItems(Model model) {
		List<EmbroideryItem> embroideryItems = emService.getEmbroideryItems();
//		List<Category> categories = catService.getCategories();
//		model.addAttribute("categories",categories);		
		model.addAttribute("embroideryItems", embroideryItems);
		return "design";
	}
}
