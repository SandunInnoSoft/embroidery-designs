package com.inno.embroidery.users.model;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;


import com.inno.embroidery.promotions.model.EmbroideryPromotion;

@Entity
@Table(name="users")
public class EmbroiderUser {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="userid")
	private int userID;
	
	@Column(nullable=false,length=255,name="fullname")
	private String userFullName;
	
	@Column(name="username",nullable=false,length=20)
	private String userName;		

	@Column(name="useremail",nullable=false,length=50)
	private String userEmail;
	
	@Column(name="passwordx",nullable=false,length=255)
	private String userPassword;
	
	@Column(name="address",nullable=false,length=255)
	private String userAddress;
	
	@Column(name="active", nullable=false,length=1)
	private int userActive;
	
	@Column(name="createddate", nullable=false,length=1)
	private Date createdDate;
	
	@Transient
	private String mode; 
	
//	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
//	private Set<UserRole> userRole = new HashSet<UserRole>(0);
	
	
	//@OneToMany(targetEntity=com.inno.embroidery.promotions.model.EmbroideryPromotion.class,mappedBy="promoPackage",cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	//private List<EmbroideryPromotion> embroideryPromotions;
	
//	@Column(name="createduserid",nullable=false,length=255)
//	private EmbroiderUser createdUser;
	
	

	

	
	/**
	 * @return the userRole
	 */
//	public Set<UserRole> getUserRole() {
//		return userRole;
//	}
//
//	/**
//	 * @param userRole the userRole to set
//	 */
//	public void setUserRole(Set<UserRole> userRole) {
//		this.userRole = userRole;
//	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

//	public List<EmbroideryPromotion> getEmbroideryPromotions() {
//		return embroideryPromotions;
//	}
//
//	public void setEmbroideryPromotions(
//			List<EmbroideryPromotion> embroideryPromotions) {
//		this.embroideryPromotions = embroideryPromotions;
//	}

	
	public String getUserFullName() {
		return userFullName;
	}

	public void setUserFullName(String userFullName) {
		this.userFullName = userFullName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public int getUserActive() {
		return userActive;
	}

	public void setUserActive(int userActive){
		this.userActive=userActive;
	}
	
	public void setUserActive(String userActive) {
		this.userActive = Integer.parseInt(userActive);
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

//	public EmbroiderUser getCreatedUser() {
//		return createdUser;
//	}
//
//	public void setCreatedUser(EmbroiderUser createdUser) {
//		this.createdUser = createdUser;
//	}
	
	
	
}
