package com.inno.embroidery.users.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.users.model.EmbroiderUser;
import com.inno.embroidery.users.service.EmbroiderUserService;

@Controller
public class EmbroiderUserController {
	@Autowired
	protected EmbroiderUserService service;

	@RequestMapping(value = { "/embroideryUsers" })
	public String getUsers(Model model) {
		List<EmbroiderUser> embroiderUsers = service.getEmbroiderUsers();
		model.addAttribute("embroiderUsers", embroiderUsers);
		model.addAttribute(new EmbroiderUser());
		return "embroideryUsers";
	}
	
//	@RequestMapping(value = { "manageEmbroideryUsers" })
//	public String getManageEmbroideryUser(Model model) {		
//		model.addAttribute("embroideryUser", new EmbroiderUser());
//		return "manageEmbroideryUsers";
//	}

	@RequestMapping(value = "create-EmbroideryUser")
	public String createEmbroideryUserGet(Model model) {
		model.addAttribute("embroiderUser", new EmbroiderUser());
		return "create-EmbroideryUser";
	}

//	@RequestMapping(value = "create-EmbroideryUser", method = RequestMethod.POST)
//	public String createUserPost(
//			@ModelAttribute("embroideryUser") EmbroiderUser embroideryUser) {
//		service.createEmbroyderUsers(embroideryUser);
//		return "redirect:create-EmbroideryUser";
//	}
	
	@RequestMapping(value = "embroideryUsers", method = RequestMethod.POST)
	public String searchUserPost( Model model , 
			@ModelAttribute("embroiderUser") EmbroiderUser embroiderUser) {
		System.out.println("The mode is: "+embroiderUser.getMode());
		if(embroiderUser.getMode().equals("SEARCH")){			
			List<EmbroiderUser> embroiderUsers = service.getSearchEmbroideryUsers(embroiderUser);
			if(embroiderUsers.isEmpty()==false) {
				model.addAttribute("embroiderUsers", embroiderUsers);
			}			
		}
		else{}
		return "embroideryUsers";
	}
	
	
	@RequestMapping(value="create-EmbroideryUser",method=RequestMethod.POST )
	public String manageUsersPost(
			@ModelAttribute("embroiderUser") EmbroiderUser embroiderUser){
		
		 if (embroiderUser.getUserID()==0 && embroiderUser.getUserActive()==0) {
			 embroiderUser.setUserActive(1);	
			 service.createEmbroyderUsers(embroiderUser);
			}
		 else if(embroiderUser.getUserID()!=0 && embroiderUser.getUserActive()==1){
			 
			 service.updateEmbroiderUsers(embroiderUser);
			 
		 }else if (embroiderUser.getUserID()!=0 && embroiderUser.getUserActive()== 0){
						
			 embroiderUser.setUserActive(0);
			 service.updateEmbroiderUsers(embroiderUser); 
			 			 
		 }
		 
		 
//		else if(embroiderUser.getUserDb()=="u"){ 
//			    service.updateEmbroiderUsers(embroiderUser);			
//		}
//		else if (embroiderUser.getUserDb()=="d") {
//			   service.updateEmbroiderUsers(embroiderUser);
//		} else{ System.out.println("******************Fail**************************"+embroiderUser.getUserDb()); }
//		
		
		return "redirect:create-EmbroideryUser";	
	} 	
	
	
}
