package com.inno.embroidery.users.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.users.model.EmbroiderUser;
import com.inno.embroidery.users.repository.EmbroiderUserRepository;

@Service
public class EmbroiderUserService {
	@Autowired
	protected EmbroiderUserRepository repository;	
	
	public List<EmbroiderUser> getEmbroiderUsers(){
		return repository.getUsers();
	}
	
	public void createEmbroyderUsers(EmbroiderUser user){		
		repository.createUser(user);
	}
	public void updateEmbroiderUsers(EmbroiderUser user){		
		repository.updateEmbroideryUser(user);
	}
	public List<EmbroiderUser> getSearchEmbroideryUsers(EmbroiderUser e_mbroiderUser){
		return repository.getSearchUsers(e_mbroiderUser);
	}
}
