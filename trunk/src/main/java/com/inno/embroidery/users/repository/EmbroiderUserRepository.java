package com.inno.embroidery.users.repository;









import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.inno.embroidery.common.HibernateUtil;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.packages.model.EmbroideryPackage;
import com.inno.embroidery.userhistory.model.UserHistory;
import com.inno.embroidery.users.model.EmbroiderUser;

@SuppressWarnings("deprecation")
@Repository
@Transactional
public class EmbroiderUserRepository {

	
	protected SessionFactory factory;	
	
	@SuppressWarnings("unchecked")
	public List<EmbroiderUser> getUsers() {
		List < EmbroiderUser> retUsers=null;
		factory=new AnnotationConfiguration().
				configure().addAnnotatedClass(com.inno.embroidery.users.model.EmbroiderUser.class).buildSessionFactory();
		Session session = factory.openSession();
	    Transaction tx = null;
	     
		try {
			tx=session.beginTransaction();
			retUsers= session.createQuery("FROM EmbroiderUser u where userActive='1'").list();
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
		
		return retUsers;
	}
	/*********** This method to grab last recorded data from functioning database*************/
	public List <EmbroiderUser> selectLastEntry(EmbroiderUser user){
		factory=HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		List <EmbroiderUser> getRecord=null;
		Transaction tx=null;
		try {
			tx=session.beginTransaction();
			getRecord=  session.createQuery("FROM EmbroiderUser u where userID='"+user.getUserID()+"'").list();					
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx!=null) tx.rollback();
	         e.printStackTrace();
			
		}finally{			
			session.close();
		}
	return getRecord;
		
	}
	
	@SuppressWarnings("deprecation")
	public void createUser(EmbroiderUser user) {
		//factory.getCurrentSession().save(user);
		factory=new AnnotationConfiguration().configure().addAnnotatedClass(com.inno.embroidery.users.model.EmbroiderUser.class).buildSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			user.setCreatedDate(date);
			tx=session.beginTransaction();
			session.save(user);
			tx.commit();
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }			
		
	}
	
	public List<EmbroiderUser> getSearchUsers(EmbroiderUser e_mbroideryUser){
		Session session = HibernateUtil.getSessionFactory().openSession();
	      Transaction tx = null;
	      List < EmbroiderUser> searchUsers = null;
	      try{
	         tx = session.beginTransaction();
	         if(e_mbroideryUser.getUserFullName().equals(null)){
	        	 searchUsers = session.createQuery("FROM EmbroiderUser u WHERE userName LIKE '%"+e_mbroideryUser.getUserName()+"%'").list();
	         }
	         else if(e_mbroideryUser.getUserName().equals(null)){
	        	 searchUsers = session.createQuery("FROM EmbroiderUser u WHERE userFullName LIKE '%"+e_mbroideryUser.getUserFullName()+"%'").list();
	         }
	         else if(e_mbroideryUser.getUserFullName().equals(null) && e_mbroideryUser.getUserName().equals(null)){
	        	 searchUsers=null;
	         }
	         else{
	        	 searchUsers = session.createQuery("FROM EmbroiderUser u WHERE userFullName LIKE '%"+e_mbroideryUser.getUserFullName()+"%' OR userName LIKE '%"+e_mbroideryUser.getUserName()+"%' ").list();
	         }
	         
	          tx.commit();
	      }catch (HibernateException s) {
	         if (tx!=null) tx.rollback();
	         s.printStackTrace(); 
	         System.exit(0);
	      }finally {
	         session.close(); 
	      }
	      return searchUsers;
	}
	/* This method will add a history record to userhistory table*/
	public void addHistory(EmbroiderUser embroiderUser){
		factory= HibernateUtil.getSessionFactory();
		Session session=factory.openSession();
		Transaction tx=null;
		UserHistory userHistory=new UserHistory();
		List <EmbroiderUser> userInformation =selectLastEntry(embroiderUser);		
		try {	
			for(Iterator<EmbroiderUser> i= userInformation.iterator();i.hasNext();){
				
				EmbroiderUser userInfo = i.next();					
				userHistory.setUserId(userInfo.getUserID());
				userHistory.setUserEmail(userInfo.getUserEmail());
				userHistory.setFullName(userInfo.getUserFullName());
				userHistory.setUserName(userInfo.getUserName());
				userHistory.setUserAddress(userInfo.getUserAddress());
				userHistory.setUserPassword(userInfo.getUserPassword());
				userHistory.setActive(userInfo.getUserActive());
				userHistory.setDate(userInfo.getCreatedDate());
				
			tx=session.beginTransaction();
			session.save(userHistory);
			tx.commit();
			System.out.println("History added!!!!!!!!!");
			}
			
		} catch (HibernateException e) {
			 e.printStackTrace();
		}finally {
	         session.close(); 
	      }
		
	}
	
	public void updateEmbroideryUser(EmbroiderUser embroiderUser ){
		factory=new AnnotationConfiguration().configure().addAnnotatedClass(com.inno.embroidery.users.model.EmbroiderUser.class).buildSessionFactory();
		Session session = factory.openSession();
	      Transaction tx = null;
	      addHistory(embroiderUser);
	      try{
	    	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();
				embroiderUser.setCreatedDate(date);
	         tx = session.beginTransaction();
	         System.out.println(embroiderUser.getUserID());
	         session.update(embroiderUser); 
	         tx.commit();
	      }catch (HibernateException e) {
	         if (tx!=null) tx.rollback();
	         e.printStackTrace(); 
	      }finally {
	         session.close(); 
	      }		
	}
	
	
//	/**
//	 * This methos is to find a user from the database by name for the login
//	 */
//	public EmbroiderUser findByUserName(String username) {
//		
//		factory=HibernateUtil.getSessionFactory();
//
//		List<EmbroiderUser> users = new ArrayList<EmbroiderUser>();
//
//		users = sessionFactory.getCurrentSession().createQuery("from User where username=?").setParameter(0, username)
//				.list();
//
//		if (users.size() > 0) {
//			return users.get(0);
//		} else {
//			return null;
//		}
//
//	}
	

}
