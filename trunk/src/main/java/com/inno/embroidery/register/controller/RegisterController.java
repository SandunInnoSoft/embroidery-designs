package com.inno.embroidery.register.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.inno.embroidery.embroideryCustomer.model.EmbroideryCustomer;
import com.inno.embroidery.embroideryCustomer.service.EmbroideryCustomerService;
import com.inno.embroidery.embroideryDesigner.model.EmbroideryDesigner;
import com.inno.embroidery.embroideryDesigner.service.EmbroideryDesignerService;

@Controller
public class RegisterController {
	
	@Autowired
	protected EmbroideryCustomerService service;

	@RequestMapping(value = { "/register" })
	public String getRegistrationPage(Model model) {
		model.addAttribute("embroideryCustomer", new EmbroideryCustomer());
		System.out.println("getEmbroideryCustomer called-------------");
		return "register";
	}	
	
	
	@RequestMapping(value="register",method=RequestMethod.POST )
	public String manageCustomersPost(Model model,
			@ModelAttribute("embroideryCustomer") EmbroideryCustomer c_ustomer){
		System.out.println("*********************************************");

		String returnText=null;

			List<EmbroideryCustomer> existingCustomers = service.getEmbroideryCustomersByUsername(c_ustomer);
			//Precheck if the a customer with the same username/email exists in the database 
			if(existingCustomers.size()==0){	
				 System.out.println("*****Create Embroidery customer called");
				 service.createEmbroideryCustomer(c_ustomer);
				 returnText="redirect:register?a=1";
				
			}else{
				System.out.println("*********Customer exists in the database");
				returnText="redirect:register?a=0";
			}

		return returnText;	
	}// End of manageDesignersPost method
	
	
	
}
