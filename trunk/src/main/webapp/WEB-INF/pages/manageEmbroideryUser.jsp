<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
    <link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
    <title>Manage  User for Embroidery</title>
   
</head>
<body>
	<form:form modelAttribute="delEmbroideryUser" method="post" name="deleteTrap">
		<form:form modelAttribute="updEmbroideryUser" method="post" name="updateTrap">
			<table>
				<tr style="display: none;" >
				
					<td>id</td>
					<td>
						<form:input path="manID"/>
						<form:input path="manID" element="span"/>
					</td>
				</tr>
				<tr>
					<td>Name:</td> 	
					<td> 
						<form:input path="manFullName"/>
						<form:errors path="manFullName" element="span"/>
					</td>
				</tr>
				<tr>
					<td>Email:</td>
					<td>
						<form:input path="manEmail"/>
						<form:errors path="manEmail" element="span"/>
					</td>
				</tr>
				<tr>
					<td>Password:</td>
					<td>
						<form:password path="manUserPassword"/>
						<form:password path="manUserPassword" element="span"/>
					</td>
				</tr>
				<tr>
					<td>Address:</td>
					<td>
						<form:textarea path="manUserAddress"/>
						<form:textarea path="manUserAddress" element="span"/>
					</td>
				</tr>
			</table>		
			<br>
				<input type="button" onclick="" value="Update" />
		</form:form>
	</form:form>
</body>
</html>