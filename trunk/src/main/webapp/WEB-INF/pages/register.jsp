<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />
<title>Embroidery online | New Designs</title>

<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/bootstrap.css"/>">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=" <c:url value="/resources/js/jquery.min.js"/>"></script>

<!-- Custom Theme files -->
<!--theme-style-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/style.css"/>">

<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jewellry online Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>

<!--//fonts-->
<script type="text/javascript" src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/easing.js"/>"></script>

<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
</script>
<script type="text/javascript">

	document.getElementById("createCustomerSubmit").disabled = true;

function createCustomer(){
	var firstName=document.getElementById("customerFirstName").value;
	var lastName=document.getElementById("customerLastName").value;
	
	document.getElementById("hiddenCustomerName").value=firstName+" "+lastName;
	document.getElementById("hiddenEmailAddress").value=document.getElementById("customerEmailAddress").value;
	document.getElementById("hiddenPhoneNumber").value=document.getElementById("customerPhoneNumber").value;
	document.getElementById("hiddenPassword").value=document.getElementById("customerConfirmPassword").value;
	document.getElementById("createCustomerForm").submit();
}

function checkPass(){
	
	var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('customerConfirmPassword');
    var message = document.getElementById('confirmMessage');
    
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    var noColor = "none";
    
    if(pass1.value == pass2.value){
    	
    	if(pass1.value=="" || pass2.value==""){
    		pass2.style.backgroundColor = noColor;
            message.style.color = noColor;
            message.innerHTML = ""
            document.getElementById("createCustomerSubmit").disabled = true;
    	}else{
    	
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords Match!"
        document.getElementById("createCustomerSubmit").disabled = false;
    	}
    }else if(pass1.value=="" || pass2.value==""){
    	
    	pass2.style.backgroundColor = noColor;
        message.style.color = noColor;
        message.innerHTML = ""
        document.getElementById("createCustomerSubmit").disabled = true;
    	
    }else{
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
        document.getElementById("createCustomerSubmit").disabled = true;
    }
	
}// End of checkPass


</script>

<!-- Ajax function -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>

<body>
<!--header-->
	<div class="header">
		<div class="container">			
			<ul>
				<li><a href="account">MY ACCOUNT</a><span>|</span></li>
				<li><a href="register">  REGISTER</a><span>|</span></li>
				<li><a href="contact" > CONTACT US</a></li>
			</ul>
				<div class="clearfix"> </div>
			</div>
			
	</div>
	<!---->
	<div class="container" id="">
		<div class="header-top">
			<div class="header-top-in">
				<div class="top-header">
					<div class="logo">
							<a href="index"><img src="<c:url value="/resources/images/embroidery.png"/>" alt=" "></a>
					</div>
					<div class="left-head">
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="top-header-in">
					<a href="#"><img class="img-responsive bag"  src="<c:url value="/resources/images/bag.png"/>" alt=" " ></a>
					<div id="shop" class="shop">
						<h6>Shopping basket</h6>
						<p style="word-spacing: 0.3em;"><span>${itemCount}</span>  <c:if test="${itemCount != 1}">ITEMS</c:if><c:if test="${itemCount == 1}">ITEM</c:if> | $<span>${totalPrice}</span></p>  
	
						<c:if test="${itemCount != 0}">
							<a href="checkout">View Cart<i> </i></a>												
						</c:if>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---->
			<div class="top-nav">
					<span class="menu">MENU </span>
					<ul>
						<li><a href="index">HOME</a></li>
						<li><a href="design"> NEW DESIGNS</a></li>
						<li><a href="gift">  PROMOTIONS  </a></li>
						<li><a href="gift">  PACKAGES  </a></li>						
						<li><a href="404">HELP</a></li>
						<li><a href="about">ABOUT US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
				</script>
			</div>
			<!--  -->
			
				<div class="register">
				<h2>Register</h2>
		 
				 <div class="col-md-6  register-top-grid">
					<h3>Personal Information</h3>
					<form>
						<div>
							<span>First Name</span>
							<input type="text" id="customerFirstName"> 
						</div>
						<div>
							<span>Last Name</span>
							<input type="text" id="customerLastName"> 
						 </div>
						 <div>
							 <span>Email Address</span>
							 <input type="text" id="customerEmailAddress"> 
						</div>
						<div>
							 <span>Phone Number</span>
							 <input type="text" id="customerPhoneNumber"> 
						</div>
<!-- 						   <a class="news-letter" href="#"> -->
<!-- 							 <label class="checkbox"><input type="checkbox" name="checkbox" checked=""><i> </i>Sign Up</label> -->
<!-- 						   </a> -->
					</form>
					 </div>
					 
				     <div class=" col-md-6  register-bottom-grid">
						    <h3>Login Information</h3>
							<form>
								<div>
									<span>Password</span>
							<input type="text" id="password" name="password"> <!-- 		id="customerPassword" -->
								</div>
								<div>
									<span>Confirm Password</span>
									<input type="text" id="customerConfirmPassword" onkeyup="checkPass(); return false;">
									<span id="confirmMessage" class="confirmMessage"></span>
								</div>
								<div class="register-but">
									<input type="button" value="submit" id="createCustomerSubmit" onclick="createCustomer()">
								</div>
							</form>
					</div>
					
				<div class="clearfix"> </div>
			</div>						
			<div class="footer">
				<div class="footer-top">
					<p class="footer-class">Copyright � 2015 Jewellry online Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul class="footer-bottom">
						<li><a href="index.html">HOME</a><span>|</span></li>
						<li><a href="about.html">  ABOUT  </a><span>|</span></li>
						<li><a href="product.html">  PRODUCTS</a><span>|</span></li>						
						<li><a href="404.html">HELP</a><span>|</span></li>
						<li><a href="contact.html">CONTACT</a></li>
					</ul>
					<div class="clearfix"> </div>
					</div>
					<div class="bottom-footer-in">
						<a href="index.html"><img src="images/logo-1.png" alt=" " ></a>
					</div>
			</div>
		</div>
	</div>
	
<!-- 	Hidden for to keep the submit data -->

 	<form:form modelAttribute="embroideryCustomer" method="post" id="createCustomerForm"> <%-- This model attribute throws an error --%>

    <table style="display: none;">
    	
        <tr>
            <td><b>Name:</b></td>
            <td>
                <form:input path="customerName" id="hiddenCustomerName"/>
                <form:errors path="customerName" element="span"/>
            </td>
        	
        </tr>
         <tr>
            <td><b>E-mail Address</b></td>
            <td>
                <form:input path="emailAddress" id="hiddenEmailAddress"/>
                <form:errors path="emailAddress" element="span"/>
            </td>
        </tr>
         <tr>
            <td><b>Phone Number :</b></td>
            <td>
                <form:input path="phoneNumber" id="hiddenPhoneNumber"/>
                <form:errors path="phoneNumber" element="span"/>
            </td>
        </tr>
        
        <tr>
            <td><b>Password:</b></td>
            <td>
                <form:input path="password" id="hiddenPassword"/>
                <form:errors path="password" element="span"/>
            </td>
        </tr>   
</form:form>
	
</body>
</html>