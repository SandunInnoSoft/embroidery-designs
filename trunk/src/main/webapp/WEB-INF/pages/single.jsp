<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page trimDirectiveWhitespaces="true"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />

<title>Embroidery online | Single</title>
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/bootstrap.css"/>">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=" <c:url value="/resources/js/jquery.min.js"/>"></script>

<!-- Custom Theme files -->
<!--theme-style-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/style.css"/>">

<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jewellry online Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/easing.js"/>"></script>

<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
</script>
<link rel="stylesheet" href="<c:url value="/resources/css/etalage.css"/>">
<script src="<c:url value="/resources/js/jquery.etalage.min.js"/>"></script>
		<script>
			jQuery(document).ready(function($){

				$('#etalage').etalage({
					thumb_image_width: 300,
					thumb_image_height: 400,
					source_image_width: 900,
					source_image_height: 1200,
					show_hint: true,
					click_callback: function(image_anchor, instance_id){
						alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
					}
				});

			});
		</script>

<!-- 		<script type="text/javascript"> -->
<!--  			function modifyURL(){ -->
<!--  				    location.replace("http://localhost:8080/embroideryapp/checkout") -->
<!--  			}			 -->
<!-- 		</script> -->

</head>
<body>
<!--header-->
	<div class="header">
		<div class="container">			
			<ul>
				<li><a href="account.html">MY ACCOUNT</a> <span>|</span></li>
				<li><a href="register.html">  REGISTER</a><span>|</span></li>
				<li ><a href="contact.html" > CONTACT US</a></li>
			</ul>
				<div class="clearfix"> </div>
			</div>
	</div>
	<!---->
	<div class="container">
		<div class="header-top">
			<div class="header-top-in">
				<div class="top-header">
					<div class="logo">
							<a href="index"><img src="<c:url value="/resources/images/embroidery.png"/>" alt=" " ></a>
					</div>
					<div class="left-head">
<!-- 						<img class="img-responsive" src="images/stone.png" alt=" " > -->
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="top-header-in">
					<a href="#"><img class="img-responsive bag" src="<c:url value="/resources/images/bag.png"/>" alt=" " ></a>
					<div id="shop" class="shop">
						<h6>Shopping basket</h6>
						<p  style="word-spacing: 0.3em;"><span>${itemCount}</span>  <c:if test="${itemCount != 1}">ITEMS</c:if><c:if test="${itemCount == 1}">ITEM</c:if> | $<span>${totalPrice}</span></p>  
	
						<c:if test="${itemCount != 0}">
							<a href="checkout">View Cart<i> </i></a>												
						</c:if>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---->
			<div class="top-nav">
					<span class="menu">MENU </span>
					<ul>
						<li><a href="index">HOME</a></li>
						<li><a href="design"> NEW DESIGNS</a></li>
						<li><a href="gift">  PROMOTIONS  </a></li>
						<li><a href="gift">  PACKAGES  </a></li>						
						<li><a href="404">HELP</a></li>
						<li><a href="about">ABOUT US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>
			</div>
			<!---->
			<div class="single">
				<div class="col-md-12 top-in-single">
					<div class="col-md-9 single-top" style="display:table-cell; vertical-align:middle; text-align:center">

					<input id="hfld" type="hidden" value="${embroideryItem.imagePath}">
											
<!-- 					<img id="emb_image" src="'resources\imgfiles\chicken06.JPG'"/>	 -->
						<img src="<c:url value="${embroideryItem.imagePath}"/>">

<!-- 					<div>${embroideryItem.imagePath}</div> -->
<!-- 						<ul id="etalage"> -->

					<script type="text/javascript">
					
					document.getElementById("emb_image").onload = function(){
						var img = getElementById("emb_image");
						var hfld = getElementById("hfld").value;
						alert(hfld);
						img.src ='/'+hfld;
					}
					
					</script>

					<script type="text/javascript">
						function addToCheckout(){
							var val = document.getElementById('cart-to').innerHTML;
						}
					</script>

					</div>	
					<div class="col-md-3 single-top-in">
						<div class="single-para">
							<h4>${embroideryItem.fileName}</h4>
							<div class="para-grid">
								<span  class="add-to">${embroideryItem.price}<span>$</span></span>
								<a id="cart-to" href="multTtms&itid=${embroideryItem.id}" class=" cart-to" onclick="addToCheckout()">Add to Cart</a>
								<div class="clearfix"></div>
							 </div>
							<div class="available">
								<h6>Available Options :</h6>
							
							<ul>	
								<li>Format:
									<select>
										<option>.DST</option>
										<option>.EMB</option>
										<option>.ART</option>
									</select>
								</li>
							</ul>
							
						</div>
								<input type="hidden" id="itemId" name="itemId" value="${embroideryItem.id}">
								<a id="proCheckout" href="sinItm&itmid${embroideryItem.id}" class="cart ">Proceed Checkout</a>							
						</div>
					</div>
				<div class="clearfix"> </div>
				    <!----- tabs-box ---->
		<div class="sap_tabs">	
				     <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
						  <ul class="resp-tabs-list">
						  	  <li class="resp-tab-item " aria-controls="tab_item-0" role="tab"><span>Design Description</span></li>
<!-- 							  <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Additional Information</span></li> -->
<!-- 							  <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>Reviews</span></li> -->
							  <div class="clearfix"></div>
						  </ul>				  	 
							<div class="resp-tabs-container">
							    <h2 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>Design Description</h2>
							    <div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
									<div class="facts">
									  <p >${embroideryItem.description}</p>
										<ul>
<!-- 											<li>Research</li> -->
<!-- 											<li>Design and Development</li> -->
<!-- 											<li>Porting and Optimization</li> -->
<!-- 											<li>System integration</li> -->
<!-- 											<li>Verification, Validation and Testing</li> -->
<!-- 											<li>Maintenance and Support</li> -->
										</ul>         
							        </div>
							    </div>
<!-- 							      <h2 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>Additional Information</h2> -->
<!-- 							      <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-1"> -->
<!-- 									<div class="facts">									 -->
<!-- 										<p > Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections </p> -->
<!-- 										<ul > -->
<!-- 											<li>Multimedia Systems</li> -->
<!-- 											<li>Digital media adapters</li> -->
<!-- 											<li>Set top boxes for HDTV and IPTV Player applications on various Operating Systems and Hardware Platforms</li> -->
<!-- 										</ul> -->
<!-- 							        </div>	 -->
<!-- 								</div>									 -->
<!-- 							      <h2 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>Reviews</h2> -->
<!-- 							  <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-2"> -->
<!-- 									 <div class="facts"> -->
<!-- 									  <p > There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined </p> -->
<!-- 										<ul> -->
<!-- 											<li>Research</li> -->
<!-- 											<li>Design and Development</li> -->
<!-- 											<li>Porting and Optimization</li> -->
<!-- 											<li>System integration</li> -->
<!-- 											<li>Verification, Validation and Testing</li> -->
<!-- 											<li>Maintenance and Support</li> -->
<!-- 										</ul>      -->
<!-- 							     </div>	 -->
<!-- 							 </div> -->
					      </div>
					 </div>
					 <script src="js/easyResponsiveTabs.js" type="text/javascript"></script>
		    <script type="text/javascript">
			    $(document).ready(function () {
			        $('#horizontalTab').easyResponsiveTabs({
			            type: 'default', //Types: default, vertical, accordion           
			            width: 'auto', //auto or any width like 600px
			            fit: true   // 100% fit in a container
			        });
			    });
			   </script>	

	</div>

				</div>
<!-- 				<div class="col-md-3"> -->
<!-- 					<div class="single-bottom"> -->
<!-- 						<h4>Categories</h4> -->
<!-- 						<ul> -->
<!-- 						<li><a href="#"><i> </i>Fusce feugiat</a></li> -->
<!-- 						<li><a href="#"><i> </i>Contrary belief</a></li> -->
<!-- 						<li><a href="#"><i> </i>Lorem Ipsum</a></li> -->
<!-- 						<li><a href="#"><i> </i>Fusce feugiat</a></li> -->
<!-- 						<li><a href="#"><i> </i>Injected humour</a></li> -->
<!-- 						</ul> -->
<!-- 					</div> -->
<!-- 					<div class="single-bottom"> -->
<!-- 						<h4>Product Categories</h4> -->
<!-- 						<ul> -->
<!-- 						<li><a href="#"><i> </i>Feugiat(5)</a></li> -->
<!-- 						<li><a href="#"><i> </i>Injected(7)</a></li> -->
<!-- 						<li><a href="#"><i> </i>Fusce (2)</a></li> -->
<!-- 						<li><a href="#"><i> </i>Feugiat(1)</a></li> -->
<!-- 						<li><a href="#"><i> </i>Contrary(6)</a></li> -->
<!-- 						</ul> -->
<!-- 					</div> -->
<!-- 					<div class="single-bottom"> -->
<!-- 						<h4>Product Categories</h4> -->
<!-- 							<div class="product-go"> -->
<!-- 								<img class="img-responsive fashion" src="images/st1.jpg" alt=""> -->
<!-- 							<div class="grid-product"> -->
<!-- 								<a href="#" class="elit">Consectetuer adipiscing elit</a> -->
<!-- 								<span class=" price-in"><small>$500.00</small> $400.00</span> -->
<!-- 							</div> -->
<!-- 							<div class="clearfix"> </div> -->
<!-- 							</div> -->
<!-- 							<div class="product-go"> -->
<!-- 								<img class="img-responsive fashion" src="images/st2.jpg" alt=""> -->
<!-- 							<div class="grid-product"> -->
<!-- 								<a href="#" class="elit">Consectetuer adipiscing elit</a> -->
<!-- 								<span class=" price-in"><small>$500.00</small> $400.00</span> -->
<!-- 							</div> -->
<!-- 							<div class="clearfix"> </div> -->
<!-- 							</div> -->
<!-- 							<div class="product-go"> -->
<!-- 								<img class="img-responsive fashion" src="images/st3.jpg" alt=""> -->
<!-- 							<div class="grid-product"> -->
<!-- 								<a href="#" class="elit">Consectetuer adipiscing elit</a> -->
<!-- 								<span class=" price-in"><small>$500.00</small> $400.00</span> -->
<!-- 							</div> -->
<!-- 							<div class="clearfix"> </div> -->
<!-- 							</div> -->
<!-- 				</div> -->
<!-- 				</div> -->
				<div class="clearfix"> </div>
		

			</div>
			
			<div class="footer">
				<div class="footer-top">
					<p class="footer-class">Copyright � 2015 Jewellry online Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul class="footer-bottom">
						<li><a href="index">HOME</a><span>|</span></li>
						<li><a href="about">  ABOUT  </a><span>|</span></li>
						<li><a href="product">  PRODUCTS</a><span>|</span></li>						
						<li><a href="404">HELP</a><span>|</span></li>
						<li><a href="contact">CONTACT</a></li>
					</ul>
					<div class="clearfix"> </div>
					</div>
					<div class="bottom-footer-in">
						<a href="index"><img src="<c:url value="/resources/images/logo-1.png"/>" alt=" " ></a>
					</div>
			</div>
		</div>
	</div>
</body>
</html>