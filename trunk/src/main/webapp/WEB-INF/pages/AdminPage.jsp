<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrator page</title>
<style type="text/css">
#sddm
{	margin: 0;
	padding: 0;
	z-index: 30}

#sddm li
{	margin: 0;
	padding: 0;
	list-style: none;
	float: left;
	font: bold 11px arial}

#sddm li a
{	display: block;
	margin: 0 1px 0 0;
	padding: 4px 10px;
	width: 60px;
	background: #5970B2;
	color: #FFF;
	text-align: center;
	text-decoration: none}

#sddm li a:hover
{	background: #49A3FF}

#sddm div
{	position: absolute;
	visibility: hidden;
	margin: 0;
	padding: 0;
	background: #EAEBD8;
	border: 1px solid #5970B2}

	#sddm div a
	{	position: relative;
		display: block;
		margin: 0;
		padding: 5px 10px;
		width: auto;
		white-space: nowrap;
		text-align: left;
		text-decoration: none;
		background: #EAEBD8;
		color: #2875DE;
		font: 11px arial}

	#sddm div a:hover
	{	background: #49A3FF;
		color: #FFF}
</style>
<script type="text/javascript" >
var timeout	= 500;
var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function mopen(id)
{	
	// cancel close timer
	mcancelclosetime();

	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function mclosetime()
{
	closetimer = window.setTimeout(mclose, timeout);
}

// cancel close timer
function mcancelclosetime()
{
	if(closetimer)
	{
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}


function setIframeSrc(tableLink, listLink,h1Header,h2Header){
	document.getElementById("createDHeader").innerText=h1Header;
	document.getElementById("listDHeader").innerText=h2Header;
	document.getElementById("createTableIframe").setAttribute("src", tableLink);
	document.getElementById("listTableIframe").setAttribute("src", listLink);
}

function frameRefresh(){
	var listLink=document.getElementById("listTableIframe").getAttribute("src");
	document.getElementById("listTableIframe").setAttribute("src", listLink);
}

function delayRefresh(){
	setTimeout(function(){ frameRefresh() }, 1000);
}

function formSubmit() {
	document.getElementById("logoutForm").submit();
}
// close layer when click-out
document.onclick = mclose;


</script>
</head>
<body>
<!-- <div align="center"> -->
<%-- <c:if test="${pageContext.request.userPrincipal.name != null}"> --%>
<%-- 		<h2>Welcome : ${pageContext.request.userPrincipal.name} | <a href="<c:url value="/j_spring_security_logout" />" > Logout</a></h2>   --%>
<%-- 	</c:if> --%>
<!-- </div> -->


<c:url value="/j_spring_security_logout" var="logoutUrl" />
<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" 
			name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	
	<c:if test="${pageContext.request.userPrincipal.name != null}">
		<h2>
			Welcome : ${pageContext.request.userPrincipal.name} | <a
				href="javascript:formSubmit()"> Logout</a>
		</h2>
	</c:if>

<ul id="sddm">
    <li><a href="#" 
        onmouseover="mopen('m1')" 
        onmouseout="mclosetime()">Admin</a>
        <div id="m1" 
            onmouseover="mcancelclosetime()" 
            onmouseout="mclosetime()">
       <a onclick="setIframeSrc('create-EmbroideryDesigner','embroideryDesigners','Create a new designer','Embroidery Designers')">Create Designer</a>
        <a onclick="setIframeSrc('create-embroideryItem','embroideryItems','Create a new item','Embroidery Items')">Create Item</a>
        <a onclick="setIframeSrc('create-EmbroideryUser','embroideryUsers','Create a new user','Embroidery Users')">Create User</a>
        <a onclick="setIframeSrc('create-NewPromotion','embroideryPromotions','Create a new promotion','Embroidery Promotions')">Create Promotion</a>
        <a onclick="setIframeSrc('create-NewPackage','embroideryPackages','Create a new Package','Embroidery Packages')">Create Package</a>
    	<a onclick="setIframeSrc('createCategory','categories','Create a new Category','Embroidery Categories')">Create Category</a>
        </div>
    </li>
</ul>
<div style="clear:both"></div>
<h1 align="center" id="createDHeader"></h1>
<iframe width="100%" height="300px" name="createTableIframe" id="createTableIframe" style="border:none" ></iframe>



<h2 align="center" id="listDHeader"></h2>
<iframe width="100%" height="430px" name="listTableIframe" id="listTableIframe" style="border:none" ></iframe>


</body>

</html>