<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<script type="text/javascript">
		function insertUser(){
			alert("User "+document.getElementById("idUserFullName").value+" will be added to database");
// 			document.getElementById("idUserActive").innerText=1;
			document.getElementById("userEditor").submit();
			
		}
		function updatePromo(){
			var check=confirm("Do you want to Update User "+document.getElementById("idUserFullName").value);
			if(check==true) {
				document.getElementById("saveButton").disabled=true;
				document.getElementById("updateButton").disabled=true;
				document.getElementById("deleteButton").disabled=true;
				document.getElementById("userEditor").submit();
			}
						
		} 
		function deleteUser(){
			var check=confirm("Do you want to Delete User "+document.getElementById("idUserFullName").value);
			if(check==true) {			
				document.getElementById("idUserActive").value='0';			
				document.getElementById("userEditor").submit();
			}
			
		}
		
		function frameRefresh(){
			 parent.frameRefresh()
			}

			function delayRefresh(){
			 setTimeout(function(){ frameRefresh() }, 1000);
			}
	</script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
    <link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
    <title>Create  User for Embroidery</title>
</head>
<body onload="delayRefresh()">

<form:form modelAttribute="embroiderUser" method="post" id="userEditor">
    <table align="center">
         <tr style="display: none;">
            <td >id </td>
            <td>
                <form:input path="userID" id="idUserID"/>
                <form:errors path="userID" element="span"/>
            </td>
        </tr>
        <tr>
            <td>Full Name*: </td>
            <td>
                <form:input path="userFullName" id="idUserFullName"/>
                <form:errors path="userFullName" element="span"/>
            </td>
        </tr>
         <tr>
            <td>User Name*: </td>
            <td>
                <form:input path="userName" id="idUserName"/>
                <form:errors path="userName" element="span"/>
            </td>
        </tr>
        <tr>
            <td>Email*:</td>
            <td>
                <form:input path="userEmail" id="idUserEmail" />
                <form:errors path="userEmail" element="span"/>
            </td>
        </tr>
        <tr>
            <td>Password*:</td>
            <td>
                <form:password  path="userPassword" id="idUserPassword" />
                <form:errors path="userPassword" element="span"/>
            </td>
        </tr>
         <tr>
            <td>Address:</td>
            <td>
                <form:input path="userAddress" id="idUserAddress" />
                <form:errors path="userAddress" element="span"/>
            </td>
        </tr>        
         <tr style="display: none;" >
            <td>active</td>
            <td>
                <form:input path="userActive" id="idUserActive" />
                <form:errors path="userActive" element="span"/>
            </td>
        </tr> 
        
         <tr>
         	<td>
          <input type="button" value="Manage" onclick="insertUser()" id="saveButton"/> 
          	</td>
          	
          	<td>
          <input type="button" value="Update" onclick="updatePromo()" id="updateButton" /> 
          	</td>
          	
          	<td>
          <input type="button" value="Delete" onclick="deleteUser()" id="deleteButton" /> 
          	</td>
          	
          </tr>
    </table>
    <br/>
   
</form:form>

</body>
</html>