<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>


<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
table, td, th {
    border: 1px solid #5970B2;
}

th {
    background-color: #5970B2;
    color: white;
}
</style>
<script type="text/javascript">
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    
    this.showRecords = function(from, to) {        
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)  
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
    
    this.showPage = function(pageNumber) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}

        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
        
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
        
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }   
    
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                        
    
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1); 
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }

    this.showPageNav = function(pagerName, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);
    	
    	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
        for (var page = 1; page <= this.pages; page++) 
            pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
        pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
        
        element.innerHTML = pagerHtml;
    }
}

</script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
	<link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
	<title>Embroidery Users</title>
	
	<script type="text/javascript">
	
		function setValues(row){
			var cells=row.getElementsByTagName("td");
			var check=confirm("Do you want to pass User "+cells[1].innerText+" data to the update page?");
			if(check==true) {				
				
				parent.createTableIframe.idUserID.value=cells[0].innerText;				
				parent.createTableIframe.idUserFullName.value=cells[1].innerText;
				parent.createTableIframe.idUserName.value=cells[2].innerText;
				parent.createTableIframe.idUserEmail.value=cells[3].innerText;
				parent.createTableIframe.idUserPassword.value=cells[4].innerText;
				parent.createTableIframe.idUserAddress.value=cells[5].innerText;
				parent.createTableIframe.idUserActive.value=cells[6].innerText;
				
								
				
			}
						
		}
		

		function searchUser(){
			
			document.getElementById("idUserMode").value="SEARCH";
			document.getElementById("searchUserForm").submit();
		}

		function clrSearch(){
			document.getElementById("searchUserNameText").value="";
		 	document.getElementById("searchUserEmailText").value="";
		}

	</script>
</head>
<body>
<!-- <h2>Embroidery Users </h2> -->
<form:form modelAttribute="embroiderUser" method="post" id="searchUserForm">
	<table align="center" id="searchUserTable" style="border: none;">
		<tr style="border: none;">
			<td style="border: none;"><b>User Full Name:-</b> </td>
			<td style="border: none;">
				<form:input path="userFullName" id="searchUserNameText"/>
                <form:errors path="userFullName" element="span"/>
			</td>
			<td style="border: none;"><b>User Name:-</b></td>
			<td style="border: none;">
				<form:input path="userName" id="searchUserUnameText"/>
                <form:errors path="userName" element="span"/>
			</td>
			<td style="display: none;">
				<form:input path="mode" id="idUserMode"/>
                <form:errors path="mode" element="span"/>
			</td>
			<td style="border: none;"><input type="button" value="Search" id="dSearch" onclick="searchUser()"></td>
			<td style="border: none;"><input type="button" value="Clear" id="searchClear" onclick="clrSearch()"/></td>
			<td style="border: none;"><input type="button" value="Show All" id="showAllResultsButton" onclick="parent.frameRefresh()"></td>
		</tr>
	</table>
</form:form>
	<br><br>
	<div id="pagDisplayDiv" align="center"></div>
		<table align="center" id="listUsersTable">
		    <tr>
		    	<th style="display:none;">id</th>
		        <th>Full Name</th>
		        <th>User Name</th>
		        <th>Email Address</th>
		        <th>Password</th>
		        <th>Address</th>
		        <th style="display:none;"> active </th>  
		          
		       
		    </tr>
		    <c:forEach items="${embroiderUsers}" var="embroiderUsers">
		        <tr onclick="setValues(this)" style="cursor:hand" name="selectRow">
		        	<td style="display:none;">${embroiderUsers.userID}</td>
		            <td>${embroiderUsers.userFullName}</td>
		            <td>${embroiderUsers.userName}</td>
		            <td>${embroiderUsers.userEmail}</td>
		            <td>${embroiderUsers.userPassword}</td>
		            <td>${embroiderUsers.userAddress}</td>
		            <td style="display:none;">${embroiderUsers.userActive}</td>
		           
		        </tr>
		    </c:forEach>
		</table>
	<br>
	<script type="text/javascript">
        var pager = new Pager('listUsersTable', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pagDisplayDiv'); 
        pager.showPage(1);
    </script>
	
	
	
<br>
<%-- <a href="<c:url value="/create-EmbroideryUser"/>">Create EmbroideryUser </a> --%>
<br>
<br>
<%-- <form:form  modelAttribute="embroiderUser" method="post" commandName="embroiderUser">  --%>
<!-- 	<table style="display:none"> -->
<!-- 		<tr> -->
<!-- 			<td> -->
<!-- 				User Id:	</td> -->
<!-- 			<td> -->
				
<%--                 <form:input  path="userID" id="idUserId"/> --%>
<%--                 <form:errors path="userID" element="span"/> --%>
                
<!--             </td>             -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td> -->
<!-- 				User Name:	</td> -->
<!-- 			<td> -->
<%--                 <form:input path="userFullName" id="idUserName"/> --%>
<%--                 <form:errors path="userFullName" element="span"/> --%>
<!--             </td>             -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>User Email: </td> -->
<!-- 			<td> -->
<%-- 				<form:input path="userEmail" id="idUserEmail"/> --%>
<%--                 <form:errors path="userEmail" element="span"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>User Password: </td> -->
<!-- 			<td> -->
<%-- 				<form:input path="userPassword" id="idUserPassword"/> --%>
<%--                 <form:errors path="userPassword" element="span"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>Address: </td> -->
<!-- 			<td> -->
<%-- 				<form:input path="userAddress" id="idUserAddress"/> --%>
<%--                 <form:errors path="userAddress" element="span"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
<!-- 		<tr> -->
<!-- 			<td>active: </td> -->
<!-- 			<td> -->
<%-- 				<form:input path="userActive" id="idUserActive"/> --%>
<%--                 <form:errors path="userActive" element="span"/> --%>
<!-- 			</td> -->
<!-- 		</tr> -->
		
		
		
		
				
<!-- 	</table> -->
<!-- 	<input style="display:none; " type="submit" value="Update" />  -->
<%-- </form:form> --%>

</body>
</html>