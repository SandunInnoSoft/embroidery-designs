<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a new type</title>
</head>
<body>
<h2>Create a new type</h2>
<form:form modelAttribute="embroideryType" method="post">
    <table>
        <tr>
            <td>Name:</td>
            <td>
                <form:input path="name"/>
                <form:errors path="name" element="span"/>
            </td>
        </tr>
        
        <tr>
            <td>Description:</td>
            <td>
                <form:input path="description"/>
                <form:errors path="description" element="span"/>
            </td>
        </tr>
    </table>
    <br/>
    <input type="submit" value="Create" />
</form:form>
</body>
</html>