<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Embroidery Designers</title>
</head>
<body>
<h2>EmbroideryDesigners</h2>
<table>
    <tr>
        <th>Type Name</th>
        <th>Description</th>
    </tr>
    <c:forEach items="${embroideryTypes}" var="embroideryType">
        <tr>
            <td>${embroideryType.name}</td>
            <td>${embroideryType.description}</td>
            
        </tr>
    </c:forEach>
</table>
<br/>
<a href="<c:url value="/create-EmbroideryType"/>">Create EmbroideryType</a>
</body>
</html>