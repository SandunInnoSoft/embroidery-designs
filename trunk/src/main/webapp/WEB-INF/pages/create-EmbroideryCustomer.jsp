<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a new customer</title>
</head>
<body>
<h2>Create a new Customer</h2>

<form:form modelAttribute="embroideryCustomer" method="post">
    <table>
        <tr>
            <td>Customer Name:</td>
            <td>
                <form:input path="customerName"/>
                <form:errors path="customerName" element="span"/>
            </td>
        </tr>
        
         <tr>
            <td>E-mail Address:</td>
            <td>
                <form:input path="emailAddress"/>
                <form:errors path="emailAddress" element="span"/>
            </td>
        </tr>
         
         <tr>
            <td>Phone Number 1:</td>
            <td>
                <form:input path="phoneNumber"/>
                <form:errors path="phoneNumber" element="span"/>
            </td>
        </tr>
        
        
    </table>
    <br/>
    <input type="submit" value="Create" />
</form:form>

</body>
</html>