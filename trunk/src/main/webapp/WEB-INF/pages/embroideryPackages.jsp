<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
	<link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
	<title>Embroidery Packages</title>
	<script type="text/javascript">
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    
    this.showRecords = function(from, to) {        
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)  
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
    
    this.showPage = function(pageNumber) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}

        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
        
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
        
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }   
    
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                        
    
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1); 
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }

    this.showPageNav = function(pagerName, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);
    	
    	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
        for (var page = 1; page <= this.pages; page++) 
            pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
        pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
        
        element.innerHTML = pagerHtml;
    }
}

</script>
	
	<script type="text/javascript">
		function setValues(row){
			var cells=row.getElementsByTagName("td");
			var check=confirm("Do you want to pass Package "+cells[1].innerText+" data to the update page?");
			if(check==true) {
				parent.createTableIframe.idPackageId.value=cells[0].innerText;				
				parent.createTableIframe.idPackageName.value=cells[1].innerText;
				parent.createTableIframe.idPackageDescription.value=cells[2].innerText;
				parent.createTableIframe.idActive.value=cells[3].innerText;				
			}
						
		}
		function searchPackage() {
			
			document.getElementById("idPackageMode").value="SEARCH";
			document.getElementById("searchPackageForm").submit();
		}

		function clrSearch(){
			document.getElementById("searchPackageNameText").value="";
		 	
		}
	</script>
	<style type="text/css">
	
		table, td, th {
    		border: 1px solid #5970B2;
		}

	th {
    	background-color: #5970B2;
    	color: white;
	}
	
</style>
	
</head>
<body>
<form:form modelAttribute="embroideryPackage" method="post" id="searchPackageForm">
	<table align="center" id="searchPackageTable" style="border: none;">
		<tr style="border: none;">
			<td style="border: none;"><b>Package Name:-</b> </td>
			<td style="border: none;">
				<form:input path="packageName" id="searchPackageNameText"/>
                <form:errors path="packageName" element="span"/>
			</td>			
			<td style="display: none;">
				<form:input path="mode" id="idPackageMode"/>
                <form:errors path="mode" element="span"/>
			</td>
			<td style="border: none;"><input type="button" value="Search" id="dSearch" onclick="searchPackage()"></td>
			<td style="border: none;"><input type="button" value="Clear" id="searchClear" onclick="clrSearch()"/></td>
			<td style="border: none;"><input type="button" value="Show All" id="showAllResultsButton" onclick="parent.frameRefresh()"></td>
		</tr>
	</table>
</form:form>
<br><br>
<div id="pagDisplayDiv" align="center"></div>
<table align="center" id="listPackagesTable">
	<tr>
		<th style="display:none;">Package ID </th>
		<th>Package Name </th>
		<th>Package Description </th>
		<th style="display:none;">active</th>
		
	</tr>
	<tr >
		 <c:forEach items="${embroideryPackages}"  var="embroideryPackages">
       	<tr onclick="setValues(this)" style="cursor:hand">
        	
        	<td style="display:none;">${embroideryPackages.packageId}</td>
            <td>${embroideryPackages.packageName}</td>
            <td>${embroideryPackages.packageDescription}</td>
            <td style="display:none;">${embroideryPackages.packageActive}</td>
            
        </tr>
    </c:forEach>
	</tr>
	
</table>
<script type="text/javascript">
        var pager = new Pager('listPackagesTable', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pagDisplayDiv'); 
        pager.showPage(1);
    </script>
</body>
</html>