<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EmbroideryRequests</title>
</head>
<body>
<h1>Embroidery Requests</h1>
<table>
    <tr>
        <th>Width</th>
        <th>Height</th>
        <th>Description</th>
        
    </tr>
    <c:forEach items="${embroideryRequests}" var="embroideryRequest">
        <tr>
            <td>${embroideryRequest.width}</td>
            <td>${embroideryRequest.height}</td>
            <td>${embroideryRequest.description}</td>
            
        </tr>
    </c:forEach>
</table>
<br/>
<a href="<c:url value="/create-EmbroideryRequest"/>">Create Embroidery Request</a>
</body>
</html>