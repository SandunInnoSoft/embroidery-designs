<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
SELECT, INPUT[type="text"] {
	width: 160px;
	box-sizing: border-box;
}

SECTION {
/* 	margin:20px; */
	padding-top:10px;
	padding-bottom:10px;	
	background-color: white;
	overflow: auto;
}

SECTION>DIV {
 	float: left;
	padding: 4px;
}

SECTION>DIV+DIV {
	width: 40px;
	text-align: center;
}
</style>
	
<!-- 	<script src="dist/libs/jquery.js"></script> -->
  <!-- 5 include the minified jstree source -->
  
  
<!-- 	<link rel="stylesheet" media="all" -->
<%--  href="<c:url value="/dist/themes/default/style.css"/>"> --%>

<link rel="stylesheet" href="dist/themes/default/style.min.css" />	
	
	
	<script type="text/javascript">
		function insertPackage(){			 
	            
			alert("Package "+document.getElementById("idPackageName").value+" will be added to database");
			document.getElementById("packageForm").submit();			
		}
		function updatePackage(){
			var check=confirm("Do you want to Update Package "+document.getElementById("idPackageName").value);
			if(check==true) {
				document.getElementById("saveButton").disabled=true;
				document.getElementById("updateButton").disabled=true;
				document.getElementById("deleteButton").disabled=true;
				document.getElementById("packageForm").submit();
				
			}
		}
		function deletePackage(){
			var check=confirm("Do you want to Delete Package "+document.getElementById("idPackageName").value);
			if(check==true) {
			document.getElementById("idActive").value='0';
			document.getElementById("packageForm").submit();
			}
		}
		
		function frameRefresh(){
			 parent.frameRefresh()
			}

			function delayRefresh(){
			 setTimeout(function(){ frameRefresh() }, 1000);
			}
	</script>
	 
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script>
$('.Collapsable').click(function(){ $(this).children().toggle(); });
</script>
  
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
    <link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
    <title>Create  Package for Embroidery</title>
   
 <div class="container">
   <div class="left"></div>
   <div class="right"></div>
</div>

<style type="text/css">
    .container {
      width:500px;
      
    }
    .left {
      width:50px;
      float:left;
    }
    .right {
      width:450px;
      float:right;
    }
    .rightright{
    	width:200px;
    	float:right
    }
    .rightleft{
    	width:250px;
    	float:left;
    }
</style>
   
  <script type="text/javascript"> 
	function viewImage(selected){
		
	}
	</script> 
<script>

// $(document).ready(function(){
// 	$("#treeItm").click(function(){
// 		$.get("image",
// 				{
// 					id: $("#idField").val()
// 				},
// 				function(data){
// 					var $response=$(data);
// 					alert("Before refreshed...");
// 		            $("#imgView").html( $response.find("#imgNew").html());
// 		        }
// 		        );
//     });
// });

</script>
	
</head>
<body onload="delayRefresh()">
<div class="container">
   <div class="left"></div>
   <div class="right"></div>
</div>

<div class="left">
<form:form modelAttribute="embroideryPackage" method="post" id="packageForm"> 
	<table align="center">
		
		<tr style="display:none; ">
			<td>
				id	</td>
			<td>
                <form:input path="packageId" id="idPackageId"/>
                <form:errors path="packageId" element="span"/>
            </td> 
            <td></td>           
		</tr>
		<tr>
			<td>
				Package Name:	</td>
			<td>
                <form:input path="packageName" id="idPackageName"/>
                <form:errors path="packageName" element="span"/>
            </td>            
		</tr>
		<tr>
			<td>Package Description: </td>
			<td>
				<form:input path="packageDescription" id="idPackageDescription"/>
                <form:errors path="packageDescription" element="span"/>
			</td>
			<td></td>
		</tr>
		<tr style="display:none; ">
			<td>
				active	</td>
			<td>
                <form:input path="packageActive" id="idActive" />
                <form:errors path="packageActive" element="span"/>
            </td> 
            <td></td>           
		</tr>
		<tr >
			<td>
				items </td>
			<td>
                <form:hidden path="as" id="myField" />
<!--                <input type="hidden" id="myField" value="" name="myField"/> -->
                <form:errors path="as" element="span"/>
            </td> 
            <td></td>           
		</tr>
		<tr>
		<td><input type="button" value="Save" id="saveButton" /></td>
				
		<td><input type="button" value="Update" onclick="updatePackage()" id="updateButton" /></td>
		
		<td><input type="button" value="Delete" onclick="deletePackage()" id="deleteButton" /></td>
		</tr>
			
	</table>
	
	<!-- This Below code to set the mode to get Image -->
	
	<form:hidden path="mode" id="idMode" />
	<!-- -------------------------------------------- -->
	
	</form:form>
</div>
<div class="right">
	<div class="rightleft">
<%-- 	<form:form modelAttribute="embroideryItem" method="post" id="packageForm"> --%>
<%-- 		<c:forEach items="@{items}" var="items"> --%>
<%-- 			<img id="displayPic" height="250" width="250" src="${items.imagePath}"> --%>
<%-- 		</c:forEach> --%>
<%-- 	</form:form> --%>
				
	</div>
	<div id="imgView" style="width: 200px;height: 200px;border: 1px solid black;float: left;"></div>	
	<div class="rightright">
								
				  <ul>
					  <c:forEach items="${categories}"  var="categories">
				  		<li style="cursor: pointer;" ><span class="Collapsable">
<%-- 					   <input type="checkbox" > --%><label>${categories.catName}</label>   </span>
					    									
							<ul> 	    	
						    	<c:forEach items="${itemCategories}" var="itemCategories" >
						    		
						    		<c:if test="${itemCategories.catId.id == categories.id}" >
						    		<li style="cursor: pointer;" id="${itemCategories.itemId.id}"  value="${itemCategories.itemId.imagePath}" onclick="clickList(this)" >   
						    			<span id="treeItm" class="Collapsable">
						    				<input type="checkbox" name="nameItems"  value="${itemCategories.itemId.id}" onclick="">  ${itemCategories.itemId.fileName}						    				
						    		    </span>	 
						    		</li>						    		
						    		</c:if>
						    	</c:forEach>	    
						      			        
						      </ul>
					    </li>
					    </c:forEach>
					    
				  </ul>
				  
			<!-- 	  <input type="text" id="selList" name="selList" style="display:none; "> -->
				  <button type="button" id="setValues">Get Values</button>
	</div>
	
	
<input name="idField" id="idField" type="hidden">


</div>
<script type="text/javascript">
	$(document).ready(function() {		 
		 $(".Collapsable").parent().children().toggle();
	        $(".Collapsable").toggle();
	});
	
    $(".Collapsable").click(function () {

        $(this).parent().children().toggle();
        $(this).toggle();

    });

   
function setSelectedValues(selId){
	document.getElementById('myField').value=document.getElementById('myField').value + "," + selId;
	
}
$(document).ready(function() {
        $("#saveButton").click(function(){	
            var favorite = [];
            $.each($("input[name='nameItems']:checked"), function(){            
                favorite.push($(this).val());
                var value1=$(this).val();  
                var subValue;
//                 $("#selectedItems").append(value1);				
//                 	$('#selList').val($('#selList').val() + value+',');               
// 		$('#selectedItems').append($('<option>', {
// 					    value: value1,
// 					    text: value1
// 					}));		
//                 alert($("#selectedItems:selected").val());
            });
           
            
//             alert("My favourite sports are: " + selectedItems.getSelectedItems());
            document.getElementById("myField").value=favorite;
            alert(document.getElementById("myField").value);
            insertPackage();
        });
        
        $('#idCheckUrl').click(function(){
// 			var url=$("hisUrl").val();
			alert("Li Clicked");
// 			$("#displayPic").attr("src",url);
		});
        
        $("#idCheckUrl").click(function(){
        	var item;
        	$.each($("input[name='nameItems']:"))
        	
        })
    });
   
</script>
<script type="text/javascript">
	function clickList(list){
		var id=list.id;
		
// 		document.getElementById("idMode").value=id;
// 		document.getElementById("idField").value=id;
		
		var xmlhttp;
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		    {
		    document.getElementById("imgView").innerHTML=xmlhttp.responseText;
		    }
		  }
		xmlhttp.open("GET","image?id="+id,true);
		xmlhttp.send();
	
	}
</script>

</body>
</html>