<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add a new designer</title>
<script type="text/javascript">

//------These variables will only be used when updating a designer------!
var designerId="";
var designerName="";
var designerEmail1="";
var designerEmail2="";
var designerPhone1="";
var designerPhone2="";
var designerAddress="";
var designerDescription="";
var active=0;
//--------------------------------------------------------------------

function preUpdateValuesSetter(dId,dName,email1,email2,phone1,phone2,address,description,act){
	designerId=dId;
	designerName=dName;
	designerEmail1=email1;
	designerEmail2=email2;
	designerPhone1=phone1;
	designerPhone2=phone2;
	designerAddress=address;
	designerDescription=description;
	active=act;
}


function preUpdateReferenceCheck(){
	
	//if(document.getElementById("dAddress").value!=designerAddress){
		var check=confirm("Do you want to proced the update?");
		if(check==true){
			updateDesigner();
		}
		//}
	//}else{
		//alert("Nothing to update");
	//}
	
}



 function val(){
 	var dName=document.getElementById("dName").value;
	var eMail1=document.getElementById("dEmail1").value;
	var eMail2=document.getElementById("dEmail2").value;
	var phone1=document.getElementById("dPhone1").value;
	var phone2=document.getElementById("dPhone2").value;
	var address=document.getElementById("dAddress").value;
	var description=document.getElementById("dDescription").value;
	
	if(dName=="" || eMail1=="" || phone1=="" || address=="")
		{
		alert("Text boxes cannot be empty!");
		}
	else{
		document.getElementById('dSubmit').disabled=true;
		document.getElementById("idDesignerMode").value="CREATE";
		document.getElementById("createDesignerForm").submit();
	}
 }

function clr(){
	
 	document.getElementById("dName").value="";
 	document.getElementById("dEmail1").value="";
 	document.getElementById("dEmail2").value="";
 	document.getElementById("dPhone1").value="";
 	document.getElementById("dPhone2").value="";
 	document.getElementById("dAddress").value="";
 	document.getElementById("dDescription").value="";
 	if(document.getElementById('dSubmit').disabled=true){
 		document.getElementById('dSubmit').disabled=false;
 	}
	
}




function updateDesigner(){
	var check=confirm("Do you want to proced the update?");
	if(check==true){
		document.getElementById('dUpdate').disabled=true;
		document.getElementById("idDesignerMode").value="UPDATE";
		document.getElementById("createDesignerForm").submit();
	}	
}

function deleteDesigner(){
	var check=confirm("Do you want to delete the record of designer "+document.getElementById("dName").value);
	if(check==true){
		document.getElementById('dDelete').disabled=true;
		document.getElementById("idDesignerMode").value="DELETE";
		document.getElementById("createDesignerForm").submit();
	}else{
		doNothing();
	}
	
}

function doNothing(){}


</script>

<style type="text/css">


</style>
</head>

<body onload="parent.delayRefresh()">
<form:form modelAttribute="embroideryDesigner" method="post" id="createDesignerForm">

    <table align="center">
    	<tr style="display: none;">
    	<td>ID</td>
    	<td><form:input path="id" id="dId"/></td>
    	</tr>
        <tr>
            <td><b>Name*:</b></td>
            <td>
                <form:input path="designerName" id="dName"/>
                <form:errors path="designerName" element="span"/>
            </td>
        	
            <td><b>Address*:</b></td>
            <td>
                <form:input path="address" id="dAddress"/>
                <form:errors path="address" element="span"/>
            </td>
        </tr>
         <tr>
            <td><b>E-mail Address 1*:</b></td>
            <td>
                <form:input path="emailAddress1" id="dEmail1"/>
                <form:errors path="emailAddress1" element="span"/>
            </td>
        
            <td><b>E-mail Address 2:</b></td>
            <td>
                <form:input path="emailAddress2" id="dEmail2"/>
                <form:errors path="emailAddress2" element="span"/>
            </td>
        </tr>
         <tr>
            <td><b>Phone Number 1*:</b></td>
            <td>
                <form:input path="phoneNumber1" id="dPhone1"/>
                <form:errors path="phoneNumber1" element="span"/>
            </td>
        
            <td><b>Phone Number 2:</b></td>
            <td>
                <form:input path="phoneNumber2" id="dPhone2"/>
                <form:errors path="phoneNumber2" element="span"/>
            </td>
        </tr>
        
        <tr>
            <td><b>Description:</b></td>
            <td>
                <form:input path="description" id="dDescription"/>
                <form:errors path="description" element="span"/>
            </td>
        </tr>
         <tr style="display: none;">
            <td>active</td>
            <td>
                <form:input path="active" id="idDesignerActive" />
                <form:errors path="active" element="span"/>
            </td>
            <td>Mode</td>
            <td>
            	<form:input path="mode" id="idDesignerMode" />
                <form:errors path="mode" element="span"/>
            </td>
        </tr> 
        <tr>
        <td><input type="button" value="Save" id="dSubmit" onclick="val()"/></td>
        <td><input type="button" value="Clear" id="dClear" onclick="clr()"/></td>
        <td><input type="button" value="Update" id="dUpdate" onclick="updateDesigner()"></td>
        <td><input type="button" value="Delete" id="dDelete" onclick="deleteDesigner()"></td>
        </tr>    
</form:form>
</body>
</html>