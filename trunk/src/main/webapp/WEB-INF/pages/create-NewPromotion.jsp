<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script type="text/javascript">
		
	function insertUpdateFunction(){		
		
		alert("Promotion "+document.getElementById("idPromoName").value+" will be added to Database");			
			
		document.getElementById("saveButton").disabled=true;
		document.getElementById("updateButton").disabled=true;
		document.getElementById("deleteButton").disabled=true;
		document.getElementById("idFormPromotion").submit();
		
	}
// 	function formValidator(){
// 		if(){
// 			isValidated='1';
// 		}else{
// 			isValidated='0';
// 		}
// 	}	
	
	function deleteFunction(){
		var check=confirm("Do you want to Delete promotion "+document.getElementById("idPromoName").value);
		if(check==true) {				
			document.getElementById("idPromoActive").value='0';
			document.getElementById("idFormPromotion").submit();			
			
		}else{ 
			
		}
		
	}
	function updatePromo(){
		var check=confirm("Do you want to Update promotion "+document.getElementById("idPromoName").value);
		if(check==true) {				
			document.getElementById("idFormPromotion").submit();			
			
		}else{ 
			
		}
					
	} 
	
	function frameRefresh(){
		 parent.frameRefresh()
		}

		function delayRefresh(){
		 setTimeout(function(){ frameRefresh() }, 1000);
		 checkLock();
		}
	
</script>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
    <link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
    <title>Create  New Promotion</title>
</head>
<body onload="delayRefresh()" id="idBody" >

<form:form modelAttribute="embroideryPromotion" method="post" id="idFormPromotion"> 
	<table align="center">	
		<tr style="display: none;">
			<td>id </td>
			<td>							
				<form:input path="promoId"  id="idPromoId"/>				
				<form:errors path="promoId" element="span"/>
			</td>
			<td></td>
		</tr>	
		<tr>
			<td>Promotion Name*:</td>
			<td>
				<form:input path="promoName" id="idPromoName"/>
                <form:errors path="promoName" element="span"/>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>Promotion Description</td>
			<td>
				<form:textarea  path="promoDescrip" id="idPromoDescrip"/>
                <form:errors path="promoDescrip" element="span"/>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>Start Date*: </td>
			<td>
				<input type="date" id="idStartDateSel" name="idStartDateSel"/>				
<%-- 				<form:input path="promoStartDate" id="idStartDate"/>				 --%>
<%-- 				<form:errors path="promoStartDate" element="span"/> --%>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>End Date*: </td>
			<td>
				<input type="date" id="idEndDateSel" name="idEndDateSel"/>
<%-- 				<form:input  id="idEndDate" />				 --%>
<%-- 				<form:errors  element="span"/> --%>
			</td>
			<td></td>
		</tr>
		<tr>
			<td>Discount*:</td>
			<td>
				<form:input path="promoDiscount" id="idPromoDiscount"/>
                <form:errors path="promoDiscount" element="span"/>
			</td>
			<td></td>
		</tr>	
		<tr style="display: none;">
			<td>active</td>
			<td>
				<form:input path="promoActive" id="idPromoActive"/>
                <form:errors path="promoActive" element="span"/>
			</td>
			<td></td>
		</tr>		
		<tr>
			<td><input type="radio" name="selection" id="package"  checked="checked" onchange="checkLock()"> Package  :  </td>
		<td>	
			<input type="radio" name="selection" id="item" onchange="checkLock()"> Item </td>
		</tr>	
		<tr id="idRowPackage">
			
			<td>
				Packages : </td>
				<td> 	 
				<select id="idPackageSelect" onchange="getSelectedPackage()">
					<c:forEach items="${packages}" var="packages">
						<option id="${packages.packageId}" value="${packages.packageId}"> ${packages.packageName} </option> 
					</c:forEach>
				</select>
				<form:hidden path="mode" id="idPackageSel"/>
				<form:errors path="mode" element="span"/>
				
                
			</td>
			<td></td>
		</tr>
		<div id="itemSearch">
			<tr id="idRowItem">
				<td><input type="text" id="idSearchString"> : <input type="button" id="idSearchButton" value="Search>" onclick="checkLock()"></td>
			</tr>
		</div>	
		<tr>
		<td><input type="button" value="Save" onclick="insertUpdateFunction()" id="saveButton" /></td>
		<td><input type="button" value="Update" onclick="updatePromo()" id="updateButton" /></td>
		<td><input type="button" value="Delete" onclick="deleteFunction()" id="deleteButton" /></td>
		</tr>
	</table>
	
	<form:hidden path="type" id="idSelectionType"/> 
	<form:errors path="type" element="span"/>
	
</form:form>
<script type="text/javascript">
function checkLock(){
	if(document.getElementById("package").checked){
		document.getElementById("itemSearch").style.display='none';
		document.getElementById("idRowPackage").style.display='inline';
		document.getElementById("idSelectionType").value="package"
		
	}else{
		document.getElementById("itemSearch").style.display='inline';
		document.getElementById("idRowPackage").style.display='none';
		document.getElementById("idSelectionType").value="item"
	}
// 	document.getElementById("idRowPackage").style.display='none';

}
function getSelectedPackage(){
// 	var id=selection.id;
	var e = document.getElementById("idPackageSelect");
	var strUser = e.options[e.selectedIndex].value;
	document.getElementById("idPackageSel").value=strUser;
	
}

</script>
<script type="text/javascript">
// $('select').change(function () {
// 	  if ($(this).val() === 'New') {
// 	    // Handle new option
// 	  }
// 	});
</script>
</body>
</html>