<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page trimDirectiveWhitespaces="true"%>

<html>
<head>

<script type="text/javascript">
		
	function insertFunction(){		
		
		alert("Category "+document.getElementById("idName").value+" will be added to Database");			
			
		document.getElementById("saveButton").disabled=true;
		document.getElementById("updateButton").disabled=true;
		document.getElementById("deleteButton").disabled=true;
		document.getElementById("idCatForm").submit();
		
	}
// 	function formValidator(){
// 		if(){
// 			isValidated='1';
// 		}else{
// 			isValidated='0';
// 		}
// 	}	
	
	function deleteCat(){
		var check=confirm("Do you want to Delete Category "+document.getElementById("idName").value);
		if(check==true) {				
			document.getElementById("idActive").value='0';
			document.getElementById("idCatForm").submit();			
			
		}else{ 
			
		}
		
	}
	function updateCat(){
		var check=confirm("Do you want to Update Category "+document.getElementById("idName").value);
		if(check==true) {				
			document.getElementById("idCatForm").submit();			
			
		}else{ 
			
		}
					
	} 
	
	function frameRefresh(){
		 parent.frameRefresh()
		}

		function delayRefresh(){
		 setTimeout(function(){ frameRefresh() }, 1000);
		}
	
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/site.css"/>">
<title>Create Category Form</title>
</head>
<body onload="delayRefresh()">

<form:form modelAttribute="category" method="post" id="idCatForm">
    <table align="center">
    	 <tr style="display: none;">
            <td>id:</td>
            <td>
                <form:input path="id" id="idCat"/>
                <form:errors path="id" element="span"/>
            </td>
        </tr>
        <tr>
            <td>Category Name:</td>
            <td>
                <form:input path="catName" id="idName"/>
                <form:errors path="catName" element="span"/>
            </td>
        </tr>
        <tr>
            <td>Description:</td>
            <td>
                <form:input path="description" id="idDescription"/>
                <form:errors path="description" element="span"/>
            </td>
        </tr>
         <tr style="display: none;">
            <td>active:</td>
            <td>
                <form:input path="active" id="idActive" />
                <form:errors path="active" element="span"/>
            </td>
        </tr>
    
    
    <tr>
    <td><input type="button" value="Save" onclick="insertFunction()" id="saveButton" /></td>
		<td><input type="button" value="Update" onclick="updateCat()" id="updateButton" /></td>
		<td><input type="button" value="Delete" onclick="deleteCat()" id="deleteButton" /></td>
	</tr>
</table>
</form:form>	
</body>
</html>