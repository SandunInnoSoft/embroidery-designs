<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Embroidery Customers</title>
</head>
<body>
<h2>EmbroideryCustomers</h2>
<table>
    <tr>
        <th>Customer Name</th>
        <th>E-Mail Address</th>
        <th>Phone Number </th>
    </tr>
    <c:forEach items="${embroideryCustomers}" var="embroideryCustomer">
        <tr>
            <td>${embroideryCustomer.customerName}</td>
            <td>${embroideryCustomer.emailAddress}</td>
            <td>${embroideryCustomer.phoneNumber}</td>
            
        </tr>
    </c:forEach>
</table>
<br/>
<a href="<c:url value="/create-EmbroideryCustomer"/>">Create EmbroideryCustomer</a>


</body>
</html>