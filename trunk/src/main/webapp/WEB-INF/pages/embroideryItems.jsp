<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page trimDirectiveWhitespaces="true"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/site.css"/>">
<title>EmbroideryItems</title>
<style type="text/css">
table, td, th {
    border: 1px solid #5970B2;
}

th {
    background-color: #5970B2;
    color: white;
}
</style>
</head>
<body>
	<table align="center">
		<tr>
			<th>File Name</th>
			<th>Description</th>
			<th>File path</th>
			<th>Image path</th>
			<th>Price</th>
			<th>Date</th>
			<th>Designer</th>
			<th>User</th>

		</tr>
		<c:forEach items="${embroideryItems}" var="embroideryItem">
			<tr>
				<td>${embroideryItem.fileName}</td>
				<td>${embroideryItem.description}</td>
				<td>${embroideryItem.filePath}</td>
				<td>${embroideryItem.imagePath}</td>
				<td>${embroideryItem.price}</td>
				<td>${embroideryItem.date}</td>
				<td>${embroideryItem.designerId}</td>
				<td>${embroideryItem.userId}</td>

			</tr>
		</c:forEach>
	</table>
	<br />
</body>
</html>