<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
    
<!-- <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd"> -->
<!-- <html> -->
<!-- <head> -->
<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"> -->
<!-- <title>Add a new Embroider Request</title> -->
<!-- </head> -->
<!-- <body> -->
<!-- <h2>Create a new request</h2> -->
<%-- <form:form modelAttribute="embroideryRequest" method="post"> --%>
<!--     <table> -->
    
<!--          <tr> -->
<!--             <td>Width:</td> -->
<!--             <td> -->
<%--                 <form:input path="width"/> --%>
<%--                 <form:errors path="width" element="span"/> --%>
<!--             </td> -->
<!--         </tr> -->
<!--          <tr> -->
<!--             <td>Height</td> -->
<!--             <td> -->
<%--                 <form:input path="height"/> --%>
<%--                 <form:errors path="height" element="span"/> --%>
<!--             </td> -->
<!--         </tr> -->
<!--          <tr> -->
<!--             <td>Description:</td> -->
<!--             <td> -->
<%--                 <form:input path="description"/> --%>
<%--                 <form:errors path="description" element="span"/> --%>
<!--             </td> -->
<!--         </tr> -->
        
        
        
<!--     </table> -->
<!--     <br/> -->
<!--     <input type="submit" value="Create" /> -->
<%-- </form:form> --%>

<!-- </body> -->
<!-- </html> -->

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="English"/>
    <link rel="stylesheet" media="all" href="<c:url value="/resources/site.css"/>">
    <title>Create embroideryRequest</title>
</head>
<body>
<h2>Create Embroidery request Here...</h2>

<hr align="left" width="600">

<form:form modelAttribute="embroideryRequest" method="post" enctype="multipart/form-data">
    <table>
		<tr>
			        <td>Select your image:</td>
			        	<td> 
			        		<input type="file" name="jpegUpload"/>
			        	</td>
		</tr>  
		<tr>
		<td>Unit of measurement: </td>
		<td><select>
				<option value="cm">cm</option>
				<option value="inches">inches</option>
		</select></td>
		</tr>
		   <tr>
            <td>Width:</td>
            <td>
                <form:input path="width"/>
                <form:errors path="width" element="span"/>
            </td>
        </tr>
         <tr>
            <td>Height</td>
            <td>
                <form:input path="height"/>
                <form:errors path="height" element="span"/>
            </td>
        </tr>
         <tr>
            <td>Description:</td>
            <td>
                <form:input path="description"/>
                <form:errors path="description" element="span"/>
            </td>
        </tr>
        
    </table>
    <br/>    
    <input type="submit" value="Create" />
</form:form>
</body>
</html>