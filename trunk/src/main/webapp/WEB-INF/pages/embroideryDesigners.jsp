<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page trimDirectiveWhitespaces="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EmbroideryDesigners</title>
<style type="text/css">
table, td, th {
    border: 1px solid #5970B2;
}

th {
    background-color: #5970B2;
    color: white;
}

 .pg-normal {
                color: black;
                font-weight: normal;
                text-decoration: none;    
                cursor: pointer;    
            }
            .pg-selected {
                color: black;
                font-weight: bold;        
                text-decoration: underline;
                cursor: pointer;
            }
</style>
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
function Pager(tableName, itemsPerPage) {
    this.tableName = tableName;
    this.itemsPerPage = itemsPerPage;
    this.currentPage = 1;
    this.pages = 0;
    this.inited = false;
    
    this.showRecords = function(from, to) {        
        var rows = document.getElementById(tableName).rows;
        // i starts from 1 to skip table header row
        for (var i = 1; i < rows.length; i++) {
            if (i < from || i > to)  
                rows[i].style.display = 'none';
            else
                rows[i].style.display = '';
        }
    }
    
    this.showPage = function(pageNumber) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}

        var oldPageAnchor = document.getElementById('pg'+this.currentPage);
        oldPageAnchor.className = 'pg-normal';
        
        this.currentPage = pageNumber;
        var newPageAnchor = document.getElementById('pg'+this.currentPage);
        newPageAnchor.className = 'pg-selected';
        
        var from = (pageNumber - 1) * itemsPerPage + 1;
        var to = from + itemsPerPage - 1;
        this.showRecords(from, to);
    }   
    
    this.prev = function() {
        if (this.currentPage > 1)
            this.showPage(this.currentPage - 1);
    }
    
    this.next = function() {
        if (this.currentPage < this.pages) {
            this.showPage(this.currentPage + 1);
        }
    }                        
    
    this.init = function() {
        var rows = document.getElementById(tableName).rows;
        var records = (rows.length - 1); 
        this.pages = Math.ceil(records / itemsPerPage);
        this.inited = true;
    }

    this.showPageNav = function(pagerName, positionId) {
    	if (! this.inited) {
    		alert("not inited");
    		return;
    	}
    	var element = document.getElementById(positionId);
    	
    	var pagerHtml = '<span onclick="' + pagerName + '.prev();" class="pg-normal"> &#171 Prev </span> | ';
        for (var page = 1; page <= this.pages; page++) 
            pagerHtml += '<span id="pg' + page + '" class="pg-normal" onclick="' + pagerName + '.showPage(' + page + ');">' + page + '</span> | ';
        pagerHtml += '<span onclick="'+pagerName+'.next();" class="pg-normal"> Next &#187;</span>';            
        
        element.innerHTML = pagerHtml;
    }
}




</script>
<script type="text/javascript">
function rowClick(row){
	var cells=row.getElementsByTagName("td");
	var check=confirm("Do you want to pass designer "+cells[0].innerText+" data to the update page?");
 	if(check==true){
 		var dId=cells[0].innerText;
 		var dName=cells[1].innerText;
 		var dAddress=cells[2].innerText;
 		var email1=cells[3].innerText;
 		var email2=cells[4].innerText;
 		var phone1=cells[5].innerText;
 		var phone2=cells[6].innerText;
 		var description=cells[7].innerText;
 		var active=cells[8].innerText;
 		
 		parent.createTableIframe.dId.value=dId;
 		parent.createTableIframe.dName.value=dName;
 		parent.createTableIframe.dAddress.value=dAddress;
 		parent.createTableIframe.dEmail1.value=email1;
 		parent.createTableIframe.dEmail2.value=email2;
 		parent.createTableIframe.dPhone1.value=phone1;
 		parent.createTableIframe.dPhone2.value=phone2;
 		parent.createTableIframe.dDescription.value=description;
 		parent.createTableIframe.idDesignerActive.value=active;
 		parent.createTableIframe.preUpdateValuesSetter(dId,dName,email1,email2,phone1,phone2,dAddress,description,active);
 		parent.createTableIframe.dSubmit.disabled=true;
 	}else{
 		doNothing();
 	}
}

function doNothing(){}

function searchDesigner(){
	document.getElementById("idDesignerMode").value="SEARCH";
	document.getElementById("searchDesignerForm").submit();
}

function clrSearch(){
	document.getElementById("searchDesignerNameText").value="";
 	document.getElementById("searchDesignerPhone1Text").value="";
}

function tableRowCount(){
	var count=(document.getElementById("listDesignersTable").rows.length)-1;
	if(count>1){
	document.getElementById("rowCountDisplay").innerText="Data of "+count+" designers loaded!";
	}else if(count==1){
		document.getElementById("rowCountDisplay").innerText="Data of only one designer loaded!";
	}else if(count==0){
		document.getElementById("rowCountDisplay").innerText="No such designer exists in the database";
	}
}

// var pager = new Pager('listDesignersTable', 3); 
// pager.init(); 
// pager.showPageNav('pager', 'pagDisplayDiv'); 
// pager.showPage(1);

</script>
</head>
<body onload="tableRowCount()">

<form:form modelAttribute="embroideryDesigner" method="post" id="searchDesignerForm">
	<table align="center" id="searchDesignerTable" style="border: none;">
		<tr style="border: none;">
			<td style="border: none;"><b>Designer Name:-</b> </td>
			<td style="border: none;">
				<form:input path="designerName" id="searchDesignerNameText"/>
                <form:errors path="designerName" element="span"/>
			</td>
			<td style="border: none;"><b>Phone Number 1:-</b></td>
			<td style="border: none;">
				<form:input path="phoneNumber1" id="searchDesignerPhone1Text"/>
                <form:errors path="phoneNumber1" element="span"/>
			</td>
			<td style="display: none;">
				<form:input path="mode" id="idDesignerMode" />
                <form:errors path="mode" element="span"/>
			</td>
			<td style="border: none;"><input type="button" value="Search" id="dSearch" onclick="searchDesigner()"></td>
			<td style="border: none;"><input type="button" value="Clear" id="searchClear" onclick="clrSearch()"/></td>
			<td style="border: none;"><input type="button" value="Show All" id="showAllResultsButton" onclick="parent.frameRefresh()"></td>
		</tr>
	</table>
</form:form>
<br>
	<h4 align="center" id="rowCountDisplay">Show</h4>
<div id="pagDisplayDiv" align="center"></div>
<table align="center" id="listDesignersTable">
    <tr>
    	<th style="display: none;">Designer ID</th>
        <th>Designer Name</th>
        <th>Designer Address</th>
        <th>E-Mail Address 1</th>
        <th>E-Mail Address 2</th>
        <th>Phone Number 1</th>
        <th>Phone Number 2</th>
        <th>Description</th>
        <th style="display: none;">Active</th>
    </tr>
    <c:forEach items="${embroideryDesigners}" var="embroideryDesigner">
        <tr onclick="rowClick(this)" style="cursor: pointer;">
        	<td style="display: none;" >${embroideryDesigner.id}</td> <%-- cell 0 --%>
            <td >${embroideryDesigner.designerName}</td> <%-- cell 1 --%>
            <td >${embroideryDesigner.address}</td>   <%-- cell 2 --%>
            <td >${embroideryDesigner.emailAddress1}</td>  <%-- cell 3 --%>
            <td >${embroideryDesigner.emailAddress2}</td>  <%-- cell 4 --%>
            <td >${embroideryDesigner.phoneNumber1}</td>  <%-- cell 5 --%>
            <td >${embroideryDesigner.phoneNumber2}</td>  <%-- cell 6 --%>
            <td >${embroideryDesigner.description}</td>   <%-- cell 7 --%>
             <td style="display: none;">${embroideryDesigner.active}</td><!-- cell 8 -->
        </tr>
    </c:forEach>
</table>
<br/>
<script type="text/javascript">
        var pager = new Pager('listDesignersTable', 10); 
        pager.init(); 
        pager.showPageNav('pager', 'pagDisplayDiv'); 
        pager.showPage(1);
    </script>


</body>
</html>