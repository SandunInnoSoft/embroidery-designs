<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />
<title>Embroidery online | Home</title>

<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/bootstrap.css"/>">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=" <c:url value="/resources/js/jquery.min.js"/>"></script>
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Custom Theme files -->
<!--theme-style-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/style.css"/>">

<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jewellry online Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/easing.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/scripts.js"/>"></script>


<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
</script>

<!-- Ajax function -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script>

$(document).ready(function(){
	$("#animal").click(function(){
		$.post("index",
				{name: "Animals"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	
//	$("")
	$("#letter").click(function(){
		$.post("index",
				{name: "Letters"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	$("#bird").click(function(){
		$.post("index",
				{name: "Birds"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	$("#alphabet").click(function(){
		$.post("index",
				{name: "Alphabet"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	$("#fish").click(function(){
		$.post("index",
				{name: "Fishes"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	$("#people").click(function(){
		$.post("index",
				{name: "People"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	$("#flower").click(function(){
		$.post("index",
				{name: "Flowers"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html( $response.find("#product").html());
		        }
		        );
    });
	
	$("#searchForm").submit(function(){
		$.get("search&id=12b4",
 				{keyword: "b"},
				function(data){
					var $response=$(data);
		            $("#content-right-in").html($response.find("#product").html());
		        }
		        );
    });
	
});

</script>

<!-- hhhhhhhhhhhhhhhhhjjjjjjjjjjjjjjjjjjjjj -->
<style>
 .carousel-inner > .item > .img,
 .carousel-inner > .item > .a > .img {
     width: 100%;
     margin: auto;
 }
</style>

</head>
<body>
<!--header-->
	<div class="header">
		<div class="container">			
			<ul>
				<li><a href="account">MY ACCOUNT</a><span>|</span></li>
				<li><a href="register">  REGISTER</a><span>|</span></li>
				<li><a href="contact" > CONTACT US</a></li>
			</ul>
				<div class="clearfix"> </div>
			</div>
			
	</div>
	<!---->
	<div class="container" id="">
		<div class="header-top">
			<div class="header-top-in">
				<div class="top-header">
					<div class="logo">
							<a href="index"><img src="<c:url value="/resources/images/embroidery.png"/>" alt=" "></a>
					</div>
					<div class="left-head">
<!-- Adding embroidery image to center of the header..		<img class="img-responsive" src="images/stone.png" alt=" " > -->
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="top-header-in">
					<a href="#"><img class="img-responsive bag"  src="<c:url value="/resources/images/bag.png"/>" alt=" " ></a>
					<div id="shop" class="shop">
						<h6>Shopping basket</h6>
						<p style="word-spacing: 0.3em;"><span>${itemCount}</span>  <c:if test="${itemCount != 1}">ITEMS</c:if><c:if test="${itemCount == 1}">ITEM</c:if> | $<span>${totalPrice}</span></p>  
	
						<c:if test="${itemCount != 0}">
							<a href="checkout">View Cart<i> </i></a>												
						</c:if>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---->
			<div class="top-nav">
					<span class="menu">MENU </span>
					<ul>
						<li class="active"><a href="index">HOME</a></li>
						<li><a href="design"> NEW DESIGNS</a></li>
						<li><a href="gift">  PROMOTIONS  </a></li>
						<li><a href="gift">  PACKAGES  </a></li>						
						<li><a href="404">HELP</a></li>
						<li><a href="about">ABOUT US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
				</script>
			</div>
			<!---->
			<div class="content">
			<div class="col-md-9 content-right">
<!-- 					<div class="banner"> -->
<!-- 						<h1>Quisque malesuada </h1> -->
<!-- 					</div> -->
					<div class="content-right-in" id="content-right-in">

<!-- Carousel Slider  for packages view -->
					<div id="packageView" style="width: 100%;height: 300px; border-radius: 10px;">
						  <br>
						  <div id="myCarousel" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ol class="carousel-indicators">
						      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						      <li data-target="#myCarousel" data-slide-to="1"></li>
						      <li data-target="#myCarousel" data-slide-to="2"></li>
						      <li data-target="#myCarousel" data-slide-to="3"></li>
						    </ol>
						
						    <!-- Wrapper for slides -->
						    <div class="carousel-inner" role="listbox">
						      <div class="item active">
						        <img src="<c:url value="/resources/images/white.png"/>" alt="Chania" width="685" height="200">
						        <div class="carousel-caption">						        
   						            <h3><a>Package name</a></h3>
							        <p>Package description goes here...Package description goes here...Package description goes here...Package description goes here...Package description goes here...Package description goes here...</p>
							        <br>
<%-- 								    <img width="80" height="80"  src="<c:url value="/resources/imgfiles/chicken06.jpg"/>" /> --%>
<%-- 								    <img width="80" height="80"  src="<c:url value="/resources/imgfiles/baby-minnie-mouse-pictures-baby_minnie_mouse_machine_embroidery_design_--_0347_1c653bcc.jpg"/>" />  --%>
<%-- 								    <img width="80" height="80"  src="<c:url value="/resources/imgfiles/1609_250.gif"/>" /> --%>
<%-- 								    <img width="80" height="80"  src="<c:url value="/resources/imgfiles/84196.jpg"/>" />  --%>
<%-- 								    <img width="80" height="80"  src="<c:url value="/resources/imgfiles/6c032029f33d.jpg"/>" /> 							         --%>
						        </div>				        
						      </div>
						
						    </div>
						
						    <!-- Left and right controls -->
						    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
						      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
						      <span class="sr-only">Previous</span>
						    </a>
						    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
						      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
						      <span class="sr-only">Next</span>
						    </a>
						  </div>

					</div>

					<div class="product" id="product">
						<c:if test="${fn:length(embroideryItems) > 0 }">
						<c:if test="${myKeyword != null}">
							<div style="text-decoration: none;font-size: medium;; color: #B5580E;word-spacing:0.4em;">Viewing Results for keyword '<span style="color: black;">${myKeyword}</span>' ...</div>
							<br>
						</c:if>
						</c:if>
						<div class="product-top">
						
						<c:if test="${fn:length(embroideryItems) > 0 }">
							<c:forEach items="${embroideryItems}" var="embroideryItem" >
									<c:if test="${embroideryItem.hasPromo == 1}">							
										<div class="col-md-3 product-grid">
												<a href="index"><img class="img-responsive" src="<c:url value="${embroideryItem.imagePath}"/>" alt=""></a>
												<div class="ut" style="position: relative;">
													<input id="hid" name="hid" type="hidden" value="${embroideryItem.id}"/>									
													<p style="padding-bottom: 1em;"><a href="itm&itmId=${embroideryItem.id}" onclick="addToCart()">${embroideryItem.fileName}</a></p>
													
														<span style="text-decoration: line-through;padding: 0em;" id="priceEl">${embroideryItem.price}</span>${embroideryItem.price - 5.50}<a style="color: black;">$</a>
														<img style="width: 80px;height: 80px;position: absolute;left: -25px;top: -215px;" alt="" src="<c:url value="/resources/images/pro.GIF"/>">												
													
<!-- 													<a id="toCart" href="#" onclick="addToCheckout()">ADD TO CART -->
<%-- <%-- 													<img id="wishlistIcon" alt="" src="<c:url value="/resources/images/wishlist.png"/>"> --%> 
<!-- 													</a> -->
												</div>
										</div>
									</c:if>										
									<c:if test="${embroideryItem.hasPromo == null}">
										<div class="col-md-3 product-grid">
												<a href="index"><img class="img-responsive" src="<c:url value="${embroideryItem.imagePath}"/>" alt=""></a>
												<div class="ut">
													<input id="hid" name="hid" type="hidden" value="${embroideryItem.id}"/>									
													<p><a href="itm&itmId=${embroideryItem.id}" onclick="addToCart()">${embroideryItem.fileName}</a></p>
													
														<span id="priceEl">${embroideryItem.price}</span>													
<%-- 														<img style="width: 150px;height: 150px;" alt="" src="<c:url value="/resources/images/pro2.png"/>">												 --%>
													
<!-- 													<a id="toCart" href="#" onclick="addToCheckout()">ADD TO CART -->
<%-- <%-- 													<img id="wishlistIcon" alt="" src="<c:url value="/resources/images/wishlist.png"/>"> --%> 
<!-- 													</a> -->
												</div>
										</div>

									</c:if>
									
							</c:forEach>						
						</c:if>						

						<c:if test="${fn:length(embroideryItems) <= 0 }">
							<c:if test="${myKeyword != null}">
							<div style="text-decoration: none;font-size: medium;; color: #B5580E;word-spacing:0.4em;">No Results were found for the keyword '<span style="color: black;">${myKeyword}</span>' ...</div>
							</c:if>	
							<c:if test="${myKeyword == null}">
								<br>
								<div style="text-decoration: none;font-size: medium;; color: #B5580E;word-spacing:0.4em;">No results were found...</div>							
							</c:if>												
						</c:if>
<!-- 							<input type="hidden" id="hf" name="hf" value="indexd"> -->

							
							<div class="clearfix"></div>

							<script type="text/javascript">
							function addToCheckout(){
								var val = document.getElementById('priceEl').innerHTML;
								document.getElementById('basketVal').innerHTML = val;
							}
							</script>

						</div>
					</div>
<!-- 					<div class="clearfix"> </div> -->
					
						<div class="clearfix"></div>
					</div>
					
					
<!-- 					<div class="content-right-bottom"> -->
<!-- 						<h3><a href="single.html">Fusce <span>id venenatis</span> velit</a></h3> -->
<!-- 						<p>Ut tempor, elit vel adipiscing elementum, odio velit adipiscing magna, non posuere nulla ante et felis. Phasellus venenatis nisi at erat euismod tristique. Nullam vestibulum aliquam ligula, quis ultricies dui pulvinar a. Maecenas adipiscing lectus nec justo semper et blandit purus consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin ut ante sit amet purus elementum porttitor faucibus at velit. Morbi dictum erat vitae sapien accumsan vitae ultricies risus euismod. Praesent pulvinar bibendum volutpat. In dignissim pretium urna id malesuada. </p> -->
<!-- 					</div> -->
<!-- 					<div class="content-right-bottom"> -->
<!-- 						<h3><a href="single.html">Fusce <span>id venenatis</span> velit</a></h3> -->

<!-- 						<p>Ut tempor, elit vel adipiscing elementum, odio velit adipiscing magna, non posuere nulla ante et felis. Phasellus venenatis nisi at erat euismod tristique. Nullam vestibulum aliquam ligula, quis ultricies dui pulvinar a. Maecenas adipiscing lectus nec justo semper et blandit purus consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Proin ut ante sit amet purus elementum porttitor faucibus at velit. Morbi dictum erat vitae sapien accumsan vitae ultricies risus euismod. Praesent pulvinar bibendum volutpat. In dignissim pretium urna id malesuada. </p> -->
<!-- 					</div> -->
				</div>
				<div class="col-md-3 content-left">
					<div class="search">
						<h5>Search Our Designs</h5>
						<form action="search&id=12b4" id="searchForm" name="searchForm" method="get">
							<input type="text" name="keyword" id="keyword" value="enter your search terms" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '';}">
							<input id="submitbtn" type="submit" value="">
						</form>
<!-- 						<a href="#">advanced search</a> -->
					</div>
					
					<!-- list Categories -->
					
					<div class="single-bottom" id="single-bottom">
					<br>
						<h4>Categories</h4>
						<ul>
						
<%-- 						<c:forEach items="${categories}" var="category"> --%>
<%-- 						<li ><a   href="#"><i id="catg"> </i>${category.catName}</a></li> --%>
<%-- 						</c:forEach> --%>

						<li ><a  id="animal"  href="#"><i> </i>Animals</a></li>
						<li ><a  id="letter" href="#"><i> </i>Letters</a></li>
						<li ><a  id="bird" href="#"><i> </i>Birds</a></li>
						<li ><a  id="alphabet" href="#"><i> </i>Alphabet</a></li>
						<li ><a  id="fish" href="#"><i> </i>Fishes</a></li>
						<li ><a  id="people" href="#"><i> </i>People</a></li>
						<li ><a  id="flower" href="#"><i> </i>Flowers</a></li>						
						
						</ul>
					</div>
									
				</div>
				
				<div class="clearfix"> </div>
			</div>
			
			<div class="footer" id="footer">
				<div class="footer-top">
					<p class="footer-class">Copyright � 2015 Jewellry online Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul class="footer-bottom">
						<li><a href="index">HOME</a><span>|</span></li>
						<li><a href="about">  ABOUT  </a><span>|</span></li>
						<li><a href="product">  PRODUCTS</a><span>|</span></li>						
						<li><a href="404">HELP</a><span>|</span></li>
						<li><a href="contact">CONTACT</a></li>
					</ul>
					<div class="clearfix"> </div>
					</div>
					<div class="bottom-footer-in">
						<a href="index"><img  src="<c:url value="/resources/images/logo-1.png"/>" alt=" "></a>
					</div>
			</div>
		</div>
	</div>
</body>
</html>