<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=utf8"
	pageEncoding="utf8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page trimDirectiveWhitespaces="true"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />

<style type="text/css">
SELECT, INPUT[type="text"] {
	width: 160px;
	box-sizing: border-box;
}

SECTION {
/* 	margin:20px; */
	padding-top:10px;
	padding-bottom:10px;	
	background-color: white;
	overflow: auto;
}

SECTION>DIV {
 	float: left;
	padding: 4px;
}

SECTION>DIV+DIV {
	width: 40px;
	text-align: center;
}
</style>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<link rel="stylesheet" media="all"
	href="<c:url value="/resources/site.css"/>">

<title>Create embroideryItem</title>


<script type="text/javascript">
		function viewValues(){
			var sel = document.getElementById('rightValues');
			var sv =[];
			for (i = 0; i < sel.length; i++) {
				sv = sel.options[i].value;
            	alert(sv);
			}
			
		}
</script>

</head>
<body>
<div align="center">
	<form:form modelAttribute="embroideryItem" method="post"
		enctype="multipart/form-data">
		<table style="float: left;margin-left: 15%;" >
			<tr>
				<td>Image:</td>
				<td><input type="file" name="jpegUpload"
					onchange="ValidateImageUpload(this)" /></td>
			</tr>

			<tr>
				<td>Embroidery File:</td>
				<td><input type="file" name="fileUpload"
					onchange="ValidateEmbFileUpload(this)" /></td>
			</tr>

			<tr>
				<td>Filename:</td>
				<td><form:input path="fileName" /> <form:errors
						path="fileName" element="span" />
				</td>
			</tr>
			<tr>
				<td>Description:</td>
				<td><form:textarea path="description" rows="8" cols="50" /> <form:errors
						path="description" element="span" /></td>
			</tr>
			<tr>
				<td>Price:</td>
				<td><form:input path="price" /> <form:errors path="price"
						element="span" /></td>
			</tr>
			<tr>
				<td>Select Designer:</td>
				<td><form:select path="designerId">
						<form:option value="1" />
						<form:option value="2" />
						<form:option value="3" />
					</form:select> <br> <form:errors path="designerId" element="span" /></td>
			</tr>
		</table>
		<br />

		<!-- Select category for the item by a Two list selection box..	-->
		<div>
		<section>
		<div>
			<select id="leftValues" size="12" multiple>
				<c:forEach items="${categories}" var="category">
					<option>${category.catName}</option>
				</c:forEach>
			</select>
		</div>
		<div align="center" style="margin-top: 7%">
			<input type="button" id="btnLeft" value="&lt;&lt;" />
			<input type="button" id="btnRight" value="&gt;&gt;" />
		</div>
		<div>
			<select name="rightValues" id="rightValues" size="12" multiple="multiple"></select>
			<div>
				<input type="text" id="txtRight" />
			</div>
		</div>
		</section>
		</div>
		<div align="center" style="margin-left: -120px;">
			<input type="submit" value="Create" onclick="viewValues()"/>
		</div>
		
		<!-- Image upload validation -->
		<script type="text/javascript">
			//Image upload validation..
			function ValidateImageUpload(oInput) {
				var _validFileExtensions = [ ".jpg", ".jpeg", ".bmp", ".gif",
						".png" ];

				if (oInput.type == "file") {
					var sFileName = oInput.value;
					if (sFileName.length > 0) {
						var blnValid = false;
						for (var j = 0; j < _validFileExtensions.length; j++) {
							var sCurExtension = _validFileExtensions[j];
							if (sFileName.substr(
									sFileName.length - sCurExtension.length,
									sCurExtension.length).toLowerCase() == sCurExtension
									.toLowerCase()) {
								blnValid = true;
								break;
							}
						}

						if (!blnValid) {
							alert("Sorry, " + sFileName
									+ " is invalid, allowed extensions are: "
									+ _validFileExtensions.join(", "));
							oInput.value = "";
							return false;
						}
					}
				}
				return true;
			}

			//Embroidery file upload validation..		
			function ValidateEmbFileUpload(oInput) {
				var _validFileExtensions = [ ".DST", ".EMB" ];

				if (oInput.type == "file") {
					var sFileName = oInput.value;
					if (sFileName.length > 0) {
						var blnValid = false;
						for (var j = 0; j < _validFileExtensions.length; j++) {
							var sCurExtension = _validFileExtensions[j];
							if (sFileName.substr(
									sFileName.length - sCurExtension.length,
									sCurExtension.length).toLowerCase() == sCurExtension
									.toLowerCase()) {
								blnValid = true;
								break;
							}
						}

						if (!blnValid) {
							alert("Sorry, " + sFileName
									+ " is invalid, allowed extensions are: "
									+ _validFileExtensions.join(", "));
							oInput.value = "";
							return false;
						}
					}
				}
				return true;
			}
		</script>

		<script type="text/javascript">
			$("#btnLeft").click(function() {
				var selectedItem = $("#rightValues option:selected");
				$("#leftValues").append(selectedItem);
			});

			$("#btnRight").click(function() {
				var selectedItem = $("#leftValues option:selected");
				$("#rightValues").append(selectedItem);
			});

			$("#rightValues").change(function() {
				var selectedItem = $("#rightValues option:selected");
				$("#txtRight").val(selectedItem.text());
			});
		</script>

	</form:form>
</div>	
</body>
</html>
