<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page trimDirectiveWhitespaces="true"%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="English" />
<title>Embroidery online | New Designs</title>

<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/bootstrap.css"/>">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src=" <c:url value="/resources/js/jquery.min.js"/>"></script>

<!-- Custom Theme files -->
<!--theme-style-->
<link rel="stylesheet" media="all"
	href="<c:url value="/resources/css/style.css"/>">

<!-- <link href="css/style.css" rel="stylesheet" type="text/css" media="all" /> -->
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Jewellry online Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Michroma' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script type="text/javascript" src="<c:url value="/resources/js/move-top.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/easing.js"/>"></script>

<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
</script>

<!-- Ajax function -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>

<body>
<!--header-->
	<div class="header">
		<div class="container">			
			<ul>
				<li><a href="account">MY ACCOUNT</a><span>|</span></li>
				<li><a href="register">  REGISTER</a><span>|</span></li>
				<li><a href="contact" > CONTACT US</a></li>
			</ul>
				<div class="clearfix"> </div>
			</div>
			
	</div>
	<!---->
	<div class="container" id="">
		<div class="header-top">
			<div class="header-top-in">
				<div class="top-header">
					<div class="logo">
							<a href="index"><img src="<c:url value="/resources/images/embroidery.png"/>" alt=" "></a>
					</div>
					<div class="left-head">
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="top-header-in">
					<a href="#"><img class="img-responsive bag"  src="<c:url value="/resources/images/bag.png"/>" alt=" " ></a>
					<div id="shop" class="shop">
						<h6>Shopping basket</h6>
						<p style="word-spacing: 0.3em;"><span>${itemCount}</span>  <c:if test="${itemCount != 1}">ITEMS</c:if><c:if test="${itemCount == 1}">ITEM</c:if> | $<span>${totalPrice}</span></p>  
						
						<c:if test="${itemCount != 0}">
							<a href="checkout">View Cart<i> </i></a>												
						</c:if>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<!---->
			<div class="top-nav">
					<span class="menu">MENU </span>
					<ul>
						<li><a href="index">HOME</a></li>
						<li><a href="design"> NEW DESIGNS</a></li>
						<li><a href="gift">  PROMOTIONS  </a></li>
						<li><a href="gift">  PACKAGES  </a></li>						
						<li><a href="404">HELP</a></li>
						<li><a href="about">ABOUT US</a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
				</script>
			</div>
			<!---->
				<div class="four">
					<h2>Internal Server Error.</h2>
					<h4>There is a problem with the resource you are looking for,</h4>
					<h4> and it cannot be displayed.</h4>
					<a href="index" >Go Home </a>
				</div>
			<div class="footer">
				<div class="footer-top">
					<p class="footer-class">Copyright � 2015 Jewellry online Template by  <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
					<ul class="footer-bottom">
						<li><a href="index.html">HOME</a><span>|</span></li>
						<li><a href="about.html">  ABOUT  </a><span>|</span></li>
						<li><a href="product.html">  PRODUCTS</a><span>|</span></li>						
						<li><a href="404.html">HELP</a><span>|</span></li>
						<li><a href="contact.html">CONTACT</a></li>
					</ul>
					<div class="clearfix"> </div>
					</div>
					<div class="bottom-footer-in">
						<a href="index.l"><img src="images/logo-1.png" alt=" " ></a>
					</div>
			</div>
		</div>
	</div>
</body>
</html>