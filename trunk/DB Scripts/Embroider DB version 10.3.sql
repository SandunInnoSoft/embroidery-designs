# database 'embroider' 
# Update script version 10.3
# author: VikumSam
# date: 20/07/15


use embroider;


alter table users
drop column dbfunction;

alter table users
drop column active;

alter table users
add active int default 1;