# Embroidery designer database update version 19.0
# @author: VikumSam
# @date: 31/08/2015


ALTER TABLE `embroider`.`item_package` 
DROP FOREIGN KEY `fk_itemPackage_users`;


ALTER TABLE `embroider`.`promotions` 
ADD COLUMN `item` INT NULL AFTER `packageid`,
ADD INDEX `fk_promotions_item_idx` (`item` ASC);
ALTER TABLE `embroider`.`promotions` 
ADD CONSTRAINT `fk_promotions_item`
  FOREIGN KEY (`item`)
  REFERENCES `embroider`.`embroidery_items` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `embroider`.`promotions` 
DROP FOREIGN KEY `fk_promotions_users`;
ALTER TABLE `embroider`.`promotions` 
DROP INDEX `fk_promotions_users` ;
