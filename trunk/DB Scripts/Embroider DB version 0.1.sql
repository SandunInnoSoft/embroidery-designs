# Database Embroidery 
#Created by: VikumSam
#Date: 06/07/15


# Creating the initial script of embroider DB
CREATE database embroider;

USE embroider;
# category table to record categories
create table category(
categoryid int not null auto_increment primary key,
categoryname varchar(25),
description varchar(50)
);

# designer table to record designer records
create table designer(
designerid int not null auto_increment primary key,
fname varchar(20),
mname varchar(20),
lname varchar(20),
email varchar(30)
);
# user table to record user record
create table users(
userid int not null auto_increment primary key,
fullname varchar(50),
useremail varchar(30),
passwordx varchar(30)
);

