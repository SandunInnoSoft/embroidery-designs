# database 'embroider' 
# Update script version 4.0
# author: VikumSam
# date: 07/07/15

use embroider ;

alter table users
add address varchar(30);

# this query is to create table 'package'
create table packages(
packageid int auto_increment primary key not null,
packagename varchar(12) not null,
packagedescrip varchar(50),
active char(1)
); 

# this query is to create an intermediate table 'itempackage'
create table item_package(
id int auto_increment primary key not null,
packageid int not null,
itemid int not null ,
# to make a foreign key reference with the 'pakage' table
constraint fk_itemPackage_Package foreign key(`packageid`) references `packages`(`packageid`),
# to make a foreign key reference with the 'item' table 
constraint fk_itemPackage_EmbroideryItems foreign key(`itemid`) references `embroidery_items`(`id`) 
);

# this query is to create an intermediate table 'promotions'
create table promotions(
id int auto_increment primary key not null,
packageid int not null,
startdate date,
enddate date,
price decimal,
active char(1),
# to make a foreign key reference with the 'pakage' table
constraint fk_promotions_Package foreign key(`packageid`) references `packages`(`packageid`)
);



