# Embroider database update version 7.0
# @author Sandun Munasinghe
# @since  2015-7-8
#

use embroider; # Selects database embroider

create table `type`( # creates a new table called type with following attributes
id int auto_increment primary key,
`name` varchar(20),
description varchar(100)
);