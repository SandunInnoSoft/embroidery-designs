# database 'embroider' 
# Update script version 15.0
# author: VikumSam
# date: 27/07/15

# This script will be used to replace price with discount 
use embroider;

alter table promotions 
drop column price;

alter table promotions
add discount double null;