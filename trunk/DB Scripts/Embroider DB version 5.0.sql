# Embroider database update version 5.0
# @author Sandun Munasinghe
# @since 2015-7-8


use embroider; # Selects the embroider database

alter table customer
add `name` varchar(20); # modifies the customer table to add a new colun to store the customer name
