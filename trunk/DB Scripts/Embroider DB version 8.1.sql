# database 'embroider' 
# Update script version 8.1
# author: VikumSam
# date: 15/07/15

# This Query updates active column in promotions and packages tables

use embroider;

alter table packages
drop column active;

alter table packages
add active char(1) default 1;

alter table promotions
drop column active;

alter table promotions
add active char(1) default 1;