# database 'embroider' 
# Update script version 10.4
# author: VikumSam
# date: 20/07/15

use embroider;

alter table packages
drop column dbfunction;

alter table packages
drop column active;

alter table packages
add active int default 1; 

alter table promotions 
drop column dbfunction;

alter table promotions 
drop column active;

alter table promotions 
add active int default 1; 