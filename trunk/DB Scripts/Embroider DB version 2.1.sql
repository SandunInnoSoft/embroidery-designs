# Embroider database update version 2.1
# @author Asanka Prabath
# @since 2015-7-7
# 

# Alter existing embroidery_items table
ALTER TABLE `embroider`.`embroidery_items` 
ADD COLUMN `userid` INT NOT NULL AFTER `price`,
ADD COLUMN `date` DATETIME NOT NULL AFTER `userid`;
