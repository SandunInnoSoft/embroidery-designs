# database 'embroider' 
# Update script version 10.1
# author: VikumSam
# date: 15/07/15

use embroider;

#This column is added to keep records active state

alter table packages
add dbfunction char(1) null;

alter table promotions
add dbfunction char(1) null;