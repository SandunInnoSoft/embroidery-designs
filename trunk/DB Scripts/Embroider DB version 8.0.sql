# database 'embroider' 
# Update script version 8.0
# author: VikumSam
# date: 15/07/15

use embroider;

#This column is added to keep records active state

alter table users
add active char(1) default 1;
