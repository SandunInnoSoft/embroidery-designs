# database 'embroider' 
# Update script version 16.0
# author: VikumSam
# date: 28/07/15

#  This query adds user name column to table 'users'  
use embroider;

alter table users
add username varchar(20);

alter table users
drop column passwordx;

alter table users
add passwordx varchar(50);

 