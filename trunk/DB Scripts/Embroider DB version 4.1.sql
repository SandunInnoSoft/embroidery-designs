# database 'embroider' 
# Update script version 4.1
# author: VikumSam
# date: 07/07/15

# adding userid and created date for tables packages, item_package, promotions
use embroider;

alter table packages
add userid int not null;

alter table packages
add constraint fk_packages_users foreign key(`userid`) references `users`(`userid`);

alter table packages
add createddate datetime;

alter table promotions
add userid int not null;

alter table promotions
add constraint fk_promotions_users foreign key(`userid`) references `users`(`userid`);

alter table promotions
add createddate datetime;

alter table item_package
add userid int not null;

alter table item_package
add constraint fk_itemPackage_users foreign key(`userid`) references `users`(`userid`);

alter table item_package
add createddate datetime;
