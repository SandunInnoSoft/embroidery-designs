# Embroider database update version 2.1
# @author Asanka Prabath
# @since 2015-7-7
# 

# selects the embroider database
use embroider;

# Deletes existing embroidery_items table
drop table embroidery_items;

CREATE TABLE `embroider`.`embroidery_items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `filepath` VARCHAR(80) NOT NULL,
  `imagepath` VARCHAR(80) NOT NULL,
  `description` VARCHAR(250) NULL,
  `designerid` INT NULL,
  `price` DECIMAL NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `designerid_idx` (`designerid` ASC),
  CONSTRAINT `designerid`
    FOREIGN KEY (`designerid`)
    REFERENCES `embroider`.`designer` (`designerid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
