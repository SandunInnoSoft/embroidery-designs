# database 'embroider' 
# Update script version 16.1
# author: VikumSam
# date: 29/07/15

use Embroider;

ALTER TABLE `embroider`.`item_package` 
DROP FOREIGN KEY `fk_itemPackage_EmbroideryItems`,
DROP FOREIGN KEY `fk_itemPackage_Package`,
DROP FOREIGN KEY `fk_itemPackage_users`;
ALTER TABLE `embroider`.`item_package` 
ADD CONSTRAINT `fk_itemPackage_EmbroideryItems`
  FOREIGN KEY (`itemid`)
  REFERENCES `embroider`.`embroidery_items` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_itemPackage_Package`
  FOREIGN KEY (`packageid`)
  REFERENCES `embroider`.`packages` (`packageid`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `fk_itemPackage_users`
  FOREIGN KEY (`userid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
