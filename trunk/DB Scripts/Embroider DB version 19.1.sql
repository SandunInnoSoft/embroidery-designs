# Embroidery database update version 19.1
# @author Sandun Munasinghe

use embroider; # Select database Embroider

drop table user_roles; # Deletes user_roles table

# Modifies the existing user id colunm to hold passwords of users
ALTER TABLE `embroider`.`customer` 
CHANGE COLUMN `userid` `password` VARCHAR(25) NOT NULL ;
