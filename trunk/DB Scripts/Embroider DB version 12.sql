# Update version 12
# @author Prabath
# @since  2015-7-21

use embroider; # Selects embroider database

CREATE TABLE `embroider`.`category_item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `catId` INT NOT NULL,
  `itemId` INT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `catId_idx` (`catId` ASC),
  INDEX `itemId_idx` (`itemId` ASC),
  CONSTRAINT `catId`
    FOREIGN KEY (`catId`)
    REFERENCES `embroider`.`category` (`categoryid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `itemId`
    FOREIGN KEY (`itemId`)
    REFERENCES `embroider`.`embroidery_items` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);
