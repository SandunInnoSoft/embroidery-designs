# Embroider database update version 13
# @author VikumSam
# @since  2015-7-22


use embroider;

alter table promotions
add promoname varchar(20) null;

alter table promotions
add promodescrip varchar(20) null;