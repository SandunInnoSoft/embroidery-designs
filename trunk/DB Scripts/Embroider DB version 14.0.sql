# Ebroidery update version 14.0
# @author Vikum
# @since  2015-7-23

ALTER TABLE `embroider`.`promotions` 
DROP FOREIGN KEY `fk_promotions_Package`,
DROP FOREIGN KEY `fk_promotions_users`;
ALTER TABLE `embroider`.`promotions` 
CHANGE COLUMN `packageid` `packageid` INT(11) NULL ,
CHANGE COLUMN `userid` `userid` INT(11) NULL ;
ALTER TABLE `embroider`.`promotions` 
ADD CONSTRAINT `fk_promotions_Package`
  FOREIGN KEY (`packageid`)
  REFERENCES `embroider`.`packages` (`packageid`),
ADD CONSTRAINT `fk_promotions_users`
  FOREIGN KEY (`userid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON UPDATE CASCADE;
