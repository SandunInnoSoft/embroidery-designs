# database 'embroider' 
# Update script version 11.0
# author: VikumSam
# date: 15/07/15

use embroider;

#This column is added to keep records active state
alter table users
drop dbfunction ;

alter table packages
drop dbfunction ;

alter table promotions
drop dbfunction;


alter table users
add dbfunction varchar(1) default 'n';

alter table packages
add dbfunction varchar(1) default 'n';

alter table promotions
add dbfunction varchar(1) default 'n';