# Embroider database update version 3.0
# @author Sandun Munasinghe
# @since 2015-7-7
# 

use embroider; # Selects embroider database

alter table designer
add userid int; # add a new column to store the User ID for the designer table

alter table designer
add `date` datetime; # add a new column to store the date created for the designer table

create table customer( # creates a new table to store the customer data
customerid int auto_increment primary key,
emailaddress varchar(20),
phonenumber char(12),
userid int,
`date` datetime
);

create table embroideryrequest( # creates a new table to record the embroider requests
requestid int primary key auto_increment,
imagefilepath varchar(100),
width float(10,6),
height float(10,6),
description varchar(60),
customerid int,
`date` datetime
);