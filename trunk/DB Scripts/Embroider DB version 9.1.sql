# Embroider database update version 9.1
# @author Asanka Prabath
# @since 2015-7-15
# 

# Alter existing embroidery_items table and drop foreign key.

ALTER TABLE `embroider`.`embroidery_items` 
DROP FOREIGN KEY `designerid`;