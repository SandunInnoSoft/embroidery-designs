# Embroider database update version 7.1
# @author Sandun Munasinghe
# @since  2015-7-9

use embroider; # Selects database embroider

#  Modifies the type table to add a new column to store the admin user ID
alter table type
add userid int;

# Modifies the type table to add a new column to store the date of the record creation
alter table type
add `date` datetime;