# database 'embroider' 
# Update script version 18.0
# author: VikumSam
# date: 05/08/15

use embroider;

# making adjusments to category table
ALTER TABLE `embroider`.`category` 
ADD COLUMN `createduserid` INT NULL AFTER `description`,
ADD COLUMN `date` DATETIME NULL AFTER `createduserid`,
ADD COLUMN `active` INT NOT NULL AFTER `date`,
ADD INDEX `fk_user_Category_idx` (`createduserid` ASC), 
COMMENT = 'This table to keep records on categories' ;

ALTER TABLE `embroider`.`category` 
ADD CONSTRAINT `fk_user_Category`
  FOREIGN KEY (`createduserid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
  
 # Creating table 'categoryhistory'  
  CREATE TABLE `embroider`.`categoryhistory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categoryid` INT NOT NULL,
  `categoryname` VARCHAR(30) NULL,
  `description` VARCHAR(50) NULL,
  `createduser` INT NULL,
  `date` DATETIME NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_categoryhistory_idx` (`createduser` ASC),
  CONSTRAINT `fk_user_categoryhistory`
    FOREIGN KEY (`createduser`)
    REFERENCES `embroider`.`users` (`userid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This keeps tracking on updated category records ';

ALTER TABLE `embroider`.`users` 
ADD COLUMN `createddate` DATE NULL AFTER `passwordx`;

ALTER TABLE `embroider`.`userhistory` 
ADD COLUMN `date` DATETIME NULL AFTER `active`;

