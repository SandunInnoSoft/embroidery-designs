# Embroidery designer database update version 19.0
# @author Sandun Munasinghe

use embroider; #Selects Embroider database

# create a new user roles table
create table user_roles(
`user_role_id` int auto_increment primary key,
role varchar(45),
userid int
);

# Add a friegn key on user roles table to refer embroidery users table
ALTER TABLE `embroider`.`user_roles` 
ADD INDEX `fk_userRoles_users_idx` (`userid` ASC);
ALTER TABLE `embroider`.`user_roles` 
ADD CONSTRAINT `fk_userRoles_users`
  FOREIGN KEY (`userid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
