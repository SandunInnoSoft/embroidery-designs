# Embroider database update version 11
# @author Sandun Munasinghe
# @since  2015-7-20

use embroider; # Selects embroider database

# Add a new column to the designer table to represent the state of the designer record in the database
alter table designer
add active int default 1;