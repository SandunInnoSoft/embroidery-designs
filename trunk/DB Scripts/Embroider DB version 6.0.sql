# database 'embroider' 
# Update script version 6.0
# author: VikumSam
# date: 08/07/15

# ====== +This adds parent user to 'user' table and OU UPDATE CASCADE for 'user','packages','promotions','item_packages'+ ============

use embroider;
# -------------- Droping the current foreigh key and add it again with ON UPDATE CASCADE -----------

alter table packages
drop foreign key fk_packages_users ;

alter table packages
add constraint fk_packages_users foreign key(`userid`) references `users`(`userid`) ON UPDATE CASCADE ;

 # -------------- Droping the current foreigh key and add it again with ON UPDATE CASCADE -----------

alter table promotions
drop foreign key fk_promotions_users;

alter table promotions
add constraint fk_promotions_users foreign key(`userid`) references `users`(`userid`) ON UPDATE CASCADE;


 #------- Adding the Parent user constrain with users table + ON UPDATE CASCADE ----------
alter table users
add createduserid int null;
 
alter table users 
add constraint fk_ParentUser foreign key(`createduserid`) references `users`(`userid`) ON UPDATE CASCADE; 


 # ---------- Droping Foreign Key Constrains -----------
 
alter table item_package
drop foreign key fk_itemPackage_Package;

alter table item_package
drop foreign key fk_itemPackage_EmbroideryItems;

alter table item_package
drop foreign key fk_itemPackage_users;


 
 
 # ---- Adding Foreign keys with ON UPDATE CASCADE -------

alter table item_package
add constraint fk_itemPackage_Package foreign key(`packageid`) references `packages`(`packageid`) ON UPDATE CASCADE;

alter table item_package
add constraint fk_itemPackage_EmbroideryItems foreign key(`itemid`) references `embroidery_items`(`id`) ON UPDATE CASCADE;

alter table item_package
add constraint fk_itemPackage_users foreign key(`userid`) references `users`(`userid`) ON UPDATE CASCADE;

 # ------------------------------------------------------------