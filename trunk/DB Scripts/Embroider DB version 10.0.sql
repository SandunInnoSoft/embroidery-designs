# database 'embroider' 
# Update script version 10.0
# author: VikumSam
# date: 15/07/15

use embroider;

#This column is added to keep records active state

alter table users
add dbfunction char(1) null;