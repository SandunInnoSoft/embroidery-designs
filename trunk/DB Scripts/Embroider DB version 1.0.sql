# Embroider database update version 1.0
# @author Sandun Munasinghe
# @since 2015-7-7
# 

use embroider; # selects the embroider database

drop table designer; # Deletes existing designer table

create table designer( # Recreates the designer table with the following attributes
designerid int auto_increment primary key,
designername varchar(20),
emailaddress1 varchar(20),
emailaddress2 varchar(20),
phonenumber1 char(12),
phonenumber2 char(12),
address varchar(60),
description varchar(50)
);