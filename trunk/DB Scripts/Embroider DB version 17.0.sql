# database 'embroider' 
# Update script version 17.0
# author: VikumSam
# date: 30/07/15

use embroider;

CREATE TABLE `embroider`.`userhistory` (
  `userhistoryid` INT NOT NULL AUTO_INCREMENT,
  `userid` INT NOT NULL,
  `fullname` VARCHAR(50) NOT NULL,
  `username` VARCHAR(20) NOT NULL,
  `useremail` VARCHAR(30) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  `createduserid` INT NOT NULL,
  `passwordx` VARCHAR(50) NOT NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`userhistoryid`),
  INDEX `fk_createdUser_idx` (`createduserid` ASC),
  CONSTRAINT `fk_createdUser`
    FOREIGN KEY (`createduserid`)
    REFERENCES `embroider`.`users` (`userid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This table is to maintain the history of table \'users\'. Whenever the table user is updated, the previous record in \'users\' will be enterd to \'userhistory\' table';

CREATE TABLE `embroider`.`promotionhistory` (
  `idpromotionhistory` INT NOT NULL AUTO_INCREMENT,
  `idpromotion` INT NOT NULL,
  `packageid` INT NOT NULL,
  `promoname` VARCHAR(20) NOT NULL,
  `promodescrip` VARCHAR(100) NOT NULL,
  `stardate` DATETIME NOT NULL,
  `enddate` DATETIME NOT NULL,
  `discount` DOUBLE NOT NULL,
  `userid` INT NOT NULL,
  `createddate` DATETIME NOT NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`idpromotionhistory`),
  INDEX `fk_promotion_package_idx` (`userid` ASC),
  CONSTRAINT `fk_promotion_package`
    FOREIGN KEY (`userid`)
    REFERENCES `embroider`.`users` (`userid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This keeps history records of the table promotion';

ALTER TABLE `embroider`.`userhistory` 
DROP FOREIGN KEY `fk_createdUser`;
ALTER TABLE `embroider`.`userhistory` 
CHANGE COLUMN `fullname` `fullname` VARCHAR(50) NULL ,
CHANGE COLUMN `username` `username` VARCHAR(20) NULL ,
CHANGE COLUMN `useremail` `useremail` VARCHAR(30) NULL ,
CHANGE COLUMN `address` `address` VARCHAR(45) NULL ,
CHANGE COLUMN `createduserid` `createduserid` INT(11) NULL ,
CHANGE COLUMN `passwordx` `passwordx` VARCHAR(50) NULL ;
ALTER TABLE `embroider`.`userhistory` 
ADD CONSTRAINT `fk_createdUser`
  FOREIGN KEY (`createduserid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `embroider`.`promotionhistory` 
DROP FOREIGN KEY `fk_promotion_package`;
ALTER TABLE `embroider`.`promotionhistory` 
CHANGE COLUMN `packageid` `packageid` INT(11) NULL ,
CHANGE COLUMN `promoname` `promoname` VARCHAR(20) NULL ,
CHANGE COLUMN `promodescrip` `promodescrip` VARCHAR(100) NULL ,
CHANGE COLUMN `stardate` `stardate` DATETIME NULL ,
CHANGE COLUMN `enddate` `enddate` DATETIME NULL ,
CHANGE COLUMN `discount` `discount` DOUBLE NULL ,
CHANGE COLUMN `userid` `userid` INT(11) NULL ,
CHANGE COLUMN `createddate` `createddate` DATETIME NULL ;
ALTER TABLE `embroider`.`promotionhistory` 
ADD CONSTRAINT `fk_promotion_package`
  FOREIGN KEY (`userid`)
  REFERENCES `embroider`.`users` (`userid`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `embroider`.`promotionhistory` 
DROP COLUMN `packageid`;

CREATE TABLE `embroider`.`packagehistory` (
  `packagehistoryid` INT NOT NULL AUTO_INCREMENT,
  `packageid` INT NOT NULL,
  `packagename` VARCHAR(20) NULL,
  `packagedescrip` VARCHAR(50) NULL,
  `userid` INT NULL,
  `createddate` DATE NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`packagehistoryid`),
  INDEX `fk_userid_idx` (`userid` ASC),
  CONSTRAINT `fk_userid`
    FOREIGN KEY (`userid`)
    REFERENCES `embroider`.`users` (`userid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This table keeps records history about table \'package\' table';

# To add designer history table
CREATE TABLE `embroider`.`designerhistory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `designerid` INT NOT NULL,
  `designername` VARCHAR(20) NULL,
  `emailaddress1` VARCHAR(45) NULL,
  `emailaddress2` VARCHAR(45) NULL,
  `phonenumber1` VARCHAR(45) NULL,
  `phonenumber2` VARCHAR(45) NULL,
  `address` VARCHAR(45) NULL,
  `description` VARCHAR(50) NULL,
  `userid` INT NULL,
  `date` DATETIME NULL,
  `active` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_user_designerhistory_idx` (`userid` ASC),
  CONSTRAINT `fk_user_designerhistory`
    FOREIGN KEY (`userid`)
    REFERENCES `embroider`.`users` (`userid`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
COMMENT = 'This table keeps history records of table \'designers\'';

